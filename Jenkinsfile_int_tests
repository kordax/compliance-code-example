#!/usr/bin/env groovy

@Library('movi@itg-master') _

version = 'not-set'

def setEnvironment(String tagName) {

    withCredentials([usernamePassword(credentialsId: 'jenkins',
            usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
            passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
        customEnv = sh(script: "gradle -q --console=plain printCustomEnv -PtagName=${tagName}",
                returnStdout: true).trim()
        cloudName = sh(script: "gradle -q --console=plain printEndpoint -PtagName=${tagName}",
                returnStdout: true).trim()

        echo "Target cloud name: $cloudName"
        if (cloudName == 'quality') {
            envName = 'quality'
        } else if (cloudName == 'preprod') {
            envName = 'preprod'
        } else {
            envName = 'development'
        }
        if ( !customEnv.empty ) {
            profile = "${envName}-${customEnv}"
        } else {
            profile = envName
        }
        echo "TAGNAME: $tagName"
        echo "Profile: $profile"
    }
}

def getCurrentCloudForTag(String tagName) {
    String out = 'itg-dev-jenkins-slave'
    if (tagName.contains('.BUILD-SNAPSHOT')) {
        out = 'itg-dev-jenkins-slave'
    } else if (tagName.contains('.BUILD-')) {
        out = 'itg-qa-jenkins-slave'
    } else if (tagName.contains('.RC')) {
        out = 'itg-preprod-jenkins-slave'
    }
    return out
}

pipeline {
    agent none

    environment {
        cloud = getCurrentCloudForTag(env.TAG_NAME)
    }
    stages {
        stage('Init') {
            when {
                anyOf {
                    expression { env.BRANCH_NAME ==~ /(master|develop|release\/[.]+)/ }
                    buildingTag()
                }
            }
            options {
                timeout(time: 5, unit: 'MINUTES')
            }
            agent {
                kubernetes {
                    cloud "${env.cloud}"
                    yamlFile 'KubernetesPod.yaml'
                }
            }
            steps {
                container('gradle') {
                    withCredentials([usernamePassword(credentialsId: 'jenkins',
                            usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
                            passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
                        script {
                            print(env.cloud)
                            sh 'gradle clean'
                            def version = sh(script: "gradle -q --console=plain printVersion",
                                    returnStdout: true).trim()
                            if (env.TAG_NAME != null && version != env.TAG_NAME) {
                                sh "sed -i -e\"s/version = '\\(.*\\)'/version = '${env.TAG_NAME}'/\" build.gradle"
                            }
                        }
                    }

                }
            }
            post {
                always {
                    archiveArtifacts artifacts: 'build.gradle'
                }
            }
        }
        stage('Build') {
            when {
                anyOf {
                    expression { env.BRANCH_NAME ==~ /(master|develop|release\/[.]+)/ }
                    buildingTag()
                }
            }
            options {
                timeout(time: 10, unit: 'MINUTES')
            }
            agent {
                kubernetes {
                    cloud "${env.cloud}"
                    yamlFile 'KubernetesPod.yaml'
                }
            }
            steps {
                container('gradle') {
                    withCredentials([usernamePassword(credentialsId: 'jenkins',
                            usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
                            passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
                        script {
                            def version = sh(script: "gradle -q --console=plain printVersion",
                                    returnStdout: true).trim()
                            if (env.TAG_NAME != null && version != env.TAG_NAME) {
                                sh "sed -i -e\"s/version = '\\(.*\\)'/version = '${env.TAG_NAME}'/\" build.gradle"
                            }
                            sh 'gradle --info --full-stacktrace clean --refresh-dependencies'
                            sh 'gradle --info --full-stacktrace build --refresh-dependencies -x check'
                        }
                    }
                }
                stash includes: '**/build/libs/*.jar', name: 'app'
            }

        }
        stage('Integration Tests') {
            when {
                anyOf {
                    expression { env.BRANCH_NAME ==~ /(master|develop|release\/[.]+)/ }
                    buildingTag()
                }
            }
            options {
                timeout(time: 10, unit: 'MINUTES')
            }
            agent {
                kubernetes {
                    cloud "${env.cloud}"
                    yamlFile 'KubernetesPod.yaml'
                }
            }
            steps {
                container('gradle') {
                    unstash 'app'
                    withCredentials([usernamePassword(credentialsId: 'jenkins',
                            usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
                            passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
                        setEnvironment(env.TAG_NAME)
                        print(profile)
                        sh "gradle :integrationTest -PspringProfiles=${profile}"

                    }
                }
            }
            post {
                always {
                    junit '**/build/test-results/**/*.xml'
                }
            }
        }
    }
}