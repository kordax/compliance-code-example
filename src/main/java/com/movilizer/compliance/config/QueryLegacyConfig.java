package com.movilizer.compliance.config;

import java.util.Date;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "movilizer.legacyapi")
public class QueryLegacyConfig {

    @Value("${movilizer.systemId:0}")
    private Long systemId;

    private Integer eventLimit;
    private Date epochDate;

    public Long getSystemId() {
        return systemId;
    }

    public Integer getEventLimit() {
        return eventLimit;
    }

    public void setEventLimit(Integer eventLimit) {
        this.eventLimit = eventLimit;
    }

    public Date getEpochDate() {
        return epochDate;
    }

    public void setEpochDate(Date epochDate) {
        this.epochDate = epochDate;
    }
}
