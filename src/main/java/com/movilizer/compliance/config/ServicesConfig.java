package com.movilizer.compliance.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movilizer.commons.config.CommonsConfig;
import com.movilizer.commons.service.AuthService;
import com.movilizer.commons.service.BarcodeToEPCService;
import com.movilizer.compliance.service.MeasuringHierarchyServiceWrappingUtil;
import com.movilizer.compliance.service.health.HealthRequestCache;
import com.movilizer.compliance.service.health.IHealthRequestCache;
import com.movilizer.compliance.util.Constants;
import com.movilizer.hierarchy.api.wrapper.v1.HierarchyService;
import io.micrometer.core.instrument.MeterRegistry;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.web.context.annotation.RequestScope;

@Configuration
@Import({AuthService.class, CommonsConfig.class})
@EnableAsync
public class ServicesConfig {

    @Bean
    public IHealthRequestCache healthRequestCachedItemHierarchy(
            HierarchyService hierarchyService, BarcodeToEPCService barcodeToEPCService) {
        return new HealthRequestCache(hierarchyService, barcodeToEPCService);
    }

    @Bean
    public Gson gsonMapper() {
        return new GsonBuilder().serializeNulls().setDateFormat(Constants.SUPERSET_REPORT_DATE_FORMAT).create();
    }

    @Bean
    public TaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor taskExecutor = new ThreadPoolTaskExecutor();
        taskExecutor.setCorePoolSize(4);
        taskExecutor.setMaxPoolSize(10);
        return taskExecutor;
    }
}
