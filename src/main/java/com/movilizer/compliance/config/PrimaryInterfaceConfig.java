package com.movilizer.compliance.config;

import javax.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "movilizer.primary-interface")
@Validated
public class PrimaryInterfaceConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(PrimaryInterfaceConfig.class);

    @org.hibernate.validator.constraints.URL private String url;
    private String user;
    private String pass;

    public String getUrl() {
        return url;
    }

    public String getUser() {
        return user;
    }

    public String getPass() {
        return pass;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @PostConstruct
    private void afterInit() {
        LOGGER.info(toString());
    }

    @Override
    public String toString() {
        return "PrimaryInterfaceConfig{"
                + "url='"
                + url
                + '\''
                + ", auth='"
                + user
                + '\''
                + ", pass='"
                + pass
                + '\''
                + '}';
    }
}
