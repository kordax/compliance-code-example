package com.movilizer.compliance.config;

import com.movilizer.commons.util.MonWrapper;
import com.movilizer.monitoring.api.wrapper.MonitoringWrapperClient;
import com.movilizer.monitoring.api.wrapper.v1.MonitoringService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class MonitoringConfig {
    @Bean
    @Primary
    MonWrapper monWrapper(MonitoringService monitoringService) {
        return new MonWrapper(monitoringService, "[AUTO-COMPLIANCE] ");
    }
}
