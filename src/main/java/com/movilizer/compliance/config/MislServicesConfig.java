package com.movilizer.compliance.config;

import java.time.Instant;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.movilizer.api.maf.dataentry.MafDataEntryBlockingClient;
import com.movilizer.api.maf.discrete.MafDataDiscreteIndexBlockingClient;
import com.movilizer.api.maf.encoding.ByteStringMapper;
import com.movilizer.api.maf.encoding.ByteStringMapperImpl;
import com.movilizer.api.maf.encoding.ByteStringUtils;
import com.movilizer.api.maf.timeseries.MafDataTimeseriesIndexBlockingClient;
import com.movilizer.commons.checking.CheckerProvider;
import com.movilizer.commons.checking.CommonCheckingService;
import com.movilizer.commons.service.MafDataService;
import com.movilizer.compliance.service.MeasuringHierarchyServiceWrappingUtil;
import com.movilizer.compliance.service.destination.DestinationResolverACImpl;
import com.movilizer.compliance.service.destination.MixedHierarchyResolver;
import com.movilizer.compliance.service.destination.TargetMarketService;
import com.movilizer.compliance.service.destination.interfaces.DestinationResolverAC;
import com.movilizer.compliance.service.warehouse.CacheableWarehouseServiceImpl;
import com.movilizer.compliance.service.warehouse.WrapperItemService;
import com.movilizer.compliance.service.warehouse.WrapperItemServiceImp;
import com.movilizer.hierarchy.api.wrapper.HierarchyClient;
import com.movilizer.hierarchy.api.wrapper.v1.HierarchyService;
import com.movilizer.masterdata.api.v1.MDDeliveryNoteService;
import com.movilizer.masterdata.api.v1.MDLocationService;
import com.movilizer.masterdata.api.v1.MDRetailProductService;
import com.movilizer.messageinbox.api.wrapper.MessageInboxClient;
import com.movilizer.messageinbox.api.wrapper.v1.MessageInboxService;
import com.movilizer.messagerelay.api.wrapper.MessageRelayClient;
import com.movilizer.messagerelay.api.wrapper.v1.MessageRelayService;
import com.movilizer.monitoring.api.wrapper.MonitoringWrapperClient;
import com.movilizer.monitoring.api.wrapper.v1.MonitoringService;
import com.movilizer.ps.itg.warehouse.services.WarehouseUtils;
import com.movilizer.uaecommons.config.DestinationResolverProperties;
import com.movilizer.uaecommons.destination.CountryCodes;
import com.movilizer.warehouse.api.wrapper.WarehouseClient;
import com.movilizer.warehouse.api.wrapper.v1.WarehouseService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class MislServicesConfig {
  @Value("${movilizer.systemId:0}")
  private Long systemId;

  @Bean
  @Primary
  WarehouseService warehouseService(
      WarehouseClient warehouseClient, MonitoringService monitoringService) {
    return new CacheableWarehouseServiceImpl(
        warehouseClient.forSystemIdV1(systemId, systemId), monitoringService);
  }

  @Bean
  WarehouseUtils warehouseUtils(
      WarehouseService warehouseService, HierarchyService hierarchyService) {
    return new WarehouseUtils(warehouseService, hierarchyService);
  }

  @Bean
  @Primary
  DestinationResolverAC destinationResolver(
      DestinationResolverProperties destinationResolverProperties,
      WarehouseService warehouseService,
      HierarchyService hierarchyService,
      MDLocationService locationService,
      MDDeliveryNoteService deliveryNoteService,
      MDRetailProductService retailProductService) {

    return new DestinationResolverACImpl(
        destinationResolverProperties,
        warehouseService,
        hierarchyService,
        locationService,
        deliveryNoteService,
        retailProductService);
  }

  @Bean
  @Primary
  MixedHierarchyResolver mixedHierarchyResolver(TargetMarketService targetMarketService) {
    return new MixedHierarchyResolver(targetMarketService);
  }

  @Bean
  public CountryCodes countryCodes() {
    DestinationResolverProperties destinationResolverProperties =
        new DestinationResolverProperties();
    destinationResolverProperties.setBrexitDate(Instant.ofEpochSecond(1574171725));
    return new CountryCodes(destinationResolverProperties.getBrexitDate());
  }

  @Bean
  @Primary
  MonitoringService monitoringService(MonitoringWrapperClient monitoringWrapperClient) {
    return monitoringWrapperClient.forSystemIdFromV1(systemId);
  }

  @Bean
  @Primary
  MessageInboxService messageInboxService(MessageInboxClient client) {
    return client.forSystemIdFromV1(systemId);
  }

  @Bean
  @Primary
  MessageRelayService messageRelayService(MessageRelayClient client) {
    return client.forSystemIdFromV1(systemId);
  }

  @Bean
  @Primary
  HierarchyService hierarchyService(
      HierarchyClient client,
      MeasuringHierarchyServiceWrappingUtil measuringHierarchyServiceWrappingUtil) {
    HierarchyService hierarchyService = client.forSystemIdFromV1(systemId);
    return measuringHierarchyServiceWrappingUtil.wrapService(hierarchyService);
  }
}
