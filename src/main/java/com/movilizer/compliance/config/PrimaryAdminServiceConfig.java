package com.movilizer.compliance.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;

@Configuration
@ConfigurationProperties(prefix = "movilizer.primary-admin-service")
@Validated
public class PrimaryAdminServiceConfig {
  private static final Logger LOGGER = LoggerFactory.getLogger(PrimaryAdminServiceConfig.class);

  @org.hibernate.validator.constraints.URL private String url;
  private String user;
  private String pass;

  public String getUrl() {
    return url;
  }

  public void setUrl(String url) {
    this.url = url;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  public String getPass() {
    return pass;
  }

  public void setPass(String pass) {
    this.pass = pass;
  }

  @PostConstruct
  private void afterInit() {
    LOGGER.info("{}", this);
  }

  @Override
  public String toString() {
    return "PrimaryAdminServiceConfig{"
        + "url='"
        + url
        + '\''
        + ", user='"
        + user
        + '\''
        + ", pass='"
        + pass
        + '\''
        + '}';
  }
}
