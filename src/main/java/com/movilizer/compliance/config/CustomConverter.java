package com.movilizer.compliance.config;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationPropertiesBinding;
import org.springframework.core.convert.converter.Converter;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;

@Component
@ConfigurationPropertiesBinding
public class CustomConverter implements Converter<String, Date> {
    private static final Logger LOGGER = LoggerFactory.getLogger(CustomConverter.class);
    private static final DateFormat DF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public @Nullable Date convert(@NotNull String property) {
        try {
            return DF.parse(property);
        } catch (ParseException e) {
            LOGGER.error("Failed to read date property: {}", property, e);

            throw new IllegalArgumentException("Failed to read date property: " + property, e);
        }
    }
}
