package com.movilizer.compliance.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "movilizer")
public class RelayConfig {
    private static final Logger LOGGER = LoggerFactory.getLogger(RelayConfig.class);

    private boolean stubRelay = false;

    private final RelayDestination primary = new RelayDestination();
    private final RelayDestination uae = new RelayDestination();
    private final RelayDestination ksa = new RelayDestination();
    private final RelayDestination router = new RelayDestination();
    private final RelayDestination uk = new RelayDestination();

    public static class Server {
        private String host = "mislproxy";
        private Integer port = 10000;

        public String getHost() {
            return host;
        }

        public void setHost(String host) {
            this.host = host;
        }

        public Integer getPort() {
            return port;
        }

        public void setPort(Integer port) {
            this.port = port;
        }
    }

    public static class RelayDestination {
        private String alias = "";
        private final Server adapter = new Server();
        private final Server target = new Server();
        private String username = "";
        private String password = "";

        public String getAlias() {
            return alias;
        }

        public void setAlias(String alias) {
            this.alias = alias;
        }

        public Server getAdapter() {
            return adapter;
        }

        public Server getTarget() {
            return target;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }
    }

    public boolean isStubRelay() {
        return stubRelay;
    }

    public void setStubRelay(boolean stubRelay) {
        this.stubRelay = stubRelay;
    }

    public RelayDestination getPrimary() {
        return primary;
    }

    public RelayDestination getUae() {
        return uae;
    }

    public RelayDestination getKsa() {
        return ksa;
    }

    public RelayDestination getRouter() {
        return router;
    }

    public RelayDestination getUk() {
        return uk;
    }
}
