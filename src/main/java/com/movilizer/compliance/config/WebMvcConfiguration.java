package com.movilizer.compliance.config;

import static com.google.common.base.Predicates.or;
import static springfox.documentation.builders.PathSelectors.regex;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.base.Predicate;
import com.movilizer.compliance.interceptor.AuthInterceptor;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.util.AntPathMatcher;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.servlet.config.annotation.*;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.BasicAuth;
import springfox.documentation.service.Contact;
import springfox.documentation.service.SecurityScheme;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableWebMvc
@Configuration
@ConfigurationProperties(prefix = "movilizer.interceptors")
@Validated
@EnableSwagger2
public class WebMvcConfiguration implements WebMvcConfigurer {
    private static final Logger LOGGER = LoggerFactory.getLogger(WebMvcConfiguration.class);
    public static final String DEFAULT_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";

    public static class Auth {
        private boolean enabled;

        public Auth() {}

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
    }

    public static class Throttling {
        private boolean enabled;

        public Throttling() {}

        public boolean isEnabled() {
            return enabled;
        }

        public void setEnabled(boolean enabled) {
            this.enabled = enabled;
        }
    }

    @NotNull private Auth auth;
    @NotNull private Throttling throttling;

    @Autowired AuthInterceptor authInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        LOGGER.info("Registering interceptors for controller");

        if (auth.enabled) {
            registry.addInterceptor(authInterceptor)
                    .excludePathPatterns(
                            "/v2/api-docs",
                            "/swagger-resources/**",
                            "/swagger-ui.html",
                            "/webjars/**")
                    .pathMatcher(new AntPathMatcher());
        }
    }

    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("swagger-ui.html")
                .addResourceLocations("classpath:/META-INF/resources/");

        registry.addResourceHandler("/webjars/**")
                .addResourceLocations("classpath:/META-INF/resources/webjars/");
    }

    @Bean
    public Docket postsApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("public-api")
                .apiInfo(getApiInfo())
                .select()
                .paths(postPaths())
                .build()
                .useDefaultResponseMessages(false);
    }

    private Predicate<String> postPaths() {
        return or(regex("/api/posts.*"), regex("/api/dummy.*"));
    }

    private ApiInfo getApiInfo() {

        return new ApiInfo(
                "HONEYWELL ITG ",
                "Api documentation",
                "1.0.0",
                null,
                new Contact("Honeywell", "https://www.honeywell.com/", null),
                null,
                null,
                Collections.emptyList());
    }

    /**
     * Makes sure dates are serialised in "yyyy-MM-dd'T'HH:mm:ss.SSSXXX" format instead of
     * timestamps
     */
    @Override
    public void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        for (HttpMessageConverter<?> converter : converters) {
            if (converter instanceof MappingJackson2HttpMessageConverter) {
                MappingJackson2HttpMessageConverter jsonMessageConverter =
                        (MappingJackson2HttpMessageConverter) converter;
                ObjectMapper objectMapper = jsonMessageConverter.getObjectMapper();
                objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
                objectMapper.setDateFormat(new SimpleDateFormat(DEFAULT_DATE_FORMAT));
                break;
            }
        }
    }

    // Resolution for dots in url coming from emails, which are truncated by spring thinking that
    // its
    // file extension
    // See https://devtools.movilizer.com/jira/browse/TNTACTSUP-87
    // Solution using example
    // https://blog.georgovassilis.com/2015/10/29/spring-mvc-rest-controller-says-406-when-emails-are-part-url-path/
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        configurer.favorPathExtension(false);
    }

    // Resolution for dots in url coming from emails, which are truncated by spring thinking that
    // its
    // file extension
    // See https://devtools.movilizer.com/jira/browse/TNTACTSUP-87
    // Solution using example
    // https://blog.georgovassilis.com/2015/10/29/spring-mvc-rest-controller-says-406-when-emails-are-part-url-path/
    @Override
    public void configurePathMatch(PathMatchConfigurer matcher) {
        matcher.setUseSuffixPatternMatch(false);
    }

    @Bean
    public Docket productAPI() {
        List<SecurityScheme> schemeList = new ArrayList<>();
        schemeList.add(new BasicAuth("basicAuth"));

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())
                .securitySchemes(schemeList)
                .select()
                .apis(RequestHandlerSelectors.any())
                .paths(PathSelectors.any())
                .build()
                .useDefaultResponseMessages(false);
    }

    public Auth getAuth() {
        return auth;
    }

    public void setAuth(Auth auth) {
        this.auth = auth;
    }

    public Throttling getThrottling() {
        return throttling;
    }

    public void setThrottling(Throttling throttling) {
        this.throttling = throttling;
    }
}
