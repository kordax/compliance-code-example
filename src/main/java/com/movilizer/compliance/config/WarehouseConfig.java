package com.movilizer.compliance.config;

import com.movilizer.api.commons.MislConfig;
import com.movilizer.api.misl.spring.warehouse.v1.ItemServiceImpl;
import com.movilizer.api.misl.spring.warehouse.v1.WarehouseClient;
import com.movilizer.api.misl.warehouse.v1.item.ItemService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class WarehouseConfig {
    @Bean
    @Primary
    ItemService itemService(MislConfig mislConfig) {
        WarehouseClient warehouseClient = new WarehouseClient(mislConfig);
        return new ItemServiceImpl(warehouseClient);
    }
}
