package com.movilizer.compliance.config;

import com.movilizer.api.commons.MislConfig;
import com.movilizer.api.ext.kafka.v2.service.KafkaService;
import com.movilizer.api.ext.spring.kafka.v2.KafkaClient;
import com.movilizer.api.ext.spring.kafka.v2.KafkaServiceImpl;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@Configuration
public class KafkaServiceConfig {
    @Bean
    @Primary
    KafkaClient kafkaClient(MislConfig mislConfig) {
        return new KafkaClient(mislConfig);
    }

    @Bean
    @Primary
    KafkaService kafkaService(KafkaClient kafkaClient) {
        return new KafkaServiceImpl(kafkaClient);
    }
}
