package com.movilizer.compliance.config;



import com.movilizer.ps.itg.compliance.restclient.api.TNTComplianceStatusService;
import com.movilizer.ps.itg.compliance.restclient.client.Client;
import com.movilizer.ps.itg.compliance.restclient.client.ClientConfig;
import lombok.Data;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
@ConfigurationProperties(prefix = "movilizer.compliance.rest")
@Data
public class ComplianceClientConfig {
  private static final Logger LOGGER = LoggerFactory.getLogger(ComplianceClientConfig.class);

  private String url;
  private String userName;
  private String userPassword;

  @Bean
  ClientConfig clientConfig(){
    ClientConfig config = new ClientConfig();
    config.setUrl(getUrl());
    config.setBasicAuthUsername(getUserName());
    config.setBasicAuthPassword(getUserPassword());
    return config;
  }

  @Bean
  TNTComplianceStatusService tntComplianceStatusService(ClientConfig config){
    return Client.create(TNTComplianceStatusService.class, config);
  }

}
