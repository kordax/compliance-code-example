package com.movilizer.compliance.config;

import com.movilizer.compliance.util.SOAPTemplate;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.SOAPConstants;
import javax.xml.soap.SOAPException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.soap.saaj.SaajSoapMessageFactory;

@Configuration
public class JAXB2Config {
    private static final Logger LOGGER = LoggerFactory.getLogger(JAXB2Config.class);

    @Qualifier("Jaxb2MarshallerAC")
    @Bean
    public Jaxb2Marshaller marshaller() {
        final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setPackagesToScan(
                "com.movilizer.ps.itg.epcis", "com.movilizer.compliance.context");
        return marshaller;
    }

    @Bean
    public SOAPTemplate soapTemplate(@Qualifier("Jaxb2MarshallerAC") Jaxb2Marshaller marshaller) {
        MessageFactory msgFactory = null;
        try {
            msgFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_1_PROTOCOL);
        } catch (SOAPException e) {
            LOGGER.error("Failed to create MessageFactory", e);
        }
        SaajSoapMessageFactory newSoapMessageFactory = new SaajSoapMessageFactory(msgFactory);
        SOAPTemplate client = new SOAPTemplate(newSoapMessageFactory);
        client.setMarshaller(marshaller);
        client.setUnmarshaller(marshaller);

        return client;
    }
}
