package com.movilizer.compliance.config;

import java.util.List;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "movilizer.reporting")
@Data
public class ReportingConfig {
  @Data
  public static class Epcis {
    private List<String> epcisDestinations;
  }

  private Epcis epcis;
}
