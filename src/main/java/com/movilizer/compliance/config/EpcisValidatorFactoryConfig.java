package com.movilizer.compliance.config;

import com.movilizer.compliance.validation.EpcisValidatorFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({EpcisValidatorFactory.class})
public class EpcisValidatorFactoryConfig {}
