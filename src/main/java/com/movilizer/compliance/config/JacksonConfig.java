package com.movilizer.compliance.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.schema.configuration.ObjectMapperConfigured;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class JacksonConfig implements ApplicationListener<ObjectMapperConfigured> {
    @Override
    public void onApplicationEvent(ObjectMapperConfigured event) {
        event.getObjectMapper().setDefaultPropertyInclusion(JsonInclude.Include.NON_NULL);
    }
}
