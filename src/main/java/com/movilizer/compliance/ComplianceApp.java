package com.movilizer.compliance;

import com.movilizer.commons.config.ItemServicesConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@EnableSwagger2
@SpringBootApplication
@ComponentScan(
    basePackages = {
      "com.movilizer.compliance",
      "com.movilizer.api.commons",
      "com.movilizer.commons",
      "com.movilizer.uaecommons",
      "io.micrometer.core"
    }, excludeFilters = @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = {ItemServicesConfig.class}))
public class ComplianceApp {
  public static void main(String[] args) {
    SpringApplication.run(ComplianceApp.class, args);
  }
}
