package com.movilizer.compliance.exception;

public class UnsupportedFieldTypeException extends OperationException {
    public UnsupportedFieldTypeException(String message) {
        super(message, null, null);
    }

    public UnsupportedFieldTypeException(String message, String opID) {
        super(message, opID, null);
    }
}
