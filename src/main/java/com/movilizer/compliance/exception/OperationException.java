package com.movilizer.compliance.exception;

public class OperationException extends Exception {
    private final String opID;
    private final String code;

    public OperationException(String message, String opID, String code) {
        super(message);
        this.opID = opID;
        this.code = code;
    }

    public OperationException(Throwable cause, String message, String opID, String code) {
        super(message, cause);
        this.opID = opID;
        this.code = code;
    }

    public String getOpID() {
        return opID;
    }

    public String getCode() {
        return code;
    }
}
