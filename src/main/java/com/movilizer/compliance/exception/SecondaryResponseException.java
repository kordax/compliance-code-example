package com.movilizer.compliance.exception;

import org.springframework.web.client.HttpServerErrorException;

public class SecondaryResponseException extends Exception {

    public SecondaryResponseException(String message) {
        super(message);
    }

    public SecondaryResponseException(String message, Throwable cause) {
        super(message, cause);
    }
}
