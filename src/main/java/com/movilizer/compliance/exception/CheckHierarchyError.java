package com.movilizer.compliance.exception;

public class CheckHierarchyError extends RuntimeException {

    public CheckHierarchyError() {
    }

    public CheckHierarchyError(String reason) {
        super(reason);
    }
}
