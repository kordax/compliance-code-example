package com.movilizer.compliance.controller;

import com.google.gson.JsonArray;
import com.movilizer.commons.exception.CommonAuthException;
import com.movilizer.commons.util.Credentials;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.exception.SecondaryResponseException;
import com.movilizer.compliance.model.ResponseError;
import com.movilizer.compliance.model.StatusRequest;
import com.movilizer.compliance.model.recovery.NoChildrenFoundException;
import com.movilizer.compliance.model.recovery.NonTopLevelException;
import com.movilizer.compliance.model.secondary.SecondaryReprocessRequest;
import com.movilizer.compliance.service.health.IHealthRequestCache;
import com.movilizer.compliance.service.secondary.SecondaryReprocessingService;
import com.movilizer.compliance.util.ApiResponsesErrors;
import com.movilizer.compliance.util.WebResponseUtils;
import com.movilizer.itg.reporting.api.report.secondary2superset.SecondaryResponse;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import java.util.concurrent.ExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;

import java.io.IOException;
import java.util.UUID;

import static com.movilizer.compliance.util.ErrorCodes.AUTHORIZATION_FAILED;

@ApiIgnore
@RestController
@RequestMapping("/secondary")
public class SecondaryReprocessingController {
  private static final Logger LOGGER =
      LoggerFactory.getLogger(SecondaryReprocessingController.class);

  private MonWrapper monWrapper;
  private SecondaryReprocessingService secondaryReprocessingService;

  public SecondaryReprocessingController(
      MonWrapper monWrapper,
      SecondaryReprocessingService secondaryReprocessingService) {
    this.monWrapper = monWrapper;
    this.secondaryReprocessingService = secondaryReprocessingService;
  }

  @ApiOperation(
      value = "reprocess failed recall codes in Secondary",
      response = SecondaryResponse.class,
      httpMethod = "POST",
      authorizations = {@Authorization(value = "basicAuth")})
  @ApiResponsesErrors
  @PostMapping(
      value = "/reprocessSecondary",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity<JsonArray> getStatus(
      @RequestBody SecondaryReprocessRequest request, HttpServletRequest httpRequest)
      throws IOException, SecondaryResponseException, NonTopLevelException,
      NoChildrenFoundException, IHealthRequestCache.UnableToGetHierarchyForUIException,
      OperationException, InterruptedException, ExecutionException {
    Credentials credentials;
    try {
      credentials = Credentials.extractCredentials(httpRequest);
    } catch (CommonAuthException e) {
      LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
      monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
      ResponseError responseError =
          new ResponseError(
              "Failed to extract credentials: " + e.getMessage(), AUTHORIZATION_FAILED);
      return WebResponseUtils.unauthorized(responseError);
    }

    final String opID = UUID.randomUUID().toString();
    long ts = System.currentTimeMillis();

    LOGGER.info(
        "Processing getStatus request: {}\nDeviceAddress: {}", request, credentials.deviceAddress);
    monWrapper.infoJson(
        request,
        "Processing {} request,  DeviceAddress: {}",
        "getStatus",
        credentials.deviceAddress);

    if (request.getReprocess()) {
      if (!secondaryReprocessingService.reprocess(request.getRecallCode(), opID, credentials)) {
        return WebResponseUtils.internalError("Request failed", opID, ts);
      } else {
        return WebResponseUtils.ok("Request succeeded", "000000", opID, ts);
      }
    } else {
      return WebResponseUtils.ok(secondaryReprocessingService.returnSecondaryResponse(request.getRecallCode(), opID));
    }
  }
}
