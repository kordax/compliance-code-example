package com.movilizer.compliance.controller;

import com.movilizer.commons.exception.CommonAuthException;
import com.movilizer.commons.util.Credentials;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.model.ResponseError;
import com.movilizer.compliance.model.StatusRequest;
import com.movilizer.compliance.model.warehouse.WarehouseDisplayRequest;
import com.movilizer.compliance.model.warehouse.WarehouseDisplayResponse;
import com.movilizer.compliance.service.ITGInboxHelperService;
import com.movilizer.compliance.service.ThrottlingService;
import com.movilizer.compliance.service.warehouse.WarehouseInfoService;
import com.movilizer.compliance.service.warehouse.WarehouseRepairService;
import com.movilizer.compliance.util.ApiResponsesErrors;
import com.movilizer.compliance.util.WebResponseUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import java.util.UUID;

import static com.movilizer.compliance.util.ErrorCodes.AUTHORIZATION_FAILED;
import static com.movilizer.compliance.util.ErrorCodes.PROCESSING_NOTHING_TO_PROCESS;

@ApiIgnore
@RestController
@RequestMapping("/warehouse")
public class WarehouseController {
    private static final Logger LOGGER = LoggerFactory.getLogger(WarehouseController.class);
    private final MonWrapper monWrapper;
    private final ThrottlingService throttlingService;
    private final ITGInboxHelperService inboxService;
    private final WarehouseInfoService warehouseInfoService;
    private final WarehouseRepairService warehouseRepairService;

    public WarehouseController(
            MonWrapper monWrapper,
            ThrottlingService throttlingService,
            ITGInboxHelperService inboxService,
            WarehouseInfoService warehouseInfoService,
            WarehouseRepairService warehouseRepairService) {
        this.monWrapper = monWrapper;
        this.throttlingService = throttlingService;
        this.inboxService = inboxService;
        this.warehouseInfoService = warehouseInfoService;
        this.warehouseRepairService = warehouseRepairService;
    }

    @ApiOperation(
            value = "EU TPD display compliance checks in warehouse",
            response = WarehouseDisplayResponse.class,
            responseContainer = "List",
            httpMethod = "POST",
            authorizations = {@Authorization(value = "basicAuth")})
    @ApiResponsesErrors
    @PostMapping(
            value = "/checkDisplays",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity check(
            @RequestBody WarehouseDisplayRequest request,
            HttpServletRequest httpRequest) {
        final String opID = UUID.randomUUID().toString();

        Credentials credentials;
        try {
            credentials = Credentials.extractCredentials(httpRequest);
        } catch (CommonAuthException e) {
            LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
            monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
            ResponseError responseError =
                new ResponseError("Failed to extract credentials: " + e.getMessage(),
                    AUTHORIZATION_FAILED);
            return WebResponseUtils.unauthorized(responseError);
        }

        LOGGER.info("Processing warehouse check request: {}\nOperationID: {}\nDeviceAddress: {}", request, opID, credentials.deviceAddress);
        monWrapper.infoJson(request, "Processing {} request, OperationID: {}, DeviceAddress: {}", "warehouse check", opID, credentials.deviceAddress);

        ResponseEntity responseEntity = handleCheckDisplayRequest(request, opID);

        return responseEntity;

    }

    private @NotNull ResponseEntity<?> handleCheckDisplayRequest(
            @NotNull WarehouseDisplayRequest request,
            @NotNull String opID){

        long ts = System.currentTimeMillis();
        if(StringUtils.isBlank(request.getGtin()) || StringUtils.isBlank(request.getWarehouseId())){
            return WebResponseUtils.badRequest(
                    "Fields \"GTIN\" and \"Warehouse ID\" can't be empty", PROCESSING_NOTHING_TO_PROCESS, opID, ts);
        }

        WarehouseDisplayResponse warehouseDisplayResponse;

        if(request.getRepair()) {
            warehouseDisplayResponse = warehouseInfoService.getWarehouseDisplayResponse(request.getGtin(), request.getWarehouseId(), opID, true);
            if(warehouseDisplayResponse.getListOfDisplaysToFix()!=null) {
                warehouseRepairService.repairDisplays(warehouseDisplayResponse.getListOfDisplaysToFix(), opID);
            }
        }

        warehouseDisplayResponse = warehouseInfoService.getWarehouseDisplayResponse(request.getGtin(), request.getWarehouseId(), opID, false);
        return WebResponseUtils.ok(warehouseDisplayResponse);
    }

}
