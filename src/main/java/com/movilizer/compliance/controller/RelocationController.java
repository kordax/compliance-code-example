package com.movilizer.compliance.controller;

import com.movilizer.compliance.model.ReportingResponse;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.movilizer.commons.exception.CommonAuthException;
import com.movilizer.commons.service.BarcodeToEPCService;
import com.movilizer.commons.util.Credentials;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.RequestIdentifier;
import com.movilizer.compliance.model.ResponseError;
import com.movilizer.compliance.model.health.ChildUIHealthResult;
import com.movilizer.compliance.model.health.HealthResponse;
import com.movilizer.compliance.model.relocation.RelocationRequest;
import com.movilizer.compliance.model.relocation.RelocationResponse;
import com.movilizer.compliance.service.DataHelperService;
import com.movilizer.compliance.service.ITGInboxHelperService;
import com.movilizer.compliance.service.RawDataReportingService;
import com.movilizer.compliance.service.ReportingService;
import com.movilizer.compliance.service.RequestIdentifierFactory;
import com.movilizer.compliance.service.ThrottlingService;
import com.movilizer.compliance.service.health.HealthService;
import com.movilizer.compliance.service.relocation.RelocationService;
import com.movilizer.compliance.util.ApiResponsesErrors;
import com.movilizer.compliance.util.ConversionUtils;
import static com.movilizer.compliance.util.ErrorCodes.AUTHORIZATION_FAILED;
import static com.movilizer.compliance.util.ErrorCodes.PROCESSING_INTERNAL_ERROR;
import static com.movilizer.compliance.util.ErrorCodes.THROTTLING_UI_ALREADY_PROCESSED;
import com.movilizer.compliance.util.MetricName;
import com.movilizer.compliance.util.MetricTag;
import static com.movilizer.compliance.util.MetricTag.tagList;
import com.movilizer.compliance.util.WebResponseUtils;
import com.movilizer.messagerelay.api.wrapper.v1.MessageRelayEntryStatus;
import com.movilizer.messagerelay.api.wrapper.v1.MessageRelayResponse;
import com.movilizer.messagerelay.api.wrapper.v1.RelayStatusCode;
import com.movilizer.mwss.model.api.ITGInboxStep;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.apache.commons.lang3.BooleanUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping("/relocation")
public class RelocationController {

  private static final Logger LOGGER = LoggerFactory.getLogger(RelocationController.class);
  private final MonWrapper monWrapper;
  private final ITGInboxHelperService inboxService;
  private final ThrottlingService throttlingService;
  private final HealthService healthService;
  private final DataHelperService helperService;
  private final ObjectMapper objectMapper;
  private final RequestIdentifierFactory requestIdentifierFactory;
  private final RawDataReportingService rawDataReportingService;
  private final MeterRegistry registry;
  private final RelocationService relocationService;
  private final BarcodeToEPCService barcodeToEPCService;

  public RelocationController(MonWrapper monWrapper, ITGInboxHelperService inboxService, ThrottlingService throttlingService,
      HealthService healthService, DataHelperService helperService, ObjectMapper objectMapper,
      RequestIdentifierFactory requestIdentifierFactory, RawDataReportingService rawDataReportingService, MeterRegistry registry,
      RelocationService relocationService, BarcodeToEPCService barcodeToEPCService) {
    this.monWrapper = monWrapper;
    this.inboxService = inboxService;
    this.throttlingService = throttlingService;
    this.healthService = healthService;
    this.helperService = helperService;
    this.objectMapper = objectMapper;
    this.requestIdentifierFactory = requestIdentifierFactory;
    this.rawDataReportingService = rawDataReportingService;
    this.registry = registry;
    this.relocationService = relocationService;
    this.barcodeToEPCService = barcodeToEPCService;
  }

  @ApiOperation(
      value = "Relocation Post Method",
      response = RelocationResponse.class,
      httpMethod = "POST",
      authorizations = {@Authorization(value = "basicAuth")})
  @ApiResponsesErrors
  @PostMapping(
      value = "/relocate",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity relocate(
      @RequestBody RelocationRequest request,
      HttpServletRequest httpRequest)
      throws OperationException, IOException, CloneNotSupportedException {

    // TODO: Make defining of Operation ID and time - status ok!

    long requestBeginTime = System.currentTimeMillis();
    RequestIdentifier requestIdentifier = requestIdentifierFactory.createRequestIdentifier();

    Credentials credentials;
    try {
      credentials = Credentials.extractCredentials(httpRequest);
    } catch (CommonAuthException e) {
      LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
      monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
      ResponseError responseError =
          new ResponseError("Failed to extract credentials: " + e.getMessage(),
              AUTHORIZATION_FAILED);
      return WebResponseUtils.unauthorized(responseError);
    }

    LOGGER.info(
        "Processing relocation request: {}\nOperationID: {}\nDeviceAddress: {}",
        request,
        requestIdentifier.getOperationId(),
        credentials.deviceAddress);
    monWrapper.infoJson(
        request,
        "Processing {} request, OperationID: {}, DeviceAddress: {}",
        "relocation",
        requestIdentifier.getOperationId(),
        credentials.deviceAddress);

    // TODO: Check if throttling service is needed for checking processed UI's - status ok!

    Map<String, ThrottlingService.ThrottlingEntry> alreadyInProgress =
        throttlingService.handleRequest(
            request.getUis(), requestIdentifier.getOperationId(), requestIdentifier.getTimeStamp());
    if (!alreadyInProgress.isEmpty()) {
      inboxService.setStatusNok(
          requestIdentifier.getOperationId(),
          THROTTLING_UI_ALREADY_PROCESSED,
          "Some uis are already in progress");
      return WebResponseUtils.nok(
          new ResponseError(
              "Some uis are already in progress",
              THROTTLING_UI_ALREADY_PROCESSED,
              alreadyInProgress),
          HttpStatus.TOO_MANY_REQUESTS);
    }

    // TODO: return of handled request - status OK
    RelocationRequest requestObject = request.clone();
    requestObject.transformUis();
    ResponseEntity responseEntity = handleRelocationRequest(request, httpRequest.getServletPath(),
        objectMapper.writeValueAsString(requestObject),
        requestIdentifier.getOperationId(), requestIdentifier.getTimeStamp(), credentials);

    MetricName.registerTimerMetric(
        registry,
        MetricName.STINT_RELOCATION_REQUEST_TIME,
        requestBeginTime,
        tagList().append(MetricTag.OPERATION_TYPE_KEY, "relocate"));

    return responseEntity;
  }

  private @NotNull ResponseEntity<?> handleRelocationRequest(
      @NotNull RelocationRequest request,
      String action,
      String requestObject,
      @NotNull String opID,
      long ts,
      @NotNull Credentials credentials)
      throws OperationException {
    List<ReportingResponse> responseList = new ArrayList<>();
    List<String> uisToRelocate = request.getUis().stream().map(barcodeToEPCService::mapToEPC).map(ConversionUtils::convertToURN).collect(Collectors.toList());
    Map<String, HealthResponse> healthResponses = new HashMap<>(request.getUis().size());
    String errorMessage = "";
    try {
      List<String> successfullyRelocatedItems = new ArrayList<>();
      HashMap<String, Object> failed = new HashMap<>();

      long startRelocation = System.currentTimeMillis();
      long endRelocation = 0, endReportGen = 0;

      if (request.getRelocate()) {
        try {
          responseList = relocationService.relocate(
                  request.getAuthority(),
              opID,
              request.getFromLocation(),
              request.getToLocation(),
              uisToRelocate,
              request.getShipping(),
              request.getReceiving(),
              credentials);

          successfullyRelocatedItems.addAll(uisToRelocate);

        } catch (OperationException e) {
          inboxService.setStatusNok(
              opID, PROCESSING_INTERNAL_ERROR, "Failed to relocate ui: " + e.getMessage());
          LOGGER.error("Failed to relocate ui: " + opID, e);
          monWrapper.errorJson(request, "Failed to relocate ui: " + opID, e);
          throttlingService.release(request.getUis());

          failed.put(uisToRelocate.stream().findFirst().get(), e.getMessage());
          errorMessage = e.getMessage();
        }
        endRelocation = System.currentTimeMillis();

        if (!request.getSkipReport()) {
          Collection<ChildUIHealthResult> healthReportResults = new ArrayList<>();

          for (String ui : request.getUis()) {
            ChildUIHealthResult childUIHealthResult = new ChildUIHealthResult();
            childUIHealthResult.setId(ui);
            healthReportResults.add(childUIHealthResult);
          }
          helperService.fillInSupersetInfo(healthReportResults, opID);
          rawDataReportingService
              .sendHealthReport(healthReportResults, action, requestObject, ts, opID);
        } else {
          LOGGER.info("Reporting to Superset skipped [" + opID + "]");
          monWrapper.info("Reporting to Superset skipped [%s]", opID);
        }

        endReportGen = System.currentTimeMillis();

      }

      String timeMsg = String.format("Relocation processing time (ms) [%s]: relocation=%s, report generation=%s",
          opID, (endRelocation - startRelocation), (endReportGen - endRelocation));
      LOGGER.debug(timeMsg);
      monWrapper.debug(timeMsg);

      throttlingService.release(request.getUis());
      inboxService.setStatusOk(opID, ITGInboxStep.Status.SUCCESSFUL);
      if (CollectionUtils.isEmpty(responseList)) {
        RelocationResponse response = new RelocationResponse(opID, request.getUis(), ts,
            "Unsupported case: " + errorMessage, null);
        response.setRelocated(false);
        return new ResponseEntity<>(response, HttpStatus.OK);
      }
      Set<String> destinations = new HashSet<>();
      for (ReportingResponse response : responseList) {
        if (response.getRelayResponse() != null) {
          Map<String, MessageRelayEntryStatus> messageRelayEntryStatusMap = response.getRelayResponse()
              .getTargetStatus();
          destinations.addAll(messageRelayEntryStatusMap.keySet());
        }
      }
      Boolean isUKOnly = destinations.stream().allMatch(destination -> destination.contains("uk"));
      Boolean isPrimaryOnly = destinations.stream().allMatch(destination -> destination.contains("primary"));

      if (!failed.isEmpty()) {
        Map<String, Object> info = new HashMap<>();
        info.put("failedToRelocateItems", failed);
        if (successfullyRelocatedItems.size() > 0) {
          info.put("successfullyRelocatedItems", successfullyRelocatedItems);
        }
        return WebResponseUtils.internalError("Failed to relocate following uis", info, opID, ts);
      }
      RelocationResponse response = new RelocationResponse(opID, request.getUis(), ts, null, null);
      if (BooleanUtils.isFalse(request.getRelocate()) ||
          BooleanUtils.isFalse(request.getReceiving()) && BooleanUtils.isFalse(request.getShipping())) {
        response.setDetails("The combination of flags does not allow to perform any actions");
        response.setRelocated(false);
        return new ResponseEntity<>(response, HttpStatus.OK);
      }
      if (BooleanUtils.isTrue(isUKOnly)) {
        LOGGER.info("Reporting to UK only: {}", opID);
        monWrapper.info("Reporting to UK only: {}", opID);
      }
      if (BooleanUtils.isTrue(isPrimaryOnly)) {
        LOGGER.info("Reporting to Primary only: {}", opID);
        monWrapper.info("Reporting to Primary only: {}", opID);
      }
      Boolean relocated = ReportingService.isResponseOk(responseList);
      List<String> relayResponsesUK = responseList
          .stream()
          .map(ReportingResponse::getRelayResponse)
          .filter(x -> !ObjectUtils.isEmpty(x.getTargetStatus().get("uk-adapter")) && x.getTargetStatus().get("uk-adapter").getCode().equals(RelayStatusCode.FAILURE))
          .map(f -> f.getTargetStatus().get("uk-adapter").getMessage()).collect(Collectors.toList());
      List<String> relayResponsesPrimary = responseList
          .stream()
          .map(ReportingResponse::getRelayResponse)
          .filter(x -> !ObjectUtils.isEmpty(x.getTargetStatus().get("mov-primary-dev")) && x.getTargetStatus().get("mov-primary-dev").getCode().equals(RelayStatusCode.FAILURE))
          .map(f -> f.getTargetStatus().get("mov-primary-dev").getMessage()).collect(Collectors.toList());
      RelocationService.transformationRelayResponses(relayResponsesPrimary);
      RelocationService.transformationRelayResponses(relayResponsesUK);
      Map<String, List<String>> relayStatus = new HashMap<>(2);
      if (!relayResponsesUK.isEmpty()) {
        relayStatus.put("UK", relayResponsesUK);
      }
      if (!relayResponsesPrimary.isEmpty()) {
        relayStatus.put("Primary", relayResponsesPrimary);
      }
      response.setRelocated(relocated);
      response.setRelayErrorMessages(!relayStatus.isEmpty() ? relayStatus : null);
      return new ResponseEntity<>(response, HttpStatus.OK);
    } catch (Exception e) {
      LOGGER.error("Exception on processing: " + e.getMessage(), e);
      monWrapper.error("Exception on processing: " + e.getMessage(), e);
      inboxService.setStatusNok(opID, PROCESSING_INTERNAL_ERROR, e.getMessage());

      throttlingService.release(request.getUis());

      throw new OperationException(
          e, "Exception on processing: " + e.getMessage(), opID, PROCESSING_INTERNAL_ERROR);
    }
  }


}
