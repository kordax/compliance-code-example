package com.movilizer.compliance.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.movilizer.commons.exception.CommonAuthException;
import com.movilizer.commons.service.AuthService;
import com.movilizer.commons.util.Credentials;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.ResponseError;
import com.movilizer.compliance.model.StatusRequest;
import com.movilizer.compliance.model.recovery.*;
import com.movilizer.compliance.service.*;
import com.movilizer.compliance.service.health.IHealthRequestCache;
import com.movilizer.compliance.service.recovery.RecoveryService;
import com.movilizer.compliance.service.recovery.RecoveryStatusService;
import com.movilizer.compliance.util.ApiResponsesErrors;
import com.movilizer.compliance.util.MetricName;
import com.movilizer.compliance.util.MetricTag;
import com.movilizer.compliance.util.WebResponseUtils;
import com.movilizer.compliance.validation.EpcisValidator;
import com.movilizer.compliance.validation.EpcisValidatorFactory;
import com.movilizer.mwss.model.api.IntegrationUser;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import static com.movilizer.compliance.util.ErrorCodes.*;
import static com.movilizer.compliance.util.MetricTag.tagList;

@ApiIgnore
@RestController
@RequestMapping("/recovery")
public class RecoveryController {
    private static final Logger LOGGER = LoggerFactory.getLogger(RecoveryController.class);
    private final ObjectMapper objectMapper;
    private final MonWrapper monWrapper;
    private final ITGInboxHelperService inboxService;
    private final ThrottlingService throttlingService;
    private final DataHelperService helperService;
    private final RecoveryStatusService recoveryStatusService;
    private final RecoveryService recoveryService;
    private final AuthService authService;
    private final EpcisValidatorFactory epcisValidatorFactory;
    private final IHealthRequestCache healthRequestCache;
    private final RawDataReportingService rawDataReportingService;
    private final MeterRegistry registry;

    @Value("${m2mUrl}")
    private String m2mUrl;

    public RecoveryController(
            MonWrapper monWrapper,
            ITGInboxHelperService inboxService,
            ThrottlingService throttlingService,
            DataHelperService helperService,
            RecoveryStatusService recoveryStatusService,
            RecoveryService recoveryService,
            AuthService authService,
            EpcisValidatorFactory epcisValidatorFactory,
            IHealthRequestCache healthRequestCache,
            MeterRegistry registry,
            RawDataReportingService rawDataReportingService,
            ObjectMapper objectMapper) {
        this.monWrapper = monWrapper;
        this.inboxService = inboxService;
        this.throttlingService = throttlingService;
        this.helperService = helperService;
        this.recoveryStatusService = recoveryStatusService;
        this.recoveryService = recoveryService;
        this.authService = authService;
        this.epcisValidatorFactory = epcisValidatorFactory;
        this.healthRequestCache = healthRequestCache;
        this.registry = registry;
        this.rawDataReportingService = rawDataReportingService;
        this.objectMapper = objectMapper;
    }

    public static String safeListToString(List<String> list) {
        if (list == null) {
            return null;
        }
        return list.stream().collect(Collectors.joining(", "));
    }

    @ApiOperation(
            value = "EU TPD aggregating the pallets with implicitly disaggregated master cases",
            response = RecoveryResponse.class,
            responseContainer = "List",
            httpMethod = "POST",
            authorizations = {@Authorization(value = "basicAuth")})
    @ApiResponsesErrors
    @PostMapping(
            value = "/recoverPallet",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity recoverPallet(
            @RequestBody RecoveryRequest request,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse) throws OperationException {
        long requestBeginTime = System.currentTimeMillis();
        long ts = System.currentTimeMillis();
        final String opID = UUID.randomUUID().toString();

        Credentials credentials;
        try {
            credentials = Credentials.extractCredentials(httpRequest);
        } catch (CommonAuthException e) {
            LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
            monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
            ResponseError responseError =
                new ResponseError("Failed to extract credentials: " + e.getMessage(),
                    AUTHORIZATION_FAILED);
            return WebResponseUtils.unauthorized(responseError);
        }

        LOGGER.info("Processing check request: {}\nOperationID: {}\nDeviceAddress: {}", request, opID, credentials.deviceAddress);
        monWrapper.infoJson(request, "Processing {} request, OperationID: {}, DeviceAddress: {}", "check", opID, credentials.deviceAddress);

        try {
            return throttlingService.throttlableContext(request.getUis(), opID, ts, () -> {
                ResponseEntity responseEntity = handleRecoverRequest(request, httpRequest.getServletPath(),
                        objectMapper.writeValueAsString(request), opID, credentials, ts);

                MetricName.registerTimerMetric(registry,
                        MetricName.STINT_RECOVERY_REQUEST_TIME, requestBeginTime,
                        tagList().append(MetricTag.OPERATION_TYPE_KEY, "recoverPallet"));

                return responseEntity;
            });
        } catch (ThrottlingServiceImpl.UISAlreadyLockedException e) {
            inboxService.setStatusNok(
                    opID, THROTTLING_UI_ALREADY_PROCESSED, "Some uis are already in progress");
            return WebResponseUtils.nok(
                    new ResponseError(
                            "Some uis are already in progress",
                            THROTTLING_UI_ALREADY_PROCESSED,
                            e.getLockedUis()),
                    HttpStatus.TOO_MANY_REQUESTS);
        } catch (Exception e) {
            error("Exception on processing: " + e.getMessage(), e);
            inboxService.setStatusNok(opID, PROCESSING_INTERNAL_ERROR, e.getMessage());
            throw new OperationException(e, "Exception on processing: " + e.getMessage(), opID,
                    PROCESSING_INTERNAL_ERROR);
        }
    }

    public void info(String message, Object... args) {
        LOGGER.info(message, args);
        monWrapper.info(message, args);
    }

    public void error(String message, Object... args) {
        LOGGER.error(message, args);
        monWrapper.error(message, args);
    }

    @ApiOperation(
            value = "EU TPD fixing pallets with master cases having two parents",
            response = RecoveryResponse.class,
            responseContainer = "List",
            httpMethod = "POST",
            authorizations = {@Authorization(value = "basicAuth")})
    @ApiResponsesErrors
    @PostMapping(
            value = "/parentMismatchFix",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity parentMismatchFix(
            @RequestBody ParentMismatchRequest request,
            HttpServletRequest httpRequest,
            HttpServletResponse httpResponse) throws OperationException {
        long requestBeginTime = System.currentTimeMillis();
        long ts = System.currentTimeMillis();
        final String opID = UUID.randomUUID().toString();

        final Credentials credentials;
        try {
            credentials = Credentials.extractCredentials(httpRequest);
        } catch (CommonAuthException e) {
            LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
            monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
            ResponseError responseError =
                new ResponseError("Failed to extract credentials: " + e.getMessage(),
                    AUTHORIZATION_FAILED);
            return WebResponseUtils.unauthorized(responseError);
        }

        LOGGER.info("Processing check request: {}\nOperationID: {}\nDeviceAddress: {}", request, opID, credentials.deviceAddress);
        monWrapper.infoJson(request, "Processing {} request, OperationID: {}, DeviceAddress: {}", "check", opID, credentials.deviceAddress);

        try {
            return throttlingService.throttlableContext(request.getUis(), opID, ts, () -> {
                ResponseEntity responseEntity = handleParentMismatchRequest(request, httpRequest.getServletPath(),
                        objectMapper.writeValueAsString(request),
                        ts, opID, credentials);

                MetricName.registerTimerMetric(registry,
                        MetricName.STINT_RECOVERY_REQUEST_TIME, requestBeginTime,
                        tagList().append(MetricTag.OPERATION_TYPE_KEY, "parentMismatchFix"));

                return responseEntity;
            });
        } catch (ThrottlingServiceImpl.UISAlreadyLockedException e) {
            inboxService.setStatusNok(
                    opID, THROTTLING_UI_ALREADY_PROCESSED, "Some uis are already in progress");
            return WebResponseUtils.nok(
                    new ResponseError("Some uis are already in progress",
                            THROTTLING_UI_ALREADY_PROCESSED, e.getLockedUis()),
                    HttpStatus.TOO_MANY_REQUESTS);
        } catch (Exception e) {
            error("Exception on processing: " + e.getMessage(), e);
            inboxService.setStatusNok(opID, PROCESSING_INTERNAL_ERROR, e.getMessage());
            throw new OperationException(e, "Exception on processing: " + e.getMessage(),
                    opID, PROCESSING_INTERNAL_ERROR);
        }
    }

    private @NotNull ResponseEntity<?> handleRecoverRequest(
            @NotNull RecoveryRequest request,
            String action,
            String requestObject,
            @NotNull String opID,
            Credentials credentials,
            long ts)
            throws OperationException, IHealthRequestCache.UnableToGetHierarchyForUIException,
            NonTopLevelException, NoChildrenFoundException {
        healthRequestCache.invalidate();
        Map<String, RecoveryResponse> recoveryResponses = new HashMap<>(request.getUis().size());
        try {
            for (String ui : request.getUis()) {
                recoveryResponses.put(ui, recoveryStatusService.getUiStatus(request.getAuthority(), ui, opID, false));
            }
        } catch (Exception e) {
            inboxService.setStatusNok(
                    opID,
                    PROCESSING_INTERNAL_ERROR,
                    "Failed to retrieve hierarchy: " + e.getMessage());
            LOGGER.error("Failed to retrieve health: " + opID, e);
            monWrapper.errorJson(request,
                    "Failed to retrieve hierarchy for operation: " + opID, e);
            throttlingService.release(request.getUis());
            return WebResponseUtils.internalError(e.getMessage(), opID, ts);
        }

        if (request.isRecover()) {
            boolean isFixed = false;
            for (RecoveryResponse recoveryResponse : recoveryResponses.values()) {
                try {
                    if (!recoveryResponse.getBlockingErrors().isEmpty()) {
                        throw new OperationException(
                                String.format("The item contains blocking errors. Can't execute recovery for: %s, blocking errors: %s",
                                        recoveryResponse.getIdToCheck(),
                                        safeListToString(recoveryResponse.getBlockingErrors())),
                                opID, PROCESSING_INTERNAL_ERROR);
                    }
                    isFixed = recoveryService.recover(request.getAuthority(), recoveryResponse, opID, ts, credentials);
                    if (!isFixed) {
                        LOGGER.error("Failed to recover ui: " + recoveryResponse.getIdToCheck());
                        monWrapper.errorJson(request, "Failed to recover ui " + recoveryResponse.getIdToCheck());
                    }
                } catch (OperationException | IHealthRequestCache.UnableToGetHierarchyForUIException e) {
                    inboxService.setStatusNok(opID, PROCESSING_INTERNAL_ERROR,
                            "Failed to recover ui: " + e.getMessage());
                    LOGGER.error("Failed to recover ui", e);
                    monWrapper.errorJson(request, "Failed to recover ui", e);
                    return WebResponseUtils.internalError(e.getMessage(), opID, ts);
                }
            }

            recoveryResponses.clear();
            for (String ui : request.getUis()) {
                RecoveryResponse status = recoveryStatusService.getUiStatus(request.getAuthority(), ui, opID, isFixed);
                recoveryResponses.put(ui, status);
            }
        }

        rawDataReportingService.sendRecoveryReport(recoveryResponses, action, credentials.deviceAddress, requestObject, ts, opID);

        return WebResponseUtils.ok(recoveryResponses);
    }

    private @NotNull ResponseEntity<?> handleParentMismatchRequest(
            @NotNull ParentMismatchRequest request,
            String action,
            String requestObject,
            long ts,
            @NotNull String opID,
            @NotNull Credentials credentials)
            throws OperationException, IHealthRequestCache.UnableToGetHierarchyForUIException {

        healthRequestCache.invalidate();
        Map<String, RecoveryResponse> recoveryResponses = new HashMap<>(request.getUis().size());

        for (String ui : request.getUis()) {
            recoveryResponses.put(ui, recoveryStatusService.getParentMismatchStatus(request.getAuthority(), ui, opID));
        }

        if (request.getFixParentMismatchFix()) {
            info("execute-parent-mismatch-recovery is true. Trying to authorize integration user {}",
                    credentials.deviceAddress);
            IntegrationUser integrationUser =
                    authService.authIntUser(
                            m2mUrl, credentials.deviceAddress, credentials.password);
            if (integrationUser == null) {
                error("Integration user authorization failed for user {}. Please check user credentials.",
                        credentials.deviceAddress);
            } else {
                EpcisValidator epcisValidator = epcisValidatorFactory.create(integrationUser);
                if (epcisValidator != null) {
                    for (String ui : request.getUis()) {
                        recoveryService.fixParentMismatch(request.getAuthority(),
                                recoveryStatusService.checkForParentsMismatch(ui, opID),
                                opID, credentials, epcisValidator, integrationUser.getStream(), credentials.deviceAddress);
                    }
                    recoveryResponses.clear();
                    for (String ui : request.getUis()) {
                        recoveryResponses.put(ui, recoveryStatusService.getParentMismatchStatus(request.getAuthority(), ui, opID));
                    }
                } else {
                    error("Failed to run execute-parent-mismatch-recovery with {}. No epcis validator found.",
                            credentials.deviceAddress);
                }
            }
        }

        rawDataReportingService.sendRecoveryReport(recoveryResponses, action, credentials.deviceAddress, requestObject, ts, opID);

        return WebResponseUtils.ok(recoveryResponses);
    }
}
