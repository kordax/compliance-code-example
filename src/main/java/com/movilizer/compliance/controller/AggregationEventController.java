package com.movilizer.compliance.controller;

import com.movilizer.compliance.service.epc.IEpcInfoService;
import java.util.*;
import javax.servlet.http.HttpServletRequest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.movilizer.commons.exception.CommonAuthException;
import com.movilizer.commons.util.Credentials;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.ResponseError;
import com.movilizer.compliance.model.StatusResponse;
import com.movilizer.compliance.model.aggreg.AggregationByFidEventTypeRequest;
import com.movilizer.compliance.model.aggreg.AggregationEventTypeRequest;
import com.movilizer.compliance.model.aggreg.ReconstructUiRequest;
import com.movilizer.compliance.model.aggreg.ReconstructUiResponse;
import com.movilizer.compliance.model.recovery.NoChildrenFoundException;
import com.movilizer.compliance.model.recovery.NonTopLevelException;
import com.movilizer.compliance.model.recovery.RecoveryResponse;
import com.movilizer.compliance.service.ITGInboxHelperService;
import com.movilizer.compliance.service.RawDataReportingService;
import com.movilizer.compliance.service.ThrottlingService;
import com.movilizer.compliance.service.ThrottlingServiceImpl;
import com.movilizer.compliance.service.aggreg.AggregationEventTypeService;
import com.movilizer.compliance.service.aggreg.ReconstructionService;
import com.movilizer.compliance.service.health.IHealthRequestCache;
import com.movilizer.compliance.service.recovery.RecoveryStatusService;
import com.movilizer.compliance.service.tdp.TdpMessageFLService;
import com.movilizer.compliance.util.ApiResponsesErrors;
import com.movilizer.compliance.util.ConversionUtils;
import static com.movilizer.compliance.util.ErrorCodes.AUTHORIZATION_FAILED;
import static com.movilizer.compliance.util.ErrorCodes.PROCESSING_INTERNAL_ERROR;
import static com.movilizer.compliance.util.ErrorCodes.THROTTLING_UI_ALREADY_PROCESSED;
import com.movilizer.compliance.util.MetricName;
import com.movilizer.compliance.util.MetricTag;
import static com.movilizer.compliance.util.MetricTag.tagList;
import com.movilizer.compliance.util.WebResponseUtils;
import com.movilizer.hierarchy.api.wrapper.v1.Item;
import com.movilizer.masterdata.api.model.location.Location;
import groovy.lang.Tuple2;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@RestController
@RequestMapping("/aggregation")
public class AggregationEventController {
  private static final Logger LOGGER = LoggerFactory.getLogger(AggregationEventController.class);
  private final MonWrapper monWrapper;
  private final RecoveryStatusService recoveryStatusService;
  private final AggregationEventTypeService aggregationEventTypeService;
  private final IHealthRequestCache healthRequestCache;
  private final ThrottlingService throttlingService;
  private final MeterRegistry registry;
  private final RawDataReportingService rawDataReportingService;
  private final ObjectMapper objectMapper;
  private final TdpMessageFLService messageFLService;
  private final ReconstructionService reconstructionService;
  private final IEpcInfoService epcInfoServiceComplianceImpl;

  public AggregationEventController(
      MonWrapper monWrapper,
      RecoveryStatusService recoveryStatusService,
      AggregationEventTypeService aggregationEventTypeService,
      IHealthRequestCache healthRequestCache,
      ThrottlingService throttlingService,
      MeterRegistry registry,
      RawDataReportingService rawDataReportingService,
      ObjectMapper objectMapper,
      TdpMessageFLService messageFLService,
      ReconstructionService reconstructionService, IEpcInfoService epcInfoServiceComplianceImpl) {
    this.monWrapper = monWrapper;
    this.recoveryStatusService = recoveryStatusService;
    this.aggregationEventTypeService = aggregationEventTypeService;
    this.healthRequestCache = healthRequestCache;
    this.throttlingService = throttlingService;
    this.registry = registry;
    this.rawDataReportingService = rawDataReportingService;
    this.objectMapper = objectMapper;
    this.messageFLService = messageFLService;
    this.reconstructionService = reconstructionService;
    this.epcInfoServiceComplianceImpl = epcInfoServiceComplianceImpl;
  }

  @ApiOperation(
      value = "AggregationEventType generator: UIs aggregation/disaggregation",
      response = RecoveryResponse.class,
      responseContainer = "List",
      httpMethod = "POST",
      authorizations = {@Authorization(value = "basicAuth")})
  @ApiResponsesErrors
  @PostMapping(
      value = "/triggerAggregationEventType",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity triggerAggregationEventTypeRequest(
      @RequestBody AggregationEventTypeRequest request, HttpServletRequest httpRequest)
      throws OperationException {

    long requestBeginTime = System.currentTimeMillis();
    final String opID = UUID.randomUUID().toString();
    long ts = System.currentTimeMillis();

    Credentials credentials;
    try {
      credentials = Credentials.extractCredentials(httpRequest);
    } catch (CommonAuthException e) {
      LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
      monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
      ResponseError responseError =
          new ResponseError(
              "Failed to extract credentials: " + e.getMessage(), AUTHORIZATION_FAILED);
      return WebResponseUtils.unauthorized(responseError);
    }

    LOGGER.info(
        "Processing triggerAggregationEventType request: {}\nOperationID: {}\nDeviceAddress: {}",
        request,
        opID,
        credentials.deviceAddress);
    monWrapper.infoJson(
        request,
        "Processing {} request,  OperationID: {}, DeviceAddress: {}",
        "triggerAggregationEventType",
        opID,
        credentials.deviceAddress);

    try {
      return throttlingService.throttlableContext(
          request.getUis(),
          opID,
          ts,
          () -> {
            ResponseEntity responseEntity =
                handleAggregationEventTypeRequest(
                    request,
                    httpRequest.getServletPath(),
                    objectMapper.writeValueAsString(request),
                    opID,
                    credentials,
                    ts);
            MetricName.registerTimerMetric(
                registry,
                MetricName.STINT_AGGR_REQUEST_TIME,
                requestBeginTime,
                tagList().append(MetricTag.OPERATION_TYPE_KEY, "sendAggEvent"));
            return responseEntity;
          });
    } catch (ThrottlingServiceImpl.UISAlreadyLockedException e) {
      return WebResponseUtils.nok(
          new ResponseError(
              "Some uis are already in progress",
              THROTTLING_UI_ALREADY_PROCESSED,
              e.getLockedUis()),
          HttpStatus.TOO_MANY_REQUESTS);
    } catch (Exception e) {
      LOGGER.error("Exception on processing: " + e.getMessage(), e);
      monWrapper.errorJson(request, "Exception on processing: " + opID, e);
      throw new OperationException(
          e, "Exception on processing: " + e.getMessage(), opID, PROCESSING_INTERNAL_ERROR);
    }
  }

  @ApiOperation(
      value = "AggregationEventType generator: : UIs aggregation/disaggregation by FID",
      response = RecoveryResponse.class,
      responseContainer = "List",
      httpMethod = "POST",
      authorizations = {@Authorization(value = "basicAuth")})
  @ApiResponsesErrors
  @PostMapping(
      value = "/triggerAggregationByFidEventType",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity triggerAggregationByFidEventTypeRequest(
      @RequestBody AggregationByFidEventTypeRequest request, HttpServletRequest httpRequest)
      throws OperationException {

    long requestBeginTime = System.currentTimeMillis();
    final String opID = UUID.randomUUID().toString();
    long ts = System.currentTimeMillis();

    Credentials credentials;
    try {
      credentials = Credentials.extractCredentials(httpRequest);
    } catch (CommonAuthException e) {
      LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
      monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
      ResponseError responseError =
          new ResponseError(
              "Failed to extract credentials: " + e.getMessage(), AUTHORIZATION_FAILED);
      return WebResponseUtils.unauthorized(responseError);
    }

    LOGGER.info(
        "Processing triggerAggregationByFidEventType request: {}\nOperationID: {}\nDeviceAddress: {}",
        request,
        opID,
        credentials.deviceAddress);
    monWrapper.infoJson(
        request,
        "Processing {} request,  OperationID: {}, DeviceAddress: {}",
        "triggerAggregationByFidEventType",
        opID,
        credentials.deviceAddress);

    try {
      return throttlingService.throttlableContext(
          request.getUis(),
          opID,
          ts,
          () -> {
            ResponseEntity responseEntity =
                handleAggregationByFidEventTypeRequest(
                    request,
                    httpRequest.getServletPath(),
                    objectMapper.writeValueAsString(request),
                    opID,
                    credentials,
                    ts);
            MetricName.registerTimerMetric(
                registry,
                MetricName.STINT_AGGR_REQUEST_TIME,
                requestBeginTime,
                tagList().append(MetricTag.OPERATION_TYPE_KEY, "sendAggByFidEvent"));
            return responseEntity;
          });
    } catch (ThrottlingServiceImpl.UISAlreadyLockedException e) {
      return WebResponseUtils.nok(
          new ResponseError(
              "Some uis are already in progress",
              THROTTLING_UI_ALREADY_PROCESSED,
              e.getLockedUis()),
          HttpStatus.TOO_MANY_REQUESTS);
    } catch (Exception e) {
      LOGGER.error("Exception on processing: " + e.getMessage(), e);
      monWrapper.errorJson(request, "Exception on processing: " + opID, e);
      throw new OperationException(
          e, "Exception on processing: " + e.getMessage(), opID, PROCESSING_INTERNAL_ERROR);
    }
  }

  @ApiOperation(
      value = "AggregationEventType generator: UIs hierarchy re-aggregation",
      response = ReconstructUiResponse.class,
      responseContainer = "List",
      httpMethod = "POST",
      authorizations = {@Authorization(value = "basicAuth")})
  @ApiResponsesErrors
  @PostMapping(
      value = "/reconstructUi",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity reconstructUi(
      @RequestBody ReconstructUiRequest request, HttpServletRequest httpRequest)
      throws OperationException {
    long requestBeginTime = System.currentTimeMillis();
    final String opID = UUID.randomUUID().toString();
    long ts = System.currentTimeMillis();

    Credentials credentials;
    try {
      credentials = Credentials.extractCredentials(httpRequest);
    } catch (CommonAuthException e) {
      LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
      monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
      ResponseError responseError =
          new ResponseError(
              "Failed to extract credentials: " + e.getMessage(), AUTHORIZATION_FAILED);
      return WebResponseUtils.unauthorized(responseError);
    }

    LOGGER.info(
        "Processing reconstructUi request: {}\nOperationID: {}\nDeviceAddress: {}",
        request,
        opID,
        credentials.deviceAddress);
    monWrapper.infoJson(
        request,
        "Processing {} request,  OperationID: {}, DeviceAddress: {}",
        "reconstructUi",
        opID,
        credentials.deviceAddress);

    try {
      return throttlingService.throttlableContext(
          request.getUis(),
          opID,
          ts,
          () -> {
            ResponseEntity responseEntity =
                handleReconstructUiRequest(
                    request,
                    httpRequest.getServletPath(),
                    objectMapper.writeValueAsString(request),
                    opID,
                    credentials,
                    ts);
            MetricName.registerTimerMetric(
                registry,
                MetricName.STINT_AGGR_REQUEST_TIME,
                requestBeginTime,
                tagList().append(MetricTag.OPERATION_TYPE_KEY, "sendReconstructEvent"));
            return responseEntity;
          });
    } catch (ThrottlingServiceImpl.UISAlreadyLockedException e) {
      return WebResponseUtils.nok(
          new ResponseError(
              "Some uis are already in progress",
              THROTTLING_UI_ALREADY_PROCESSED,
              e.getLockedUis()),
          HttpStatus.TOO_MANY_REQUESTS);
    } catch (Exception e) {
      LOGGER.error("Exception on processing: " + e.getMessage(), e);
      monWrapper.errorJson(request, "Exception on processing: " + opID, e);
      throw new OperationException(
          e, "Exception on processing: " + e.getMessage(), opID, PROCESSING_INTERNAL_ERROR);
    }
  }

  private @NotNull ResponseEntity<?> handleAggregationEventTypeRequest(
      @NotNull AggregationEventTypeRequest request,
      String action,
      String requestObject,
      @NotNull String opID,
      Credentials credentials,
      long ts)
      throws NoChildrenFoundException, NonTopLevelException,
          IHealthRequestCache.UnableToGetHierarchyForUIException, OperationException {

    healthRequestCache.invalidate();
    Map<String, RecoveryResponse> recoveryResponses = new HashMap<>(request.getUis().size());
    for (String ui : request.getUis()) {
      final String epc = healthRequestCache.provideEpcForUi(ConversionUtils.convertToURN(ui));
      Optional<String> fid = epcInfoServiceComplianceImpl.getFid(epc, request.getAuthority(), opID);
      if (fid.isEmpty()) {
        throw new OperationException(
            "Failed to retrieve facility id for epc: " + epc, opID, PROCESSING_INTERNAL_ERROR);
      }
      Optional<Location> location = messageFLService.getLocation(fid.get());
      if (location.isEmpty()) {
        throw new OperationException(
            "Failed to retrieve location for epc: " + epc, opID, PROCESSING_INTERNAL_ERROR);
      }
      Optional<ResponseEntity> exceptionalCases =
          checkExceptionalCases(location.get(), epc, opID, ts);
      if (exceptionalCases.isPresent()) {
        return exceptionalCases.get();
      }
    }
    try {
      for (String ui : request.getUis()) {
        String epc = healthRequestCache.provideEpcForUi(ConversionUtils.convertToURN(ui));
        RecoveryResponse recoveryResponse = recoveryStatusService.getUiStatus(request.getAuthority(), epc, opID, true);
        recoveryResponses.put(ui, recoveryResponse);
      }
    } catch (Exception e) {
      LOGGER.error("Failed to retrieve health: " + opID, e);
      monWrapper.errorJson(request, "Failed to retrieve hierarchy for operation: " + opID, e);
      return WebResponseUtils.internalError(e.getMessage(), opID, ts);
    }

    if (request.getTrigger()) {
      for (RecoveryResponse recoveryResponse : recoveryResponses.values()) {
        try {
          boolean isProcessed =
              aggregationEventTypeService.triggerAggregationEventType(
                      request.getAuthority(),
                  recoveryResponse,
                  request.getTriggerDisaggregation(),
                  request.getTriggerAggregation(),
                  opID,
                  ts,
                  credentials);

          if (!isProcessed) {
            LOGGER.error(
                "Failed to process aggregationEventType for ui {}. OperationID: {}",
                recoveryResponse.getIdToCheck(),
                opID);
            monWrapper.errorJson(
                request,
                "Failed to process aggregationEventType for ui {}. Operation ID: {}",
                recoveryResponse.getIdToCheck(),
                opID);
          }
        } catch (OperationException | IHealthRequestCache.UnableToGetHierarchyForUIException e) {
          LOGGER.error("Failed to process ui", e);
          monWrapper.errorJson(request, "Failed to process ui", e);
          return WebResponseUtils.internalError(e.getMessage(), opID, ts);
        }
      }

      recoveryResponses.clear();
      for (String ui : request.getUis()) {
        String epc = healthRequestCache.provideEpcForUi(ConversionUtils.convertToURN(ui));
        RecoveryResponse recoveryResponse = recoveryStatusService.getUiStatus(request.getAuthority(), epc, opID, true);
        recoveryResponses.put(ui, recoveryResponse);
      }
    }

//    rawDataReportingService.sendRecoveryReport(
//        recoveryResponses, action, credentials.deviceAddress, requestObject, ts, opID);
    return WebResponseUtils.ok(recoveryResponses);
  }

  private @NotNull ResponseEntity<?> handleAggregationByFidEventTypeRequest(
      @NotNull AggregationByFidEventTypeRequest request,
      String action,
      String requestObject,
      @NotNull String opID,
      Credentials credentials,
      long ts)
      throws NoChildrenFoundException, NonTopLevelException,
          IHealthRequestCache.UnableToGetHierarchyForUIException, OperationException {

    healthRequestCache.invalidate();
    Map<String, RecoveryResponse> recoveryResponses = new HashMap<>(request.getUis().size());
    String facilityId = request.getLocation();
    if (StringUtils.isBlank(facilityId)) {
      throw new OperationException(
          "No facility Id provided in location parameter: ", opID, PROCESSING_INTERNAL_ERROR);
    }
    Optional<Location> location = messageFLService.getLocation(facilityId);
    if (location.isEmpty()) {
      throw new OperationException(
          "Failed to retrieve location for " + facilityId, opID, PROCESSING_INTERNAL_ERROR);
    }
    LOGGER.info(
        "Found location {} for fid {} . opID {}",
        location.get().getRomanizedName1(),
        facilityId,
        opID);
    monWrapper.info(
        "Found location {} for fid {} . opID {}",
        location.get().getRomanizedName1(),
        facilityId,
        opID);
    for (String ui : request.getUis()) {
      final String epc = healthRequestCache.provideEpcForUi(ConversionUtils.convertToURN(ui));
      Optional<ResponseEntity> exceptionalCases =
          checkExceptionalCases(location.get(), epc, opID, ts);
      if (exceptionalCases.isPresent()) {
        return exceptionalCases.get();
      }
    }
    try {
      for (String ui : request.getUis()) {
        String epc = healthRequestCache.provideEpcForUi(ConversionUtils.convertToURN(ui));
        RecoveryResponse recoveryResponse = recoveryStatusService.getUiStatus(request.getAuthority(), epc, opID, true);
        recoveryResponses.put(ui, recoveryResponse);
      }
    } catch (Exception e) {
      LOGGER.error("Failed to retrieve health: " + opID, e);
      monWrapper.errorJson(request, "Failed to retrieve hierarchy for operation: " + opID, e);
      return WebResponseUtils.internalError(e.getMessage(), opID, ts);
    }

    if (request.getTrigger()) {
      for (RecoveryResponse recoveryResponse : recoveryResponses.values()) {
        try {
          boolean isProcessed =
              aggregationEventTypeService.triggerAggregationEventType(
                      request.getAuthority(),
                  recoveryResponse,
                  request.getTriggerDisaggregation(),
                  request.getTriggerAggregation(),
                  opID,
                  ts,
                  credentials);

          if (!isProcessed) {
            LOGGER.error(
                "Failed to process aggregationEventType for ui {}. OperationID: {}",
                recoveryResponse.getIdToCheck(),
                opID);
            monWrapper.errorJson(
                request,
                "Failed to process aggregationEventType for ui {}. Operation ID: {}",
                recoveryResponse.getIdToCheck(),
                opID);
          }
        } catch (OperationException | IHealthRequestCache.UnableToGetHierarchyForUIException e) {
          LOGGER.error("Failed to process ui", e);
          monWrapper.errorJson(request, "Failed to process ui", e);
          return WebResponseUtils.internalError(e.getMessage(), opID, ts);
        }
      }

      recoveryResponses.clear();
      for (String ui : request.getUis()) {
        String epc = healthRequestCache.provideEpcForUi(ConversionUtils.convertToURN(ui));
        RecoveryResponse recoveryResponse = recoveryStatusService.getUiStatus(request.getAuthority(), epc, opID, true);
        recoveryResponses.put(ui, recoveryResponse);
      }
    }

    rawDataReportingService.sendRecoveryReport(
        recoveryResponses, action, credentials.deviceAddress, requestObject, ts, opID);
    return WebResponseUtils.ok(recoveryResponses);
  }

  private ResponseEntity handleReconstructUiRequest(
      @NotNull ReconstructUiRequest request,
      String action,
      String requestObject,
      @NotNull String opID,
      Credentials credentials,
      long ts)
      throws IHealthRequestCache.UnableToGetHierarchyForUIException, OperationException {

    if (request.getTrigger() && request.getHierarchyDepth() <= 0) {
      throw new OperationException(
          "The hierarchy depth can't be less or equal to 0: ", opID, PROCESSING_INTERNAL_ERROR);
    }

    Map<String, ReconstructUiResponse> reconstructResponses =
        new HashMap<>(request.getUis().size());

    healthRequestCache.invalidate();
    Map<String, Map<Integer, List<Tuple2<Item, StatusResponse>>>> uiDepthMap =
        new HashMap<>(request.getUis().size());
    for (String ui : request.getUis()) {
      Item hitem = healthRequestCache.provideHierarchyItem(ui);
      Integer maxDepth = healthRequestCache.getHierarchyDepth(hitem);
      // Validating hierarchy depth. If provided depth is higher than max depth then reconstruct to
      // outer level.
      Integer validatedDepth =
          request.getHierarchyDepth() >= maxDepth ? maxDepth - 1 : request.getHierarchyDepth();

      try {
        Map<Integer, List<Tuple2<Item, StatusResponse>>> itemsListMap =
                reconstructionService.prepareHierarchyLevelsMap(request.getAuthority(), hitem, validatedDepth, opID);

        reconstructResponses.put(
                ui, ReconstructionService.prepareReconstructUiResponse(ui, maxDepth, itemsListMap, opID));

        if (request.getOnlyLegacy()) {
          itemsListMap = reconstructionService.filterLegacyOnly(itemsListMap, validatedDepth);
        }
        uiDepthMap.put(ui, itemsListMap);
      }catch (OperationException e){
        return WebResponseUtils.internalError(e.getMessage(), opID, System.currentTimeMillis());
      }

    }

    if (request.getTrigger()) {
      for (String ui : request.getUis()) {
        try {
          boolean isProcessed =
              reconstructionService.reconstruct(
                      request.getAuthority(),
                  uiDepthMap.get(ui),
                  request.getHierarchyDepth(),
                  request.getTriggerDisaggregation(),
                  request.getTriggerAggregation(),
                  opID,
                  credentials);

          if (!isProcessed) {
            LOGGER.error("Failed to process reconstruction for ui {}. OperationID: {}", ui, opID);
            monWrapper.errorJson(
                request, "Failed to process reconstruction for ui {}. Operation ID: {}", ui, opID);
          } else {
            LOGGER.info("Successful reconstruction for ui {}. OperationID: {}", ui, opID);
            monWrapper.info("Successful reconstruction for ui {}. Operation ID: {}", ui, opID);
          }

          ReconstructUiResponse resultResponse = ReconstructionService.prepareReconstructUiResponse(ui, isProcessed, opID);
          reconstructResponses.put(ui, resultResponse);
        } catch (OperationException e) {
          LOGGER.error("Failed to process ui " + ui, e);
          monWrapper.errorJson(request, "Failed to process ui " + ui, e);
          ReconstructUiResponse resultResponse = ReconstructionService.prepareReconstructUiResponse(ui, false, opID);
          reconstructResponses.put(ui, resultResponse);
        }
      }
    }
    rawDataReportingService.sendReconstructionReport(
        reconstructResponses, action, credentials.deviceAddress, requestObject, ts, opID);
    return WebResponseUtils.ok(reconstructResponses);
  }

  private Optional<ResponseEntity> checkExceptionalCases(
      Location location, String epc, String opID, long ts) {
    Set<String> targetMarkets = messageFLService.getTargetMarkets(epc);
    boolean hasEuTM = messageFLService.targetMarketsAreFromEU(targetMarkets);
    boolean hasUkTM = messageFLService.targetMarketsAreFromUK(targetMarkets);
    Set<String> rskus = messageFLService.getRskus(epc);
    if (messageFLService.isFromNi(location) && !hasEuTM && !hasUkTM) {
      LOGGER.error(
          "Exceptional case: No [EU, UK] rskus resolved whereas NI is location of event. epc {}, rsku {}, opID {}",
          epc,
          rskus,
          opID);
      monWrapper.error(
          "Exceptional case: No [EU, UK] rskus resolved whereas NI is location of event. epc {}, rsku {}, opID {}",
          epc,
          rskus,
          opID);
      String message =
          String.format(
              "Exceptional case: No [EU, UK] rskus resolved whereas NI is location of event. epc %s, rsku %s",
              epc, rskus);
      return Optional.of(WebResponseUtils.internalError(message, opID, ts));
    }
    if (messageFLService.isFromGb(location) && hasEuTM && !hasUkTM) {
      LOGGER.error(
          "Exceptional case: [EU] rsku {} resolved whereas GB is location of event. epc {}, opID {}",
          rskus,
          epc,
          opID);
      monWrapper.error(
          "Exceptional case: [EU] rsku {} resolved whereas GB is location of event. epc {}, opID {}",
          rskus,
          epc,
          opID);
      String message =
          String.format(
              "Exceptional case: [EU] rsku %s resolved whereas GB is location of event. epc %s",
              rskus, epc);
      return Optional.of(WebResponseUtils.internalError(message, opID, ts));
    }
    return Optional.empty();
  }
}
