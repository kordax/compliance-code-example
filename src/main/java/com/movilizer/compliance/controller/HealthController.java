package com.movilizer.compliance.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.movilizer.commons.exception.CommonAuthException;
import com.movilizer.commons.service.AuthService;
import com.movilizer.commons.service.LocationManager;
import com.movilizer.commons.util.Credentials;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.converters.EnumsConverter;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.GenericResponse;
import com.movilizer.compliance.model.ResponseError;
import com.movilizer.compliance.model.StatusResponse;
import com.movilizer.compliance.model.health.*;
import com.movilizer.compliance.service.*;
import com.movilizer.compliance.service.health.HealthService;
import com.movilizer.compliance.service.health.IHealthRequestCache;
import com.movilizer.compliance.service.health.RepairService;
import com.movilizer.compliance.service.recovery.RecoveryStatusService;
import com.movilizer.compliance.util.ApiResponsesErrors;
import com.movilizer.compliance.util.MetricName;
import com.movilizer.compliance.util.WebResponseUtils;
import com.movilizer.mwss.model.api.ITGInboxStep;
import com.movilizer.mwss.model.api.MobileUser;
import com.movilizer.ps.itg.compliance.restclient.model.Authority;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.apache.commons.lang.BooleanUtils;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import springfox.documentation.annotations.ApiIgnore;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.*;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

import static com.movilizer.compliance.util.ErrorCodes.*;
import static com.movilizer.compliance.util.MetricTag.*;
import static java.util.stream.Collectors.toMap;

@ApiIgnore
@RestController
@RequestMapping("/health")
public class HealthController {
    private static final Logger LOGGER = LoggerFactory.getLogger(HealthController.class);
    private final MonWrapper monWrapper;
    @Autowired
    public IHealthRequestCache healthRequestCache;
    @Autowired
    LocationManager locationManager;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MeterRegistry registry;

    @Value("${m2mUrl}")
    private String m2mUrl;

    private HealthService healthService;
    private RepairService repairService;
    private ITGInboxHelperService inboxService;
    @Autowired
    private ThrottlingService throttlingService;
    @Autowired
    private AuthService authService;
    @Autowired
    private RawDataReportingService rawDataReportingService;
    @Autowired
    private DataHelperService helperService;
    @Autowired
    private TaskExecutor taskExecutor;
    @Autowired
    private RecoveryStatusService recoveryStatusService;

    public HealthController(
            MonWrapper monWrapper,
            HealthService healthService,
            RepairService repairService,
            ITGInboxHelperService inboxService) {
        this.monWrapper = monWrapper;
        this.healthService = healthService;
        this.repairService = repairService;
        this.inboxService = inboxService;
    }

    @ApiOperation(
            value = "EU TPD commissioning message from aggregation history",
            response = GenericResponse.class,
            responseContainer = "List",
            httpMethod = "POST",
            authorizations = {@Authorization(value = "basicAuth")})
    @ApiResponsesErrors
    @PostMapping(
            value = "/checkMobile",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity checkMobile(
            @RequestBody HealthMobileRequest request,
            HttpServletRequest httpRequest) {
        long requestBeginTime = System.currentTimeMillis();
        final String opID = UUID.randomUUID().toString();
        long ts = System.currentTimeMillis();

        Credentials credentials;
        try {
            credentials = Credentials.extractCredentials(httpRequest);
        } catch (CommonAuthException e) {
            LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
            monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
            ResponseError responseError =
                    new ResponseError("Failed to extract credentials: " + e.getMessage(),
                            AUTHORIZATION_FAILED);
            return WebResponseUtils.unauthorized(responseError);
        }
        MobileUser mobileUser =
                authService.authMobileUser(m2mUrl, credentials.deviceAddress, credentials.password);
        if (mobileUser == null) {
            return WebResponseUtils.unauthorized(
                    "Unable to retrieve mobile user configuration", AUTHORIZATION_FAILED, opID, ts);
        }
        if (mobileUser.getLocation() == null || mobileUser.getLocation().isBlank()) {
            return WebResponseUtils.unauthorized(
                    "Unable to retrieve mobile user location", AUTHORIZATION_FAILED, opID, ts);
        }

        if (inboxService.createEntryInInbox(opID, ts, mobileUser) == null) {
            ResponseError responseError =
                    new ResponseError("Inbox saving issues", PROCESSING_INBOX_ERROR);
            return WebResponseUtils.nok(responseError, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        LOGGER.info("Processing check request: {}\nOperationID: {}\nDeviceAddress: {}", request, opID, credentials.deviceAddress);
        monWrapper.infoJson(request, "Processing {} request,  OperationID: {}, DeviceAddress: {}", "checkMobile", opID, credentials.deviceAddress);

        if (request.getUis().values().stream().noneMatch(ui -> !ui.isBlank())) {
            LOGGER.error("No uis to process:  {}", opID);
            monWrapper.errorJson(request, "No uis to process: {}", opID);

            inboxService.setStatusNok(opID, PROCESSING_NOTHING_TO_PROCESS, "No uis to process");
            WebResponseUtils.badRequest(
                    "No uis to process", PROCESSING_NOTHING_TO_PROCESS, opID, ts);
        }

        final HealthRequest transformedRequest =
                new HealthRequest(
                        new ArrayList<>(request.getUis().values()),
                        request.isRepair(),
                        request.getCommissioning(),
                        request.getVirtualShipment(),
                        false);

        boolean locked = transformedRequest.getUis().stream()
                .anyMatch(throttlingService::isUIInProgress);
        if (locked) {
            inboxService.setStatusNok(
                    opID, THROTTLING_UI_ALREADY_PROCESSED, "Some uis are already in progress");
            return WebResponseUtils.ok(
                    new ResponseError(
                            "Some uis are already in progress",
                            THROTTLING_UI_ALREADY_PROCESSED,
                            transformedRequest.getUis().stream()
                                    .filter(throttlingService::isUIInProgress)
                                    .collect(Collectors.toMap(
                                            ui -> ui,
                                            ui -> throttlingService.getInProgress().get(ui))))
            );
        }

        try {
            Future resultFuture = throttlingService.throttlableContextAsync(transformedRequest.getUis(), opID, ts, () ->
                    handleHealthRequest(transformedRequest, httpRequest.getServletPath(),
                            objectMapper.writeValueAsString(request),
                            opID, ts, credentials));
            taskExecutor.execute(() -> {
                try {
                    resultFuture.get();
                } catch (Exception ex) {
                    inboxService.setStatusNok(opID, THROTTLING_UI_ALREADY_PROCESSED,
                            "Unable to process health request for mobile check due to error: " + ex.getMessage());
                    monWrapper.errorJson(request, "Unable to process health request for mobile check", ex);
                    LOGGER.error("Unable to process health request for mobile check", ex);
                }
            });

        } catch (ThrottlingServiceImpl.UISAlreadyLockedException ex) {
            inboxService.setStatusNok(
                    opID, THROTTLING_UI_ALREADY_PROCESSED, "Some uis are already in progress");
            monWrapper.errorJson(request, "Unable to process health request for mobile check", ex);
            throw new RuntimeException("Some uis are already in progress", ex);
        }

        TagList tagList = tagList()
                .append(MOBILE, true)
                .append(REPAIRING, BooleanUtils.isTrue(request.isRepair()))
                .append(COMMISSIONING, BooleanUtils.isTrue(request.getCommissioning()))
                .append(SHIPMENT, BooleanUtils.isTrue(request.getVirtualShipment()));

        MetricName.registerTimerMetric(registry,
                MetricName.STINT_HEALTH_CHECK_REQUEST_TIME, requestBeginTime, tagList);

        return WebResponseUtils.ok("Request succeeded", "000000", opID, ts);
    }

    @ApiOperation(
            value = "Release uis, held by throttling locker",
            response = String.class,
            responseContainer = "List",
            httpMethod = "POST",
            authorizations = {@Authorization(value = "basicAuth")})
    @ApiResponsesErrors
    @PostMapping(
            value = "/release-uis",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity releaseUIS(
            @RequestBody ThrottlingReleaseRequest request) {
        List<Map<String, Object>> results = new ArrayList<>();
        if (request != null && request.getUisToRelease() != null) {
            for (String ui : request.getUisToRelease()) {
                Map<String, Object> uiReleaseResult = new HashMap<>();
                boolean locked = throttlingService.isUIInProgress(ui);
                if (locked) {
                    throttlingService.release(List.of(ui));
                }
                uiReleaseResult.put("ui", ui);
                uiReleaseResult.put("was-locked", locked);
                results.add(uiReleaseResult);
            }
        }
        return WebResponseUtils.ok(results);
    }

    @ApiOperation(
            value = "EU TPD commissioning message from aggregation history",
            response = HealthResponse.class,
            responseContainer = "List",
            httpMethod = "POST",
            authorizations = {@Authorization(value = "basicAuth")})
    @ApiResponsesErrors
    @PostMapping(
            value = "/check",
            consumes = {MediaType.APPLICATION_JSON_VALUE},
            produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity check(
            @RequestBody @Valid HealthRequest request,
            HttpServletRequest httpRequest) {
        long requestBeginTime = System.currentTimeMillis();
        long ts = System.currentTimeMillis();
        final String opID = UUID.randomUUID().toString();

        Credentials credentials;
        try {
            credentials = Credentials.extractCredentials(httpRequest);
        } catch (CommonAuthException e) {
            LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
            monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
            ResponseError responseError =
                    new ResponseError("Failed to extract credentials: " + e.getMessage(),
                            AUTHORIZATION_FAILED);
            return WebResponseUtils.unauthorized(responseError);
        }

        LOGGER.info("Processing check request: {}\nOperationID: {}\nDeviceAddress: {}", request, opID, credentials.deviceAddress);
        monWrapper.infoJson(request, "Processing {} request,  OperationID: {}, DeviceAddress: {}", "check", opID, credentials.deviceAddress);

        try {
            HealthRequest requestObject = request.clone();
            requestObject.transformUis();
            return throttlingService.throttlableContext(request.getUis(), opID, ts, () -> {
                ResponseEntity responseEntity = handleHealthRequest(request, httpRequest.getServletPath(),
                        objectMapper.writeValueAsString(requestObject), opID, ts, credentials);

                TagList tagList = tagList()
                        .append(MOBILE, false)
                        .append(REPAIRING, BooleanUtils.isTrue(request.isRepair()))
                        .append(COMMISSIONING, BooleanUtils.isTrue(request.getCommissioning()))
                        .append(SHIPMENT, BooleanUtils.isTrue(request.getVirtualShipment()));

                MetricName.registerTimerMetric(registry,
                        MetricName.STINT_HEALTH_CHECK_REQUEST_TIME, requestBeginTime, tagList);

                return responseEntity;
            });
        } catch (ThrottlingServiceImpl.UISAlreadyLockedException e) {
            inboxService.setStatusNok(
                    opID, THROTTLING_UI_ALREADY_PROCESSED, "Some uis are already in progress");
            return WebResponseUtils.nok(
                    new ResponseError(
                            "Some uis are already in progress",
                            THROTTLING_UI_ALREADY_PROCESSED,
                            e.getLockedUis()),
                    HttpStatus.TOO_MANY_REQUESTS);
        } catch (Exception e) {
            if (e.getCause() instanceof OperationException) {
                OperationException opEx = (OperationException) e.getCause();
                error("Exception on processing: " + opEx.getMessage(), opEx);
                inboxService.setStatusNok(opID, PROCESSING_INTERNAL_ERROR, opEx.getMessage());
                return WebResponseUtils.internalError(new ResponseError(opEx.getMessage(),
                        opEx.getCode(), Map.of("operation_id", opEx.getOpID(), "timestamp", ts)));
            } else {
                error("Exception on processing: " + e.getMessage(), e);
                inboxService.setStatusNok(opID, PROCESSING_INTERNAL_ERROR, e.getMessage());
                return WebResponseUtils.internalError(new ResponseError(e.getMessage(), PROCESSING_INTERNAL_ERROR));
            }
        }
    }

    @InitBinder
    public void initBinder(final WebDataBinder webdataBinder) {
        webdataBinder.registerCustomEditor(StatusResponse.State.class, new EnumsConverter());
    }

    public void info(String message, Object... args) {
        LOGGER.info(message, args);
        monWrapper.info(message, args);
    }

    public void error(String message, Object... args) {
        LOGGER.error(message, args);
        monWrapper.error(message, args);
    }

    private @NotNull ResponseEntity<?> handleHealthRequest(
            @NotNull HealthRequest request,
            String action,
            String requestObject,
            @NotNull String opID,
            long ts,
            @NotNull Credentials credentials)
            throws OperationException, IHealthRequestCache.UnableToGetHierarchyForUIException {
        Map<String, HealthResponse> healthResponses = new HashMap<>(request.getUis().size());
        Map<String, HealthResponse> healthResponsesForReport = new HashMap<>(request.getUis().size());
        healthRequestCache.invalidate();

        for (String ui : request.getUis()) {
            try {
                healthResponses.put(ui, healthService.getHealth(ui, request.getAuthority(), opID, ts));

                if (recoveryStatusService.getAncestorsMap(ui, opID).isEmpty()) {
                    info("No parent mismatch detected for ui: " + ui);
                } else {
                    info("Parent mismatch detected for ui: " + ui);
                }
                if (request.isRepair()) {
                    repairService.repair(
                            healthResponses.get(ui).getResults(),
                            request.getAuthority(),
                            opID,
                            request.getCommissioning(),
                            request.getVirtualShipment(),
                            request.getProcessActivated(),
                            credentials);
                    healthResponsesForReport.put(ui, healthService.getHealth(ui, request.getAuthority(), opID, ts));
                } else {
                    healthResponsesForReport.put(ui, healthResponses.get(ui));
                }
            } catch (Exception ex) {
                HealthResponse response = healthResponses.get(ui);
                if (response == null) {
                    response = new HealthResponse();
                    response.setCompliant(false);
                    response.setParentId(ui);
                    response.setOperationId(opID);
                    response.setTimestamp(String.valueOf(ts));
                }

                ChildUIHealthResult newResult = new ChildUIHealthResult(ui, ui, true);
                newResult.setContainsError(true);
                response.setResults(new HashMap<String, ChildUIHealthResult>() {{
                    put(ui, newResult);
                }});
                response.addErrorMessage(
                        new ErrorMessage(ErrorMessage.ErrorType.PROCESSING_INTERNAL_ERROR, ex.getMessage()));
                healthResponsesForReport.put(ui, response);
                LOGGER.error("Error repair ui: {} because {}", ui, ex.getMessage());
                monWrapper.errorJson(ex, "Error repair ui: {} because {}", ui, ex.getMessage(), ex);
            }
        }

        LinkedHashMap<String, HealthResponse> healthResponsesSortedByCompliant = healthResponsesForReport.entrySet().stream()
                .sorted(Map.Entry.comparingByValue(new Comparator<HealthResponse>() {
                    @Override
                    public int compare(HealthResponse o1, HealthResponse o2) {
                        return o2.getCompliant().compareTo(o1.getCompliant());
                    }
                }))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                        (e1, e2) -> e1, LinkedHashMap::new));

        // Processing health report per each ui provided
        Map<String, HealthResponse> healthResponsesSortedByCompliantForReport = new LinkedHashMap<>();
        for (HealthResponse response : healthResponsesSortedByCompliant.values()) {
            String ui = response.getResults().keySet().stream().findFirst().get();
            response.getResults().values().forEach(r -> r.setUser(credentials.deviceAddress));
            try {
                response = healthService.createHealthReport(request, response);
                Collection<ChildUIHealthResult> healthReportResults =
                        response.getResults().values();
                helperService.fillInSupersetInfo(healthReportResults, opID);

                rawDataReportingService.sendHealthReport(healthReportResults, action, requestObject, ts, opID);
            } catch (Exception e) {
                LOGGER.warn("Failed to report UI health! Please check the ui: {}[{}]", ui, opID);
                monWrapper.warn(
                        "Failed to report UI health! Please check the ui: {}[{}]", ui, opID);
            }

            healthResponsesSortedByCompliantForReport.put(ui, response);
        }

        boolean compliant = Optional.ofNullable(healthResponsesSortedByCompliantForReport)
                .map(responses -> responses.values())
                .map(values -> values.iterator())
                .map(iterator -> iterator.hasNext() ? iterator.next() : null)
                .map(healthResponse -> healthResponse.getCompliant()).get();
        if (compliant) {
            inboxService.setStatusOk(opID, ITGInboxStep.Status.SUCCESSFUL);
        } else {
            inboxService.setStatusNok(opID, PROCESSING_NOTHING_TO_PROCESS,
                    "The item is still non-comlpliant after repairing");
        }
        return WebResponseUtils.ok(healthResponsesSortedByCompliantForReport);
    }
}
