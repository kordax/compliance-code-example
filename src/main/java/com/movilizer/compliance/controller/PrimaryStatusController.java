package com.movilizer.compliance.controller;

import com.movilizer.commons.exception.CommonAuthException;
import com.movilizer.commons.util.Credentials;
import com.movilizer.compliance.constants.PrimaryServiceConstants;
import com.movilizer.compliance.util.WebResponseUtils;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;

import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.converters.EnumsConverter;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.PrimaryStatusRequest;
import com.movilizer.compliance.model.ResponseError;
import com.movilizer.compliance.model.StatusMobileRequest;
import com.movilizer.compliance.model.StatusRequest;
import com.movilizer.compliance.model.StatusResponse;
import static com.movilizer.compliance.model.StatusResponse.Fields.idToCheck;
import com.movilizer.compliance.model.health.ChildUIHealthResult;
import com.movilizer.compliance.model.health.HealthResponse;
import com.movilizer.compliance.model.status.FilteredForStatusItemRequest;
import com.movilizer.compliance.service.HealthStatusHierarchyTraversingStatusFilter;
import com.movilizer.compliance.service.HierarchyItemLevelResolver;
import com.movilizer.compliance.service.RESTService;
import com.movilizer.compliance.service.health.HealthService;
import com.movilizer.compliance.service.health.IHealthRequestCache;
import com.movilizer.compliance.util.ApiResponsesErrors;
import com.movilizer.compliance.util.Constants;
import com.movilizer.compliance.util.MetricName;
import com.movilizer.compliance.util.MetricTag;

import static com.movilizer.compliance.util.ErrorCodes.AUTHORIZATION_FAILED;
import static com.movilizer.compliance.util.MetricTag.tagList;
import io.micrometer.core.instrument.MeterRegistry;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import javax.servlet.http.HttpServletRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

@ApiIgnore
@RestController
@RequestMapping("/status")
public class PrimaryStatusController {

  private static final Logger LOGGER = LoggerFactory.getLogger(PrimaryStatusController.class);

  @Autowired private MonWrapper monWrapper;

  @Autowired private IHealthRequestCache healthRequestCache;

  @Autowired private MeterRegistry registry;

  @Autowired private RESTService restService;

  @Autowired private HealthService healthService;

  @Autowired
  private HealthStatusHierarchyTraversingStatusFilter healthStatusHierarchyTraversingStatusFilter;

  private static final Integer MAX_RETRIES = 3;

  @ApiOperation(
      value = "Find items in hierarchy for their primary status",
      response = String.class,
      responseContainer = "List",
      httpMethod = "POST",
      authorizations = {@Authorization(value = "basicAuth")})
  @ApiResponsesErrors
  @PostMapping(
      value = "/getItemsForStatuses",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity getHealthForStates(
      @RequestBody FilteredForStatusItemRequest statusRequest, HttpServletRequest httpRequest)
      throws OperationException {

    final String opID = UUID.randomUUID().toString();
    long ts = System.currentTimeMillis();

    final Credentials credentials;
    try {
      credentials = Credentials.extractCredentials(httpRequest);
    } catch (CommonAuthException e) {
      LOGGER.error("Failed to extract credentials for request {}: {}", statusRequest, e);
      monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", statusRequest, e);
      ResponseError responseError =
          new ResponseError(
              "Failed to extract credentials: " + e.getMessage(), AUTHORIZATION_FAILED);
      return WebResponseUtils.unauthorized(responseError);
    }

    LOGGER.info(
        "Processing getItemsForStatuses request: {}\nOperationID: {}\nDeviceAddress: {}",
        statusRequest,
        opID,
        credentials.deviceAddress);
    monWrapper.infoJson(
        statusRequest,
        "Processing {} request, OperationID: {}, DeviceAddress: {}",
        "getItemsForStatuses",
        opID,
        credentials.deviceAddress);

    Set<String> resultIds = new HashSet<>();
    healthRequestCache.invalidate();
    String found = statusRequest.getFound();
    String currentFid = statusRequest.getCurrentFID();

    for (String ui : statusRequest.getUis()) {
      StatusResponse.State[] states =
          statusRequest.getQueryStatus().toArray(new StatusResponse.State[0]);
      HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel[] levels =
          statusRequest
              .getQueryPackagingLevel()
              .toArray(new HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel[0]);

      HealthResponse response = healthService.getHealth(ui, statusRequest.getAuthority(), opID, ts);
      Map<String, ChildUIHealthResult> results = response.getResults();
      for (Map.Entry<String, ChildUIHealthResult> entry : results.entrySet()) {
        Collection<ChildUIHealthResult> flatResults =
            healthStatusHierarchyTraversingStatusFilter.flattenHealthResults(entry.getValue());
        flatResults =
            flatResults.stream()
                .filter(
                    result ->
                        healthStatusHierarchyTraversingStatusFilter.itemMeetsLevel(result, levels))
                .filter(
                    result ->
                        healthStatusHierarchyTraversingStatusFilter.itemMeetsState(result, states))
                .filter(
                    result ->
                        healthStatusHierarchyTraversingStatusFilter.itemMeetsFound(result, found))
                .filter(
                    result ->
                        healthStatusHierarchyTraversingStatusFilter.itemMeetsCurrentFid(
                            result, currentFid))
                .collect(Collectors.toList());
        flatResults.forEach(res -> resultIds.add(res.getId()));
      }
    }

    return ResponseEntity.ok(resultIds);
  }

  @ApiOperation(
      value = "get compliance status from primary",
      response = StatusResponse.class,
      responseContainer = "List",
      httpMethod = "POST",
      authorizations = {@Authorization(value = "basicAuth")})
  @ApiResponsesErrors
  @PostMapping(
      value = "/getStatusMobile",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity getStatusMobile(
      @RequestBody StatusMobileRequest request, HttpServletRequest httpRequest) {
    StatusRequest transformedRequest =
        new StatusRequest(new ArrayList<>(request.getUis().values()), request.isTransform());

    return getStatus(transformedRequest, httpRequest);
  }

  @ApiOperation(
      value = "get compliance status from primary",
      response = StatusResponse.class,
      responseContainer = "List",
      httpMethod = "POST",
      authorizations = {@Authorization(value = "basicAuth")})
  @ApiResponsesErrors
  @PostMapping(
      value = "/getStatus",
      consumes = {MediaType.APPLICATION_JSON_VALUE},
      produces = {MediaType.APPLICATION_JSON_VALUE})
  public ResponseEntity getStatus(
      @RequestBody StatusRequest request, HttpServletRequest httpRequest) {
    Credentials credentials;
    try {
      credentials = Credentials.extractCredentials(httpRequest);
    } catch (CommonAuthException e) {
      LOGGER.error("Failed to extract credentials for request {}: {}", request, e);
      monWrapper.errorJson(e, "Failed to extract credentials for request {}: {}", request, e);
      ResponseError responseError =
              new ResponseError(
                      "Failed to extract credentials: " + e.getMessage(), AUTHORIZATION_FAILED);
      return WebResponseUtils.unauthorized(responseError);
    }
    LOGGER.info(
            "Processing getStatus request: {}\nDeviceAddress: {}", request, credentials.deviceAddress);
    monWrapper.infoJson(
            request,
            "Processing {} request,  DeviceAddress: {}",
            "getStatus",
            credentials.deviceAddress);
    Map<String, Map<String, String>> convertedResponse = null;
    int attempt = 0;
    while (true) {
      Exception primaryException = null;
      try {
        long requestBeginTime = System.currentTimeMillis();
        ResponseEntity<? extends List> result =
                restService.postPrimaryInterface(
                        new PrimaryStatusRequest(request),
                        List.class,
                        PrimaryServiceConstants.POST_UI_STATUS);
        MetricName.registerTimerMetric(
                registry,
                MetricName.STINT_PRIMARY_RESPONSE_TIME,
                requestBeginTime,
                tagList().append(MetricTag.OPERATION_TYPE_KEY, "getStatus"));
        LOGGER.debug("Received response from primary");
        monWrapper.debugJson(request, "Received response from primary");

        if (result.getBody() != null && !result.getBody().isEmpty()) {
          convertedResponse = new HashMap<>();
          for (Map<String, String> map : (List<Map<String, String>>) result.getBody()) {
            convertedResponse.put(map.get(idToCheck.name()), map);
          }
        }
        break;
      } catch (Exception e) {
        LOGGER.warn("Failed to get status on {} attempt", attempt);
        monWrapper.warnJson(request, "Failed to get status on {} attempt", attempt);
        primaryException = e;
      }
      if (++attempt == MAX_RETRIES) {
        LOGGER.error("Failed to get status", primaryException);
        monWrapper.errorJson(request, "Failed to get status", primaryException);

        return new ResponseEntity<>(
                new ResponseError(
                        primaryException.getMessage(), Constants.RESP_ERROR_COMMON_CODE, Collections.emptyMap()),
                HttpStatus.INTERNAL_SERVER_ERROR);
      }
      try {
        Thread.sleep(1000);
      } catch (InterruptedException e) {
        LOGGER.error("Thread was being aborted");
        monWrapper.error("Thread was being aborted");
      }
    }
    if (convertedResponse == null || convertedResponse.isEmpty()) {
      LOGGER.info("No results found");
      monWrapper.debugJson(request, "No results found for uis");
      return new ResponseEntity<>(
              new ResponseError(
                      "Nothing was found", Constants.RESP_ERROR_COMMON_CODE, Collections.emptyMap()),
              HttpStatus.NOT_FOUND);
    }
    return new ResponseEntity<>(convertedResponse, HttpStatus.OK);

  }

  @InitBinder
  public void initBinder(final WebDataBinder webdataBinder) {
    webdataBinder.registerCustomEditor(StatusResponse.State.class, new EnumsConverter());
  }
}
