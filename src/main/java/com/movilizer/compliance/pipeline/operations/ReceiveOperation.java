package com.movilizer.compliance.pipeline.operations;

import static com.movilizer.compliance.util.ErrorCodes.PROCESSING_INTERNAL_ERROR;

import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.ReportingResponse;
import com.movilizer.compliance.pipeline.Operation;
import com.movilizer.compliance.pipeline.OperationContext;
import com.movilizer.compliance.pipeline.OperationResult;
import com.movilizer.compliance.service.ComplianceMessageBuilder;
import com.movilizer.compliance.service.ReportingService;
import com.movilizer.ps.itg.epcis.BusinessTransactionListType;
import com.movilizer.ps.itg.epcis.BusinessTransactionType;
import com.movilizer.ps.itg.epcis.ObjectEventType;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;
import org.springframework.util.CollectionUtils;;

@SuperBuilder
public class ReceiveOperation extends Operation {
  private final ComplianceMessageBuilder complianceMessageBuilder;
  private final ObjectEventType dispatchEvent;
  private final ReportingService reportingService;

  @Override
  public CompletableFuture<OperationResult> execute(@NotNull OperationContext context) {
    return CompletableFuture.supplyAsync(() -> {
      ObjectEventType receivingEvent;
      try {
        receivingEvent = complianceMessageBuilder.createReceivingEventFromDispatch(
            dispatchEvent, System.currentTimeMillis(), context.getOpID());
      } catch (OperationException e) {
        return OperationResult.builder()
            .exception(e)
            .context(context)
            .opClazz(ReceiveOperation.class)
            .build();
      }

      try {
        final List<ReportingResponse> receiveResponseList;
        final List<ReportingResponse> responseList = new ArrayList<>();
        receiveResponseList = reportingService.reportEvent(context.getAuthority(), receivingEvent, context.getOpID(), context.getCredentials());

        if (!CollectionUtils.isEmpty(receiveResponseList)) {
          responseList.addAll(receiveResponseList);
        }

        if (next == null) {
          return OperationResult.builder()
              .responses(responseList)
              .opClazz(ReceiveOperation.class)
              .context(context)
              .build();
        } else {
          return next.execute(context).get();
        }
      } catch (Exception e) {
        return OperationResult.builder()
            .exception(new OperationException(
                String.format(
                    "Failed to run receive for destination location %s due to error : %s",
                    resolveDispatchEventDestinationLocation(receivingEvent), e.getMessage()),
                context.getOpID(),
                PROCESSING_INTERNAL_ERROR)
            )
            .context(context)
            .opClazz(ReceiveOperation.class)
            .build();
      }
    });
  }

  protected String resolveDispatchEventDestinationLocation(ObjectEventType receivingEvent) {
    return Optional.ofNullable(receivingEvent)
        .map(ObjectEventType::getBizTransactionList)
        .map(BusinessTransactionListType::getBizTransaction)
        .map(transactionType -> transactionType.isEmpty() ? null : transactionType.get(0))
        .map(BusinessTransactionType::getValue)
        .orElse("null");
  }
}
