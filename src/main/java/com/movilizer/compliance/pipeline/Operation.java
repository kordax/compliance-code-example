package com.movilizer.compliance.pipeline;

import java.util.concurrent.CompletableFuture;
import lombok.experimental.SuperBuilder;
import org.jetbrains.annotations.NotNull;

@SuperBuilder
public abstract class Operation {
  protected Operation next;

  /**
   * Execute operation and return the latest OperationResult.
   *
   * @param context operation context
   * @return Future with operation result
   */
  public abstract CompletableFuture<OperationResult> execute(
      @NotNull OperationContext context
  );

  /**
   * Add next Operation in chain.
   *
   * @param next next operation
   * @param <T> extends this class
   * @return ref to next instance
   */
  public <T extends Operation> T addNext(T next) {
    this.next = next;

    return next;
  }
}
