package com.movilizer.compliance.pipeline;

import com.movilizer.commons.util.Credentials;
import com.movilizer.ps.itg.compliance.restclient.model.Authority;
import lombok.Builder;
import lombok.Getter;

@Builder
@Getter
public class OperationContext {
  private final Authority authority;
  private final String opID;
  private final Credentials credentials;
}
