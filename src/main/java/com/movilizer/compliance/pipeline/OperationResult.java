package com.movilizer.compliance.pipeline;

import com.movilizer.compliance.model.ReportingResponse;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Singular;

@Builder
@Getter
public class OperationResult {
  @Singular
  private final List<ReportingResponse> responses;
  @Singular
  private final List<Throwable> exceptions;
  private final String errorDescription;
  private final Class<? extends Operation> opClazz;
  private final OperationContext context;
}
