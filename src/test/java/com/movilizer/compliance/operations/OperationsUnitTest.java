package com.movilizer.compliance.operations;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import com.movilizer.commons.repository.RegexIdRepository;
import com.movilizer.commons.service.BarcodeToEPCService;
import com.movilizer.commons.service.EPCToBarcodeService;
import com.movilizer.commons.service.IdUtils;
import com.movilizer.commons.util.Credentials;
import com.movilizer.commons.util.EpcisUtils;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.AbstractUnitTestTest;
import com.movilizer.compliance.config.WebMvcConfiguration.Auth;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.internal.utils.EventUtils;
import com.movilizer.compliance.model.ReportingResponse;
import com.movilizer.compliance.pipeline.Operation;
import com.movilizer.compliance.pipeline.OperationContext;
import com.movilizer.compliance.pipeline.OperationResult;
import com.movilizer.compliance.pipeline.operations.ReceiveOperation;
import com.movilizer.compliance.service.ComplianceMessageBuilder;
import com.movilizer.compliance.service.ReportingService;
import com.movilizer.compliance.service.health.HealthService;
import com.movilizer.compliance.util.EpcisXmlUtils;
import com.movilizer.messagerelay.api.wrapper.v1.MessageRelayEntryStatus;
import com.movilizer.messagerelay.api.wrapper.v1.MessageRelayResponse;
import com.movilizer.messagerelay.api.wrapper.v1.RelayStatusCode;
import com.movilizer.mwss.model.api.Epcis.BizTransaction;
import com.movilizer.ps.itg.compliance.restclient.model.Authority;
import com.movilizer.ps.itg.epcis.EPCISDocumentType;
import com.movilizer.ps.itg.epcis.ObjectEventType;
import com.movilizer.ps.itg.epcis.ObjectFactory;
import java.io.FileNotFoundException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ExecutionException;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.parsers.ParserConfigurationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

@TestInstance(Lifecycle.PER_CLASS)
public class OperationsUnitTest extends AbstractUnitTestTest {
  @Mock
  RegexIdRepository regexIdRepository;
  @Mock
  MonWrapper monWrapper;
  @Mock
  ReportingService reportingService;

  ComplianceMessageBuilder complianceMessageBuilder;
  BarcodeToEPCService barcodeToEPCService;
  EPCToBarcodeService epcToBarcodeService;

  ObjectFactory factory = new ObjectFactory();

  @BeforeAll
  public void setup() throws ParserConfigurationException {
    MockitoAnnotations.openMocks(this);
    final IdUtils idUtils = new IdUtils(regexIdRepository);
    barcodeToEPCService = new BarcodeToEPCService(idUtils);
    epcToBarcodeService = new EPCToBarcodeService(idUtils);
    complianceMessageBuilder = new ComplianceMessageBuilder(
        monWrapper,
        barcodeToEPCService,
        epcToBarcodeService
    );
  }

  @Test
  public void receive_operation_on_uk()
      throws ExecutionException, InterruptedException, JAXBException, FileNotFoundException, OperationException {
    final String opID = UUID.randomUUID().toString();
    final Authority authority = Authority.UK;
    final Credentials credentials = new Credentials();
    final String deviceAddress = "validation-engine";
    final String password = "password";
    credentials.deviceAddress = deviceAddress;
    credentials.password = password;

    EPCISDocumentType documentType = fileService.loadXml("data/integration/xml/DispatchTemplate.xml", EPCISDocumentType.class);
    ObjectEventType dispatchEvent =
        ((JAXBElement<ObjectEventType>) documentType.getEPCISBody().getEventList().getObjectEventOrAggregationEventOrQuantityEvent().get(0)).getValue();

    final String fid = "300";
    EventUtils.setBizTransaction(BizTransaction.F_ID, fid, dispatchEvent);

    Operation operation = ReceiveOperation.builder()
        .complianceMessageBuilder(complianceMessageBuilder)
        .dispatchEvent(dispatchEvent)
        .reportingService(reportingService)
        .build();

    OperationContext context = OperationContext.builder()
        .opID(opID)
        .authority(authority)
        .credentials(credentials)
        .build();

    //reportingService.reportEvent(context.getAuthority(), receivingEvent, context.getOpID(), context.getCredentials());
    when(reportingService.reportEvent(eq(context.getAuthority()), any(), eq(context.getOpID()), eq(context.getCredentials())))
        .thenReturn(reportingResponsesOk(1, Authority.UK));

    OperationResult result = operation.execute(context).get();
    assertValidResult(result, context, ReceiveOperation.class);
  }

  private List<ReportingResponse> reportingResponsesOk(int quantity, Authority... authorities) {
    List<ReportingResponse> result = new ArrayList<>(quantity);
    MessageRelayEntryStatus status = new MessageRelayEntryStatus(
        RelayStatusCode.SUCCESS, "", OffsetDateTime.now(), "adapter", "000000", UUID.randomUUID().toString()
    );
    Map<String, MessageRelayEntryStatus> targetStatus = new HashMap<>();
    for (Authority authority : authorities) {
      targetStatus.put(authority.name(), status);
    }
    MessageRelayResponse messageRelayResponse = new MessageRelayResponse(status, targetStatus);

    for (int i = 0; i < quantity; i ++) {
      result.add(new ReportingResponse(messageRelayResponse));
    }

    return result;
  }

  private void assertValidResult(OperationResult result, OperationContext context, Class<? extends Operation> opClazz) {
    Assertions.assertTrue(result.getExceptions().isEmpty());
    Assertions.assertEquals(context, result.getContext());
    Assertions.assertEquals(opClazz, result.getOpClazz());
    Assertions.assertNull(result.getErrorDescription());
    Assertions.assertFalse(result.getResponses().isEmpty());
  }
}
