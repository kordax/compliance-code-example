package com.movilizer.compliance;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movilizer.compliance.internal.service.FileService;
import com.movilizer.compliance.internal.service.HierarchyGenerator;
import com.movilizer.compliance.internal.service.HierarchyGeneratorMockImpl;
import com.movilizer.compliance.internal.service.IdGenerator;
import lombok.Getter;

@Getter
public class AbstractUnitTestTest {
  protected final Gson gson = new GsonBuilder().setPrettyPrinting().create();
  protected final HierarchyGenerator generator = new HierarchyGeneratorMockImpl(gson, new IdGenerator());
  protected final FileService fileService = new FileService(new GsonBuilder().setPrettyPrinting().create());
}
