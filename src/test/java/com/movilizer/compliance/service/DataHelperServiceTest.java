package com.movilizer.compliance.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.RequestIdentifier;
import com.movilizer.masterdata.api.model.customer.Customer;
import com.movilizer.masterdata.api.model.location.Location;
import com.movilizer.masterdata.api.v1.MDCustomerService;
import com.movilizer.masterdata.api.v1.MDLocationService;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

class DataHelperServiceTest {

    static final Logger LOGGER = LoggerFactory.getLogger(DataHelperServiceTest.class);

    @Mock
    MDCustomerService mdCustomerService;

    @Mock
    MDLocationService mdLocationService;

    @Mock
    MonWrapper monWrapper;

    @Mock
    RequestIdentifier requestIdentifier;

    @InjectMocks
    DataHelperService dataHelperService;

    SampleData sampleData;

    public void prepareData() throws IOException {
        MockitoAnnotations.initMocks(this);
        sampleData = new ObjectMapper()
                .readValue(getClass().getResourceAsStream("master-data-locations-for-fid.json"), SampleData.class);
        when(mdLocationService.findByFacilityId(anyString())).thenAnswer(invocation ->
                sampleData.getLocations().get(invocation.getArgument(0)));
        when(mdCustomerService.get(anyString(), anyString()))
                .thenReturn(Optional.empty());
        when(requestIdentifier.getOperationId()).thenReturn("operation-id");
        when(requestIdentifier.getTimeStamp()).thenReturn(0L);
    }

    @Test
    void getEOIDfromFID() throws IOException {
        prepareData();
        String locId = sampleData.getLocationIdSample();
        // Mockito barely works here doe to boot
        try {
            dataHelperService.getEOIDfromFID(locId, "1212121212");
        } catch (OperationException ex) {
            LOGGER.info("exception message is: {}", ex.getMessage());
            if (ex.getMessage().contains("Unable to find locations for customer with source system")) {
                return;
            }
        }
        assert false;
    }

    public static class SampleData {
        private HashMap<String, List<Location>> locations;
        private String locationIdSample;
        private Customer customer;

        public HashMap<String, List<Location>> getLocations() {
            return locations;
        }

        public void setLocations(HashMap<String, List<Location>> locations) {
            this.locations = locations;
        }

        public String getLocationIdSample() {
            return locationIdSample;
        }

        public void setLocationIdSample(String locationIdSample) {
            this.locationIdSample = locationIdSample;
        }

        public Customer getCustomer() {
            return customer;
        }

        public void setCustomer(Customer customer) {
            this.customer = customer;
        }
    }
}