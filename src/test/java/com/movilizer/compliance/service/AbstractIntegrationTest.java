package com.movilizer.compliance.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.internal.service.FileService;
import com.movilizer.compliance.internal.service.TestCaseLoader;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInfo;
import org.junit.jupiter.api.TestInstance;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.env.Environment;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@SpringBootTest
public abstract class AbstractIntegrationTest {
  public final String MONITORING_PREFIX = "IntegrationTest";

  protected Logger logger = LoggerFactory.getLogger(AbstractIntegrationTest.class);;
  protected MonWrapper monitoring;
  protected Gson gson = new GsonBuilder().setPrettyPrinting().create();

  @Autowired
  protected FileService fileService;
  @Autowired
  protected Environment env;
  @Autowired
  protected TestCaseLoader caseLoader;

  abstract protected Object setUp(Object... args) throws Exception;

  abstract protected Object tearDown(Object... args) throws Exception;

  @BeforeAll
  public void beforeAll(TestInfo info) throws Exception {
    logger.debug("{} -> setup()", info.getDisplayName());
    setUp();
  }

  @AfterAll
  public void afterAll(TestInfo info) throws Exception {
    logger.debug("{} -> tearDown()", info.getDisplayName());
    tearDown();
  }
}
