package com.movilizer.compliance.service;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;

import static com.movilizer.compliance.util.ConversionUtils.getPackSerialAndProductCode;

public class ConversionUtilsTest {

    public List<String> requestUis = new ArrayList<>();
    public List<String> responseUisPrimaryFormat = new ArrayList<>();

    public void setup() {
        requestUis.add("5RQCLUXX:uFHY/bXGBT9%22PP2360403070013632320051808");
        requestUis.add("5RQCLUXX:uXX;-mtBBRby22PP2360403070013632320051808");
        requestUis.add("5RQCLUXX:us4BC6VVBfeY22PP2360403070013632320051808");
        requestUis.add("5RQCLUXX:uLmp7Lc-Mmfa22PP2360403070013632320051808");

        responseUisPrimaryFormat.add("QCLUXXuFHY/bXGBT9%22PP23604030700136323");
        responseUisPrimaryFormat.add("QCLUXXuXX;-mtBBRby22PP23604030700136323");
        responseUisPrimaryFormat.add("QCLUXXus4BC6VVBfeY22PP23604030700136323");
        responseUisPrimaryFormat.add("QCLUXXuLmp7Lc-Mmfa22PP23604030700136323");
    }

    @Test
    public void compareRequestPackWithPrimaryResponseFormat_test() {
        for (int i = 0; i < requestUis.size(); i++) {
            String croppedRequestUi = getPackSerialAndProductCode(requestUis.get(i));
            assert responseUisPrimaryFormat.get(i).contains(croppedRequestUi);
        }
    }

    @Test
    public void spanishPackSerialAndProductCodeExtraction_test() {
        String packSerialAndProductCode = getPackSerialAndProductCode("2351E9Ar;/>.lCpqK5]0105000433009705800820033016");
        assert packSerialAndProductCode.equals("1E9Ar;/>.lCpqK505000433009705");
    }
}
