package com.movilizer.compliance.service;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

class HierarchyItemLevelResolverTest {

    String PALLET_SAMPLE = "urn:epc:id:sscc:0464444.0000000244";
    String MC_SAMPLE____ = "urn:epc:id:sgtin:4030400.016481.PP00000244010000";
    String OUTER_SAMPLE_ = "urn:epc:id:sgtin:4030400.006481.PP099999244010100";
    String PACK_SAMPLE__ = "urn:itg:id:upuil:5RQCBDRU:PP44010101a112340000004216034220021215";

    @Test
    void resolveUIPackagingLevel() {
        HierarchyItemLevelResolver target = new HierarchyItemLevelResolver();
        assert target.resolveUIPackagingLevel(PALLET_SAMPLE) == HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.PALLET;
        assert target.resolveUIPackagingLevel(MC_SAMPLE____) == HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.MASTER_CASE;
        assert target.resolveUIPackagingLevel(OUTER_SAMPLE_) == HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.OUTER;
        assert target.resolveUIPackagingLevel(PACK_SAMPLE__) == HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.PACK;
    }
}