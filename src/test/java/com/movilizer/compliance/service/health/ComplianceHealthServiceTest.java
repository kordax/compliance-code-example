package com.movilizer.compliance.service.health;

import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.StatusResponse;
import com.movilizer.compliance.service.tdp.CodeConversionUtils;
import com.movilizer.compliance.util.AuthorityUtils;
import com.movilizer.ps.itg.compliance.restclient.api.TNTComplianceStatusService;
import com.movilizer.ps.itg.compliance.restclient.model.Authority;
import com.movilizer.ps.itg.compliance.restclient.model.AuthorityStatusCode;
import com.movilizer.ps.itg.compliance.restclient.model.LegacyComplianceStatusEntry;
import com.movilizer.ps.itg.compliance.restclient.model.TntLegacyStatusResponse;
import com.movilizer.ps.itg.compliance.restclient.model.request.TntLegacyStatusRequest;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Timer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.*;
import java.util.concurrent.TimeUnit;

import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
class ComplianceHealthServiceTest {

  @Mock
  MonWrapper monWrapper;
  @Mock
  MeterRegistry registry;
  @Mock
  TNTComplianceStatusService complianceStatusService;
  @InjectMocks
  ComplianceHealthService complianceHealthService;

  @BeforeEach
  void setUp() {
    Timer timer = Mockito.mock(Timer.class);
    doNothing().when(timer).record(anyLong(), isA(TimeUnit.class));
    doReturn(timer).when(registry).timer(anyString(), ArgumentMatchers.<String>any());
  }

  @Test
  void complianceStatus_positiveAttemptOneUI_ReturnStatusResponse() throws Exception {
    // Given
    Authority authority = Authority.PRIMARY;
    String ui = "urn:itg:id:upuil:5RQCBDRU:xxxx010101PP93ab0403040050039421032421";
    Map<String, TntLegacyStatusResponse> complianceStatusServiceResponse =
      mockComplianceStatusService(Collections.singletonList(ui), authority);
    // When
    StatusResponse statusResponse = complianceHealthService.complianceStatus(ui, authority, UUID.randomUUID().toString());
    // Then
    Assertions.assertTrue(
      checkStatusResponse(
        statusResponse,
        complianceStatusServiceResponse.get(ui).getLegacyComplianceStatusEntries().get(authority)));
  }

  @Test
  void complianceStatus_negativeAttemptOneUI_ThrowException() throws Exception {
    // Given
    Authority authority = Authority.PRIMARY;
    String ui = "urn:itg:id:upuil:5RQCBDRU:xxxx010101PP93ab0403040050039421032421";
    // When
    when(complianceStatusService.getLegacyComplianceStatus(any())).thenThrow(new RuntimeException());
    Assertions.assertThrows(OperationException.class, () -> {
      StatusResponse statusResponse = complianceHealthService.complianceStatus(ui, authority, UUID.randomUUID().toString());
    });

    // Then
    verify(complianceStatusService, atLeastOnce()).getLegacyComplianceStatus(any());
  }

  @Test
  void complianceStatus_positiveAttemptSeveralUIs_ReturnStatusResponse() throws Exception {
    // Given
    Authority authority = Authority.PRIMARY;
    List<String> uis = Arrays.asList(
      "urn:epc:id:sscc:3180000.3000000200",
      "urn:epc:id:sgtin:3258172.191119.VB30000200010000",
      "urn:epc:id:sgtin:3258170.092433.VB399999200010100",
      "urn:itg:id:upuil:5RQCBDRU:VB00010101a112340000003000300220021215",
      "urn:itg:id:upuil:5RQCBDRU:VB00010102a112340000003000300220021215"
    );
    Map<String, TntLegacyStatusResponse> complianceStatusServiceResponse =
      mockComplianceStatusService(uis, authority);
    // When
    Map<String, StatusResponse> statusResponseMap = complianceHealthService.complianceStatus(uis, authority, UUID.randomUUID().toString());
    // Then
    statusResponseMap.forEach((ui, response) -> {
      Assertions.assertTrue(
        checkStatusResponse(
          response,
          complianceStatusServiceResponse.get(ui).getLegacyComplianceStatusEntries().get(authority)));
    });
  }

  private Map<String, TntLegacyStatusResponse> mockComplianceStatusService(List<String> uis, Authority authority) {
    // prepare request:
    TntLegacyStatusRequest request = new TntLegacyStatusRequest();
    request.setIds(uis);
    request.setAuthorities(List.of(authority));
    // prepare response:
    Map<String, TntLegacyStatusResponse> complianceStatusServiceResponse = new HashMap<>();
    uis.forEach(ui -> {
      TntLegacyStatusResponse legacyStatusResponse = new TntLegacyStatusResponse(ui);
      Map<Authority, LegacyComplianceStatusEntry> legacyComplianceStatusEntries = new HashMap<>();
      LegacyComplianceStatusEntry legacyComplianceStatusEntry = new LegacyComplianceStatusEntry();
      legacyComplianceStatusEntry.setAuthority(authority);
      legacyComplianceStatusEntry.setFound(true);
      legacyComplianceStatusEntry.setIdToCheck(ui);
      legacyComplianceStatusEntry.setIdPart(CodeConversionUtils.removeUpUiPrefix(ui));
      legacyComplianceStatusEntry.setCurrentFID("CurrentFID");
      legacyComplianceStatusEntry.setState(AuthorityStatusCode.ACTIVATED);
      legacyComplianceStatusEntry.setContainsError(false);
      legacyComplianceStatusEntries.put(authority, legacyComplianceStatusEntry);
      legacyStatusResponse.setLegacyComplianceStatusEntries(legacyComplianceStatusEntries);
      complianceStatusServiceResponse.put(ui, legacyStatusResponse);
    });
    when(complianceStatusService.getLegacyComplianceStatus(eq(request))).thenReturn(complianceStatusServiceResponse);
    return complianceStatusServiceResponse;
  }

  private boolean checkStatusResponse(StatusResponse statusResponse,
                                      LegacyComplianceStatusEntry legacyComplianceStatusEntry) {
    Assertions.assertNotNull(statusResponse);
    Assertions.assertNotNull(legacyComplianceStatusEntry);
    Assertions.assertEquals(statusResponse.getFound(), legacyComplianceStatusEntry.isFound());
    Assertions.assertEquals(statusResponse.getIdPart(), legacyComplianceStatusEntry.getIdPart());
    Assertions.assertEquals(statusResponse.getIdToCheck(), legacyComplianceStatusEntry.getIdToCheck());
    Assertions.assertEquals(statusResponse.getCurrentFID(), legacyComplianceStatusEntry.getCurrentFID());
    Assertions.assertEquals(statusResponse.getState(), legacyComplianceStatusEntry.getState() != null ? AuthorityUtils.authorityStatusCodeToStatusResponceCode(legacyComplianceStatusEntry.getState()) : null);
    Assertions.assertEquals(statusResponse.isContainsError(), legacyComplianceStatusEntry.isContainsError());
    Assertions.assertEquals(statusResponse.getLegacy14(), false);
    return true;
  }
}