package com.movilizer.compliance.service;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.movilizer.commons.service.BarcodeToEPCService;
import com.movilizer.commons.service.EPCToBarcodeService;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.model.StatusResponse;
import com.movilizer.compliance.model.health.HealthResponse;
import com.movilizer.compliance.service.health.HealthService;
import com.movilizer.compliance.service.health.IHealthRequestCache;
import com.movilizer.compliance.service.health.ComplianceHealthService;
import com.movilizer.ps.itg.compliance.restclient.model.Authority;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

public class GetHealthTest {
    //TODO: rework TC

    @Mock
    public IHealthRequestCache healthRequestCache;

    @Mock
    public ComplianceHealthService complianceHealthService;

    @Mock
    public BarcodeToEPCService barcodeToEPCService;

    @Mock
    public EPCToBarcodeService epcToBarcodeService;

    @InjectMocks
    public HealthService healthService;

    @Mock
    MonWrapper monWrapper;

    SampleData sampleData;

    String uiPallet = "urn:epc:id:sscc:1102525.1000000225";


    @Before
    public void prepareHierarchyItem() throws Exception {
        MockitoAnnotations.initMocks(this);
        sampleData = new ObjectMapper().readValue(getClass().getResourceAsStream("./hItem.json"), SampleData.class);
        for (int i = 0; i < 4; i++) {
            sampleData.primaryStatus.add(new HashMap<>());
            sampleData.primaryStatus.get(i).put(sampleData.uis.get(i), sampleData.getStatusResponse().get(i));
        }
        when(this.healthRequestCache.provideHierarchyItem(anyString())).thenReturn(sampleData.getItem());
        when(this.healthRequestCache.provideEpcForUi(anyString())).thenReturn(sampleData.getEpc());
        for (int i = 0; i < 4; i++) {
            when(this.complianceHealthService.complianceStatus(sampleData.uis.get(i), Authority.PRIMARY, "1212121212")).thenReturn(sampleData.getStatusResponse().get(i));
            List<String> ui = new ArrayList<>();
            if (i != 3) {
                ui.add(sampleData.uis.get(i));
            } else {
                ui.add("5RQCBDRU:VL25010101a112340403060019986220102216");
            }
            when(this.complianceHealthService.complianceStatus(ui, Authority.PRIMARY, "1212121212")).thenReturn(sampleData.primaryStatus.get(i));
            when(this.barcodeToEPCService.mapToEPC(sampleData.uis.get(i))).thenReturn(sampleData.uis.get(i));
        }
        when(this.epcToBarcodeService.toBarcode(anyString())).thenReturn("(00)111025250000002258");
    }

    @Test
    public void itemActivatedTest() throws Exception {
        prepareHierarchyItem();
        sampleData.getStatusResponse().stream().forEach(p -> p.setState(StatusResponse.State.ACTIVATED));
        HealthResponse healthResponse = new ObjectMapper().readValue(getClass().getResourceAsStream("./response-get-health-activated.json"), HealthResponse.class);
        HealthResponse healthResponseTest = healthService.getHealth(uiPallet, Authority.PRIMARY, "1212121212", 1213131311);
        assert healthResponse.getCompliant().equals(healthResponseTest.getCompliant());
    }

//    TODO: Fix according to dev spec
//    @Test
//    @Disabled
//    public void itemDisaggregatedTest() throws Exception {
//        prepareHierarchyItem();
//        sampleData.getStatusResponse().stream().forEach(p -> p.setState(StatusResponse.State.DISAGGREGATED));
//        sampleData.getStatusResponse().stream().skip(sampleData.getStatusResponse().size() - 1).findAny().get().setState(StatusResponse.State.ACTIVATED);
//        HealthResponse healthResponse = new ObjectMapper().readValue(getClass().getResourceAsStream("./response-get-health-disaggregated.json"), HealthResponse.class);
//        HealthResponse healthResponseTest = healthService.getHealth(uiPallet, Authority.PRIMARY, "1212121212", 1213131311);
//        Assertions.assertEquals(healthResponse.getCompliant(), healthResponseTest.getCompliant());
//        Assertions.assertEquals(
//            healthResponse.getResults().get(uiPallet).getState(),
//            healthResponseTest.getResults().get(uiPallet).getState()
//        );
//    }

    public static class SampleData {
        private Item item;
        private String epc;
        private List<String> uis;
        private List<StatusResponse> statusResponse;
        private List<Map<String, StatusResponse>> primaryStatus = new ArrayList<>(4);


        public Item getItem() {
            return item;
        }

        public void setItem(Item item) {
            this.item = item;
        }

        public String getEpc() {
            return epc;
        }

        public void setEpc(String epc) {
            this.epc = epc;
        }

        public List<StatusResponse> getStatusResponse() {
            return statusResponse;
        }

        public void setStatusResponse(List<StatusResponse> statusResponse) {
            this.statusResponse = statusResponse;
        }

        public List<String> getUis() {
            return uis;
        }

        public void setUis(List<String> uis) {
            this.uis = uis;
        }
    }

    public static class Item extends com.movilizer.hierarchy.api.wrapper.v1.Item {

        @JsonCreator
        public Item(
                @JsonProperty("id")
                        String id,
                @JsonProperty("parentId")
                        String parentId,
                @JsonProperty("eventRef")
                        byte[] eventRef,
                @JsonProperty("children")
                        List<Item> children,
                @JsonProperty("timeInMillis")
                        long timeInMillis,
                @JsonProperty("statusRef")
                        byte[] statusRef) {
            super(id, parentId, eventRef, null, timeInMillis, statusRef);
            List<com.movilizer.hierarchy.api.wrapper.v1.Item> pack = new ArrayList<>(1);
            pack.add(new com.movilizer.hierarchy.api.wrapper.v1.Item("urn:itg:id:upuil:5RQCBDRU:VL25010101a112340403060019986220102216",
                    "urn:epc:id:sgtin:4030600.023139.VL199999225010100",
                    null,
                    null,
                    194783,
                    statusRef));
            List<com.movilizer.hierarchy.api.wrapper.v1.Item> outer = new ArrayList<>(1);
            outer.add(new com.movilizer.hierarchy.api.wrapper.v1.Item("urn:epc:id:sgtin:4030600.023139.VL199999225010100",
                    "urn:epc:id:sgtin:4030600.023140.VL10000225010000",
                    null,
                    new ArrayList<>(pack),
                    194783,
                    statusRef));
            List<com.movilizer.hierarchy.api.wrapper.v1.Item> mc = new ArrayList<>(1);
            mc.add(new com.movilizer.hierarchy.api.wrapper.v1.Item("urn:epc:id:sgtin:4030600.023140.VL10000225010000",
                    "urn:epc:id:sscc:1102525.1000000225",
                    null,
                    new ArrayList<>(outer),
                    194783,
                    statusRef));
            setChildren(mc);
        }
    }
}

