package com.movilizer.compliance.service;


import com.movilizer.compliance.exception.OperationException;

import com.movilizer.compliance.model.health.HealthResponse;
import com.movilizer.compliance.service.health.HealthService;

import com.movilizer.ps.itg.compliance.restclient.model.Authority;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import java.util.*;


@ActiveProfiles("local")
public class HealthServiceIntegrationTest extends AbstractIntegrationTest {

  @Autowired
  HealthService healthService;



  @Override
  protected Object setUp(Object... args) {
    return null;
  }

  @Override
  protected Object tearDown(Object... args) {
    return null;
  }

  @Test
  public void checkForCompliantPallet() throws OperationException {

    HealthResponse health = healthService.getHealth("urn:itg:id:upuil:5RQCBDRU:xxxx010101PP93ab0403040050039421032421", Authority.PRIMARY, UUID.randomUUID().toString(), 0);
    System.out.print(health.toString() + "\n");
    Assertions.assertEquals(health.getCompliant(), true);

  }

  @Test
  public void checkForNonCompliantPallet() throws OperationException {

    HealthResponse health = healthService.getHealth("urn:epc:id:sscc:6469595.6000000295", Authority.PRIMARY, UUID.randomUUID().toString(), 0);
    System.out.print(health.toString() + "\n");
    Assertions.assertEquals(health.getCompliant(), false);

  }

}