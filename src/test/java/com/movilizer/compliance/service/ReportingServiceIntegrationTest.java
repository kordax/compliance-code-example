package com.movilizer.compliance.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doReturn;

import com.google.gson.JsonObject;
import com.movilizer.commons.model.MessageType;
import com.movilizer.commons.util.Credentials;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.internal.dto.HierarchyGenerationEntry;
import com.movilizer.compliance.internal.service.HierarchyHelperService;
import com.movilizer.compliance.model.ReportingResponse;
import com.movilizer.compliance.service.tdp.TdpMessageFLService;
import com.movilizer.compliance.util.EPCISEventUtils;
import com.movilizer.masterdata.api.model.location.Location;
import com.movilizer.mwss.model.api.Epcis.BizTransaction;
import com.movilizer.ps.itg.compliance.restclient.model.Authority;
import com.movilizer.ps.itg.epcis.ActionType;
import com.movilizer.ps.itg.epcis.AggregationEventType;
import com.movilizer.ps.itg.epcis.BusinessLocationType;
import com.movilizer.ps.itg.epcis.EPC;
import com.movilizer.ps.itg.epcis.EPCISEventType;
import com.movilizer.ps.itg.epcis.EPCISEventType.AUIs;
import com.movilizer.ps.itg.epcis.EPCISEventType.UpUIs;
import com.movilizer.ps.itg.epcis.EPCListType;
import com.movilizer.ps.itg.epcis.ObjectFactory;
import com.movilizer.uaecommons.destination.Destination;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.SpyBean;

public class ReportingServiceIntegrationTest extends AbstractIntegrationTest {
  @SpyBean
  private ReportingService service;
  @SpyBean
  private TdpMessageFLService messageFLService;

  @Autowired
  HierarchyHelperService hierarchy;

  private final ObjectFactory factory = new ObjectFactory();

  @Override
  protected Object setUp(Object... args) {
    return null;
  }

  @Override
  protected Object tearDown(Object... args) {
    return null;
  }

  @Test
  public void ReportUKEventToTheEPCISCapture_Get200_WhenUK(TestInfo testInfo)
          throws IOException, DatatypeConfigurationException, OperationException {
    AggregationEventType event = factory.createAggregationEventType();

    setMissingFields(event, testInfo);

    String opId = UUID.randomUUID().toString();
    Credentials credentials = caseLoader.loadTestCredentials();
    Location location = new Location();
    location.setCountry("UK");

    String rsku = "12346789";
    List<String> childEpcs = event.getChildEPCs().getEpc().stream().map(EPC::getValue).collect(Collectors.toList());
    Set<String> targetMarkets = Set.of("UK");

    mockSpiedBeans(location, childEpcs, Set.of(rsku), targetMarkets);
    service.processEvent(Authority.UK, event, opId, credentials);
  }

  private void setMissingFields(AggregationEventType event, TestInfo testInfo)
      throws DatatypeConfigurationException, IOException {
    GregorianCalendar calendar = new GregorianCalendar();
    calendar.setTime(new Date());
    XMLGregorianCalendar xmlGregorianCalendar =
        DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
    event.setEventTime(xmlGregorianCalendar);
    event.setEventTimeZoneOffset("Z");

    setEpcs(testInfo.getDisplayName(), event);

    event.setMessageType(MessageType.AGGREGATION.getValue());
    event.setAction(ActionType.ADD);

    JsonObject locations = caseLoader.retrieveLocations();
    BusinessLocationType businessLocationType = new BusinessLocationType();
    businessLocationType.setId(locations.get("bizLocation").getAsString());

    event.setBizLocation(businessLocationType);

    EPCISEventUtils.setBizTransaction(BizTransaction.MESSAGE_ID, UUID.randomUUID().toString(), event);
    EPCISEventUtils.setBizTransaction(BizTransaction.TRANSACTION_ID, UUID.randomUUID().toString(), event);
    EPCISEventUtils.setBizTransaction(BizTransaction.SYSTEM, UUID.randomUUID().toString(), event);
    event.setAggregationType(EPCISEventUtils.getAggregationType(
        event.getChildEPCs().getEpc().stream().map(EPC::getValue).collect(Collectors.toList()))
    );
  }

  private void mockSpiedBeans(
      Location location,
      List<String> childsEpcs,
      Set<String> rskus,
      Set<String> targetMarkets) {
    doReturn(Optional.ofNullable(location)).when(messageFLService).getLocationSenderByBizLocation(any());
    doReturn(rskus).when(messageFLService).getRskus(childsEpcs);
    doReturn(rskus).when(messageFLService).getRskus(anyString());
    doReturn(targetMarkets).when(messageFLService).getTargetMarkets(anyString());
    doReturn(
        targetMarkets.stream().map(this::marketToDest).collect(Collectors.toList())
    ).when(messageFLService).getDestination(any(EPCISEventType.class));
  }

  private Destination marketToDest(String targetMarket) {
    if ("UK".equals(targetMarket)) {
      return Destination.UK;
    }

    return Destination.PRIMARY;
  }

  private void setEpcs(String caseName, AggregationEventType epcisEventType) throws IOException {
    List<HierarchyGenerationEntry> entries = hierarchy.generateHierarchies(caseName);
    HierarchyGenerationEntry any = entries.stream().findAny().orElseThrow();

    epcisEventType.setParentID(any.getId());

    if (epcisEventType.getChildEPCs() == null) {
      epcisEventType.setChildEPCs(new EPCListType());
    }

    epcisEventType.getChildEPCs().getEpc().addAll(
        any.getChildren().stream().map(e -> {
          EPC epc = new EPC();
          epc.setValue(e.getId());
          epc.setAttr6(String.valueOf(e.getLevel()));

          return epc;
      }).collect(Collectors.toList())
    );

    List<HierarchyGenerationEntry> upUiEntries = entries.stream().filter(e -> e.getLevel().equals(10)).collect(Collectors.toList());
    List<HierarchyGenerationEntry> aUiEntries = entries.stream().filter(e -> !e.getLevel().equals(10)).collect(Collectors.toList());

    UpUIs upUIs = new UpUIs();
    AUIs auIs = new AUIs();
    upUIs.getEpc().addAll(
        upUiEntries
            .stream()
            .map(e -> { EPC epc = new EPC(); epc.setValue(e.getId()); return epc;})
            .collect(Collectors.toList())
    );
    auIs.getEpc().addAll(
        aUiEntries
            .stream()
            .map(e -> { EPC epc = new EPC(); epc.setValue(e.getId()); return epc;})
            .collect(Collectors.toList())
    );

    epcisEventType.setUpUIs(upUIs);
    epcisEventType.setAUIs(auIs);
  }
}