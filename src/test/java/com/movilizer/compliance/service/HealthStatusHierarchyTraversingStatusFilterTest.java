package com.movilizer.compliance.service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.movilizer.compliance.model.StatusResponse;
import com.movilizer.compliance.model.health.ChildUIHealthResult;
import org.junit.jupiter.api.Test;
import static org.mockito.ArgumentMatchers.anyString;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import static org.mockito.Mockito.doCallRealMethod;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

class HealthStatusHierarchyTraversingStatusFilterTest {

    @InjectMocks
    public HealthStatusHierarchyTraversingStatusFilter target = new HealthStatusHierarchyTraversingStatusFilter();

    @Mock
    public HierarchyItemLevelResolver hierarchyItemLevelResolver;

    public static void traverseHealthResults(ChildUIHealthResult node) {
        if (node.getChildren() != null) {
            for (Map.Entry<String, ChildUIHealthResult> entry : node.getChildren().entrySet()) {
                entry.getValue().setId(entry.getKey());
                entry.getValue().setParentId(node.getId());
                traverseHealthResults(entry.getValue());
            }
        }
    }

    public ChildUIHealthResult prepareData() throws IOException {
        MockitoAnnotations.initMocks(this);
        doCallRealMethod().when(hierarchyItemLevelResolver).resolveUIPackagingLevel(anyString());
        ObjectMapper objectMapper = new ObjectMapper();
        InputStream responseStream = getClass().getResourceAsStream("./health-response.json");
        ChildUIHealthResult stubData = objectMapper.readValue(responseStream, ChildUIHealthResult.class);
        traverseHealthResults(stubData);
        stubData.setId("urn:epc:id:sscc:0464444.0000000244");
        return stubData;
    }

    @Test
    void itemMeetsState() throws IOException {
        ChildUIHealthResult stubData = prepareData();
        assert target.itemMeetsState(stubData, StatusResponse.State.ACTIVATED);
        ChildUIHealthResult someItemNode = stubData.getChildren().get("urn:epc:id:sgtin:4030400.016481.PP00000244010000");
        someItemNode.setState(StatusResponse.State.REGISTERED);
        assert !target.itemMeetsState(someItemNode, StatusResponse.State.ACTIVATED);
    }

    @Test
    void itemMeetsLevel() throws IOException {
        ChildUIHealthResult stubData = prepareData();

        assert target.itemMeetsLevel(stubData,
                HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.PALLET,
                HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.PACK);
        ChildUIHealthResult leafItemNode = stubData
                .getChildren().get("urn:epc:id:sgtin:4030400.016481.PP00000244010000")
                .getChildren().get("urn:epc:id:sgtin:4030400.006481.PP099999244010200")
                .getChildren().get("urn:itg:id:upuil:5RQCBDRU:PP44010101a112340000004216034220-STUB1");
        assert target.itemMeetsLevel(leafItemNode,
                HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.PALLET,
                HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.PACK);
        ChildUIHealthResult outerItemNode = stubData
                .getChildren().get("urn:epc:id:sgtin:4030400.016481.PP00000244010000")
                .getChildren().get("urn:epc:id:sgtin:4030400.006481.PP099999244010200");
        assert !target.itemMeetsLevel(outerItemNode,
                HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.PALLET,
                HierarchyItemLevelResolver.IDPrefixBasedPackagingLevel.PACK);
    }

    @Test
    void itemMeetsFound() throws IOException {
        ChildUIHealthResult stubData = prepareData();
        assert target.itemMeetsFound(stubData, "true");
        ChildUIHealthResult someItemNode = stubData.getChildren().get("urn:epc:id:sgtin:4030400.016481.PP00000244010000");
        someItemNode.setFound(Boolean.FALSE);
        assert !target.itemMeetsFound(someItemNode, "true");
    }

    @Test
    void itemMeetsCurrentFid() throws IOException {
        ChildUIHealthResult stubData = prepareData();
        assert target.itemMeetsCurrentFid(stubData, "QCBDR<1DE171841056917");
        ChildUIHealthResult someItemNode = stubData.getChildren().get("urn:epc:id:sgtin:4030400.016481.PP00000244010000");
        someItemNode.setCurrentFID("KPL100000CU");
        assert !target.itemMeetsCurrentFid(someItemNode, "QCBDR<1DE171841056917");
    }

    @Test
    void flattenHealthResults() throws IOException {
        ChildUIHealthResult stubData = prepareData();

        assert target.flattenHealthResults(stubData).size() == 8;
    }
}