package com.movilizer.compliance.service;

import static com.movilizer.compliance.service.HierarchyItemLevelResolver.BOTTOM_LEVEL_ITEM_PREFIX;
import static com.movilizer.compliance.service.HierarchyItemLevelResolver.BOTTOM_LEVEL_SHORT_ITEM_PREFIX;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;

import com.movilizer.commons.model.RegexId;
import com.movilizer.commons.repository.RegexIdRepository;
import com.movilizer.commons.service.BarcodeToEPCService;
import com.movilizer.commons.service.EPCToBarcodeService;
import com.movilizer.commons.service.IdUtils;
import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.AbstractUnitTestTest;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.internal.dto.HierarchyGenerationEntry;
import com.movilizer.compliance.model.StatusResponse;
import com.movilizer.compliance.model.StatusResponse.State;
import com.movilizer.compliance.model.health.ChildUIHealthResult;
import com.movilizer.compliance.model.health.HealthResponse;
import com.movilizer.compliance.service.health.ComplianceHealthService;
import com.movilizer.compliance.service.health.HealthService;
import com.movilizer.compliance.service.health.IHealthRequestCache;
import com.movilizer.compliance.service.health.IHealthRequestCache.UnableToGetHierarchyForUIException;
import com.movilizer.compliance.service.tdp.CodeConversionUtils;
import com.movilizer.compliance.service.tdp.TdpMessageFLService;
import com.movilizer.hierarchy.api.wrapper.v1.HierarchyService;
import com.movilizer.ps.itg.compliance.restclient.model.Authority;
import java.io.FileNotFoundException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.TestInstance.Lifecycle;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.mock.mockito.SpyBean;

@TestInstance(Lifecycle.PER_CLASS)
public class HealthServiceTest extends AbstractUnitTestTest {
  private HealthService healthService;

  @Mock
  HierarchyService hierarchyService;
  @Mock
  RegexIdRepository regexIdRepository;
  @Mock
  MonWrapper monWrapper;
  @Mock
  IHealthRequestCache healthRequestCache;
  @Mock
  TdpMessageFLService flService;
  @Mock
  ComplianceHealthService complianceHealthService;
  @Mock
  DataHelperService dataHelperService;

  BarcodeToEPCService barcodeToEPCService;
  EPCToBarcodeService epcToBarcodeService;

  @BeforeAll
  public void setup() {
    MockitoAnnotations.openMocks(this);
    final IdUtils idUtils = new IdUtils(regexIdRepository);
    barcodeToEPCService = new BarcodeToEPCService(idUtils);
    epcToBarcodeService = new EPCToBarcodeService(idUtils);
    healthService = new HealthService(
        hierarchyService,
        barcodeToEPCService,
        monWrapper,
        epcToBarcodeService,
        healthRequestCache,
        flService,
        complianceHealthService,
        dataHelperService
    );
  }

  @Test
  public void get_packs_as_registered_when_packs_not_found()
      throws FileNotFoundException, OperationException, UnableToGetHierarchyForUIException {
    final HierarchyGenerationEntry entry = gson.fromJson(
        fileService.loadJson("data/integration/json/hierarchies.json")
            .get("get_packs_as_registered_when_packs_not_found")
            .getAsJsonArray().get(0),
        HierarchyGenerationEntry.class
    );
    List<HierarchyGenerationEntry> hierarchy = generator.processEntry(entry);
    recursivelyMock(hierarchy);

    final HierarchyGenerationEntry h = hierarchy.get(0);
    final String opId = UUID.randomUUID().toString();
    final long t0 = System.currentTimeMillis();

    HealthResponse response = healthService.getHealth(h.getId(), Authority.UK, opId, t0);
    Assertions.assertTrue(true);
    Assertions.assertNotNull(response.getCompliant());
    Assertions.assertNotNull(response.getOperationId());
    Assertions.assertNotNull(response.getTimestamp());
    Assertions.assertNotNull(response.getResults());
    Assertions.assertEquals(false, response.getCompliant());
    Assertions.assertEquals(opId, response.getOperationId());
    Assertions.assertEquals(t0, Long.parseLong(response.getTimestamp()));
    Assertions.assertTrue(response.getResults().entrySet().stream().anyMatch(e -> e.getKey().equals(CodeConversionUtils.removeUpUiPrefix(h.getId()))));
    List<ChildUIHealthResult> upUiResults = response.getResults().entrySet()
        .stream()
        .filter(e -> e.getKey().contains(BOTTOM_LEVEL_SHORT_ITEM_PREFIX) || e.getKey().contains(BOTTOM_LEVEL_ITEM_PREFIX))
        .map(Entry::getValue)
        .collect(Collectors.toList());

    for (ChildUIHealthResult result : upUiResults) {
      Assertions.assertEquals(State.REGISTERED , result.getState());
      Assertions.assertTrue(result.isFound());
    }
  }

  private void recursivelyMock(Collection<HierarchyGenerationEntry> entries)
      throws UnableToGetHierarchyForUIException, OperationException {
    for (HierarchyGenerationEntry entry : entries) {
      when(
          hierarchyService.getItem(anyLong(), eq(entry.getId()))
      ).thenReturn(Optional.of(entry.toHItem()));
      when(
          hierarchyService.getItem(anyLong(), eq(entry.getId()))
      ).thenReturn(Optional.of(entry.toHItem()));
      doReturn(
          entry.toHItem()
      ).when(healthRequestCache).provideHierarchyItem(entry.getId());
      doReturn(
          entry.getId()
      ).when(healthRequestCache).provideEpcForUi(entry.getId());

      final StatusResponse response = new StatusResponse();
      response.setFound(false);
      response.setIdPart(entry.getId());
      response.setContainsError(true);
      response.setState(State.NOT_FOUND);
      response.setIdToCheck(entry.getId());
      response.setLegacy14(false);
      response.setContainsError(true);
      doReturn(
          response
      ).when(complianceHealthService).complianceStatus(eq(entry.getId()), eq(Authority.UK), anyString());

      Map<String, StatusResponse> map = new HashMap<>();
      map.put(entry.getId(), response);
      doReturn(
          map
      ).when(complianceHealthService).complianceStatus(eq(Collections.singletonList(
          CodeConversionUtils.removeUpUiPrefix((entry.getId()))
      )), eq(Authority.UK), anyString());
      if (entry.getChildren() != null) {
        recursivelyMock(entry.getChildren());
      }
    }
  }
}
