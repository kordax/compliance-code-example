package com.movilizer.compliance.service.aggreg;

import com.movilizer.commons.util.MonWrapper;
import com.movilizer.compliance.exception.OperationException;
import com.movilizer.compliance.model.StatusResponse;
import com.movilizer.compliance.model.aggreg.ReconstructUiResponse;
import com.movilizer.compliance.service.ReconstructionMessageBuilder;
import com.movilizer.compliance.service.health.ComplianceHealthService;
import com.movilizer.compliance.service.recovery.RecoveryService;
import com.movilizer.hierarchy.api.wrapper.v1.Item;
import com.movilizer.ps.itg.compliance.restclient.model.Authority;
import groovy.lang.Tuple2;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

public class ReconstructionServiceTest {

  private static final String OP_ID = "f63ffcfc-982f-4998-9454-2debf15df69b";

  @Mock
  private MonWrapper monWrapper;

  @Mock
  private ComplianceHealthService complianceHealthService;

  @Mock
  private ReconstructionMessageBuilder reconstructionMessageBuilder;

  @Mock
  private RecoveryService recoveryService;

  @InjectMocks
  private ReconstructionService reconstructionService;

  @Before
  public void init() throws Exception {
    MockitoAnnotations.initMocks(this);

    Mockito.when(complianceHealthService.complianceStatus(anyString(), eq(Authority.PRIMARY), eq(OP_ID))).thenReturn(new StatusResponse());
  }

  @Test
  public void prepareReconstructUiResponseNoLegacyNoMixedStatesTest() {
    // Arrange
    Map<Integer, List<Tuple2<Item, StatusResponse>>> levelTupleMap = new HashMap<>();
    String parentUi = "urn:epc:id:sscc:8598989.8000000289";
    levelTupleMap.put(
        1,
        generateItemStatusTupleList(
            generateStatusResponseList(parentUi, 1, false, StatusResponse.State.ACTIVATED)));
    levelTupleMap.put(
        2,
        generateItemStatusTupleList(
            generateStatusResponseList("", 5, false, StatusResponse.State.ACTIVATED)));
    levelTupleMap.put(
        3,
        generateItemStatusTupleList(
            generateStatusResponseList("", 25, false, StatusResponse.State.ACTIVATED)));
    Integer maxDepth = 4;

    // Act
    ReconstructUiResponse reconstructUiResponse =
        ReconstructionService.prepareReconstructUiResponse(
            parentUi, maxDepth, levelTupleMap, OP_ID);

    // Assert
    Assert.assertEquals(reconstructUiResponse.getUi(), parentUi);
    Assert.assertEquals(reconstructUiResponse.getMaxHierarchyDepth(), maxDepth);
    Assert.assertEquals(reconstructUiResponse.getOperationId(), OP_ID);
    Assert.assertFalse(reconstructUiResponse.getHasLegacyItems());
    Assert.assertFalse(reconstructUiResponse.getHasMixedStates());
  }

  @Test
  public void prepareReconstructUiResponseNoLegacyWithMixedStatesTest() {
    // Arrange
    Map<Integer, List<Tuple2<Item, StatusResponse>>> levelTupleMap = new HashMap<>();
    String parentUi = "urn:epc:id:sscc:8598989.8000000289";
    levelTupleMap.put(
        1,
        generateItemStatusTupleList(
            generateStatusResponseList(parentUi, 1, false, StatusResponse.State.ACTIVATED)));
    levelTupleMap.put(
        2,
        generateItemStatusTupleList(
            generateStatusResponseList("", 5, false, StatusResponse.State.DISAGGREGATED)));
    levelTupleMap.put(
        3,
        generateItemStatusTupleList(
            generateStatusResponseList("", 25, false, StatusResponse.State.ACTIVATED)));
    Integer maxDepth = 4;

    // Act
    ReconstructUiResponse reconstructUiResponse =
        ReconstructionService.prepareReconstructUiResponse(
            parentUi, maxDepth, levelTupleMap, OP_ID);

    // Assert
    Assert.assertEquals(reconstructUiResponse.getUi(), parentUi);
    Assert.assertEquals(reconstructUiResponse.getMaxHierarchyDepth(), maxDepth);
    Assert.assertEquals(reconstructUiResponse.getOperationId(), OP_ID);
    Assert.assertFalse(reconstructUiResponse.getHasLegacyItems());
    Assert.assertTrue(reconstructUiResponse.getHasMixedStates());
  }

  @Test
  public void prepareReconstructUiResponseWithLegacyWithMixedStatesTest() {
    // Arrange
    Map<Integer, List<Tuple2<Item, StatusResponse>>> levelTupleMap = new HashMap<>();
    String parentUi = "urn:epc:id:sscc:8598989.8000000289";
    levelTupleMap.put(
        1,
        generateItemStatusTupleList(
            generateStatusResponseList(parentUi, 1, false, StatusResponse.State.ACTIVATED)));
    levelTupleMap.put(
        2,
        generateItemStatusTupleList(
            generateStatusResponseList("", 5, true, StatusResponse.State.DISAGGREGATED)));
    levelTupleMap.put(
        3,
        generateItemStatusTupleList(
            generateStatusResponseList("", 25, false, StatusResponse.State.ACTIVATED)));
    Integer maxDepth = 4;

    // Act
    ReconstructUiResponse reconstructUiResponse =
        ReconstructionService.prepareReconstructUiResponse(
            parentUi, maxDepth, levelTupleMap, OP_ID);

    // Assert
    Assert.assertEquals(reconstructUiResponse.getUi(), parentUi);
    Assert.assertEquals(reconstructUiResponse.getMaxHierarchyDepth(), maxDepth);
    Assert.assertEquals(reconstructUiResponse.getOperationId(), OP_ID);
    Assert.assertTrue(reconstructUiResponse.getHasLegacyItems());
    Assert.assertTrue(reconstructUiResponse.getHasMixedStates());
  }

  @Test
  public void prepareReconstructUiResponseWithLegacyNoMixedStatesTest() {
    // Arrange
    Map<Integer, List<Tuple2<Item, StatusResponse>>> levelTupleMap = new HashMap<>();
    String parentUi = "urn:epc:id:sscc:8598989.8000000289";
    levelTupleMap.put(
        1,
        generateItemStatusTupleList(
            generateStatusResponseList(parentUi, 1, false, StatusResponse.State.ACTIVATED)));
    levelTupleMap.put(
        2,
        generateItemStatusTupleList(
            generateStatusResponseList("", 5, true, StatusResponse.State.ACTIVATED)));
    levelTupleMap.put(
        3,
        generateItemStatusTupleList(
            generateStatusResponseList("", 25, false, StatusResponse.State.ACTIVATED)));
    Integer maxDepth = 4;

    // Act
    ReconstructUiResponse reconstructUiResponse =
        ReconstructionService.prepareReconstructUiResponse(
            parentUi, maxDepth, levelTupleMap, OP_ID);

    // Assert
    Assert.assertEquals(reconstructUiResponse.getUi(), parentUi);
    Assert.assertEquals(reconstructUiResponse.getMaxHierarchyDepth(), maxDepth);
    Assert.assertEquals(reconstructUiResponse.getOperationId(), OP_ID);
    Assert.assertTrue(reconstructUiResponse.getHasLegacyItems());
    Assert.assertFalse(reconstructUiResponse.getHasMixedStates());
  }

  @Test
  public void prepareHierarchyLevelsMapTest() throws OperationException {
    // Arrange
    String parentUi = "urn:epc:id:sscc:8598989.8000000289";
    int hierarchyDepth = 4;
    Item parentItem = generateItemHierarchy(parentUi, "", hierarchyDepth, 5);

    // Act
    Map<Integer, List<Tuple2<Item, StatusResponse>>> resultMap = reconstructionService.prepareHierarchyLevelsMap(Authority.PRIMARY, parentItem, hierarchyDepth, OP_ID);

    // Assert
    Assert.assertEquals(resultMap.keySet().size(), hierarchyDepth);
    Assert.assertTrue(resultMap.keySet().contains(1));
    Assert.assertTrue(resultMap.keySet().contains(2));
    Assert.assertTrue(resultMap.keySet().contains(3));
    Assert.assertTrue(resultMap.keySet().contains(4));
  }

  private List<StatusResponse> generateStatusResponseList(
      String ui, Integer count, Boolean legacy, StatusResponse.State state) {
    List<StatusResponse> statusResponseList = new ArrayList<>();
    for (int i = 0; i < count; i++) {
      StatusResponse response = new StatusResponse();
      response.setContainsError(false);
      response.setState(state);
      response.setFound(true);
      response.setCurrentFID("FID");
      if (ui.isEmpty()) {
        response.setIdPart(String.valueOf(i));
        response.setIdToCheck(String.valueOf(i));
      } else {
        response.setIdPart(ui);
        response.setIdToCheck(ui);
      }
      if (legacy) {
        response.setLegacy14(true);
      } else {
        response.setLegacy14(false);
      }
      statusResponseList.add(response);
    }
    return statusResponseList;
  }

  private List<Tuple2<Item, StatusResponse>> generateItemStatusTupleList(
      List<StatusResponse> statusResponses) {
    List<Tuple2<Item, StatusResponse>> resultList = new ArrayList<>();

    for (StatusResponse statusResponse : statusResponses) {
      Item item =
          new Item(statusResponse.getIdToCheck(), "parent", null, new ArrayList<>(), 1l, null);
      Tuple2 tuple2 = new Tuple2(item, statusResponse);
      resultList.add(tuple2);
    }

    return resultList;
  }

  //For future use
  private Item generateItemHierarchy(
      String ui, String parentId, Integer levels, Integer childrenPerLevel) {
    if (levels == 0) {
      return null;
    }
    levels--;
    Item result = new Item(ui, parentId, null, null, System.currentTimeMillis(), null);
    List<Item> children = new ArrayList<>();
    for (int i = 0; i < childrenPerLevel; i++) {
      Item child =
          generateItemHierarchy(
              String.valueOf(i) + String.valueOf(levels), ui, levels, childrenPerLevel);
      if (child != null) {
        children.add(child);
      }
    }
    result.setChildren(children);
    return result;
  }

}
