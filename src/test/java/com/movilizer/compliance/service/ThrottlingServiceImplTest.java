package com.movilizer.compliance.service;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import com.movilizer.commons.service.BarcodeToEPCService;
import com.movilizer.commons.util.MonWrapper;
import java.util.List;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
class ThrottlingServiceImplTest {

    public static final long SOME_NUMBER = 42L;
    public static final long LOWER_NUMBER = 41L;
    public static final long HIGHER_NUMBER = 43L;
    @Mock public BarcodeToEPCService barcodeToEPCService;
    @Mock public MonWrapper monWrapper;
    @Mock public CurrentTimeProvider currentTimeProvider;
    @InjectMocks public ThrottlingServiceImpl service;
    public String[] ids = new String[] {"uid1", "uid2", "uid3", "uid4", "uid5"};

    public void setup() {
        MockitoAnnotations.initMocks(this);

        when(barcodeToEPCService.mapToEPC(anyString()))
                .thenAnswer(invocation -> invocation.getArgument(0) + "_resolved");

        doNothing()
                .when(monWrapper)
                .errorJson(any(Object.class), anyString(), any(Throwable.class));

        when(currentTimeProvider.getCurrentTime()).thenReturn(LOWER_NUMBER);
    }

    @Test
    public void processLockTTL_EmptyTest() {
        setup();
        service.handleRequest(List.of("uid1", "uid2", "uid3", "uid4"), "opID", 4242);
        service.processLockTTL(SOME_NUMBER);
        assert service.ttlMap.isEmpty();
    }

    @Test
    public void processLockTTL_PartialTest() {
        setup();
        service.handleRequest(List.of("uid1", "uid2", "uid3", "uid4"), "opID", 4242);
        when(currentTimeProvider.getCurrentTime()).thenReturn(HIGHER_NUMBER);
        service.handleRequest(List.of("uid5"), "opID", 4242);
        service.processLockTTL(SOME_NUMBER);
        assert service.ttlMap.size() == 1;
    }

    @Test
    public void uisLockedTest() {
        setup();
        service.handleRequest(List.of("uid1", "uid2", "uid3", "uid4"), "opID", 4242);
        assert service.uisLocked(List.of("uid10")).isEmpty();
        assert !service.uisLocked(List.of("uid3")).isEmpty();
    }
}
