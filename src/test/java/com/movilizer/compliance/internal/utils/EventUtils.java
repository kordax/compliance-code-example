package com.movilizer.compliance.internal.utils;

import com.movilizer.mwss.model.api.Epcis;
import com.movilizer.ps.itg.epcis.*;
import javax.xml.datatype.XMLGregorianCalendar;
import org.apache.logging.log4j.util.Strings;

import javax.xml.bind.JAXBElement;
import java.util.List;
import java.util.Optional;

import static com.movilizer.mwss.model.api.Epcis.DestinationTypes.SHIP_TO_DESTINATION;


public class EventUtils {
  public static void setEventTime(XMLGregorianCalendar calendar, JAXBElement<EPCISEventType> jaxbElement) {
    setEventTime(calendar, jaxbElement.getValue());
  }

  public static void setEventTime(XMLGregorianCalendar calendar, EPCISEventType epcisEventType) {
    if (epcisEventType instanceof AggregationEventType) {
      epcisEventType.setEventTime(calendar);
    }
    if (epcisEventType instanceof ObjectEventType) {
      epcisEventType.setEventTime(calendar);
    }
  }

  public static void setBizTransaction(Epcis.BizTransaction type, String value, JAXBElement<EPCISEventType> jaxbElement) {
    EPCISEventType epcisEventType = jaxbElement.getValue();

    setBizTransaction(type, value, epcisEventType);
  }

  public static void setBizTransaction(Epcis.BizTransaction type, String value, EPCISEventType epcisEventType) {
    if (epcisEventType instanceof AggregationEventType) {
      AggregationEventType event = (AggregationEventType) epcisEventType;
      if (event.getBizTransactionList() == null) {
        event.setBizTransactionList(new BusinessTransactionListType());
      }

      Optional<BusinessTransactionType> transactionOpt = event.getBizTransactionList().getBizTransaction()
          .stream()
          .filter(t -> t.getType().equals(type.getXmlValue()))
          .findAny();
      if (transactionOpt.isPresent()) {
        transactionOpt.get().setValue(value);
      } else {
        BusinessTransactionType transaction = new BusinessTransactionType();
        transaction.setType(type.getXmlValue());
        transaction.setValue(value);
        event.getBizTransactionList().getBizTransaction().add(transaction);
      }
    }
    if (epcisEventType instanceof ObjectEventType) {
      ObjectEventType event = (ObjectEventType) epcisEventType;
      if (event.getBizTransactionList() == null) {
        event.setBizTransactionList(new BusinessTransactionListType());
      }

      Optional<BusinessTransactionType> transactionOpt = event.getBizTransactionList().getBizTransaction()
          .stream()
          .filter(t -> t.getType().equals(type.getXmlValue()))
          .findAny();
      if (transactionOpt.isPresent()) {
        transactionOpt.get().setValue(value);
      } else {
        BusinessTransactionType transaction = new BusinessTransactionType();
        transaction.setType(type.getXmlValue());
        transaction.setValue(value);
        event.getBizTransactionList().getBizTransaction().add(transaction);
      }
    }
  }

  public static void setDestinationLocation(AggregationEventType event, String location) {
    BusinessTransactionListType bizTransactionList = event.getBizTransactionList();
    if (bizTransactionList == null) {
      bizTransactionList = new BusinessTransactionListType();
      event.setBizTransactionList(bizTransactionList);
    }

    List<BusinessTransactionType> bizTransaction = bizTransactionList.getBizTransaction();
    BusinessTransactionType transaction =
        bizTransaction
            .stream()
            .filter(t -> t.getType().equals(SHIP_TO_DESTINATION.getXmlValue()))
            .findFirst()
            .orElse(null);

    if (transaction == null) {
      transaction = new BusinessTransactionType();
      transaction.setType(SHIP_TO_DESTINATION.getXmlValue());
    }

    transaction.setValue(location);
  }

  public static BusinessTransactionListType getBizTransactions(Object eventJaxb) {
    Object event = ((JAXBElement<?>) eventJaxb).getValue();

    if (event instanceof ObjectEventType) {
      return ((ObjectEventType) event).getBizTransactionList();
    } else if (event instanceof AggregationEventType) {
      return ((AggregationEventType) event).getBizTransactionList();
    } else if (event instanceof TransformationEventType) {
      return ((TransformationEventType) event).getBizTransactionList();
    } else if (event instanceof TransactionEventType) {
      return ((TransactionEventType) event).getBizTransactionList();
    } else if (event instanceof QuantityEventType) {
      return ((QuantityEventType) event).getBizTransactionList();
    }
    throw new IllegalArgumentException(
        "Type " + eventJaxb.getClass().getSimpleName() + " is not supported");
  }

  public String getBizStep(Object eventJaxb) {
    Object event = ((JAXBElement<?>) eventJaxb).getValue();

    if (event instanceof ObjectEventType) {
      return ((ObjectEventType) event).getBizStep();
    } else if (event instanceof com.movilizer.ps.itg.epcis.AggregationEventType) {
      return ((com.movilizer.ps.itg.epcis.AggregationEventType) event).getBizStep();
    } else if (event instanceof TransformationEventType) {
      return ((TransformationEventType) event).getBizStep();
    } else if (event instanceof TransactionEventType) {
      return ((TransactionEventType) event).getBizStep();
    } else if (event instanceof QuantityEventType) {
      return ((QuantityEventType) event).getBizStep();
    }
    throw new IllegalArgumentException(
        "Type " + eventJaxb.getClass().getSimpleName() + " is not supported");
  }

  public DestinationListType getDestinationListType(Object eventJaxb) {
    Object event = ((JAXBElement<?>) eventJaxb).getValue();

    if (event instanceof ObjectEventType) {
      return ((ObjectEventType) event).getExtension() == null ? null
          : ((ObjectEventType) event).getExtension().getDestinationList();
    } else if (event instanceof AggregationEventType) {
      return ((AggregationEventType) event).getExtension() == null ? null
          : ((AggregationEventType) event).getExtension().getDestinationList();
    } else if (event instanceof TransformationEventType) {
      return null;
    } else if (event instanceof TransactionEventType) {
      return ((TransactionEventType) event).getExtension() == null ? null
          : ((TransactionEventType) event).getExtension().getDestinationList();
    } else if (event instanceof QuantityEventType) {
      return null;
    }
    throw new IllegalArgumentException(
        "Type " + eventJaxb.getClass().getSimpleName() + " is not supported");
  }

  public static String getMessageType(Object eventJaxb) {
    EPCISEventType event = (EPCISEventType) ((JAXBElement<?>) eventJaxb).getValue();
    return event.getMessageType();
  }

  public static Optional<String> getBizTransaction(
      Epcis.BizTransaction query, BusinessTransactionListType bizTransactionList) {
    if (bizTransactionList != null && bizTransactionList.getBizTransaction() != null) {
      for (BusinessTransactionType transaction : bizTransactionList.getBizTransaction()) {
        if (transaction != null) {
          if (transaction.getType() != null && transaction.getValue() != null) {
            if (transaction.getType().equals(query.getXmlValue())
                && Strings.isNotBlank(transaction.getValue())) {
              return Optional.of(transaction.getValue());
            }
          }
        }
      }
    }
    return Optional.empty();
  }

  public static Optional<String> getDestination(
      Epcis.DestinationTypes query, DestinationListType destinationListType) {
    if (destinationListType != null) {
      for (SourceDestType destType : destinationListType.getDestination()) {
        if (destType.getType().equals(query.getXmlValue())
            && Strings.isNotBlank(destType.getValue())) {
          return Optional.of(destType.getValue());
        }
      }
    }
    return Optional.empty();
  }

  public static JAXBElement<?> getFirstEvent(EPCISDocumentType epcisDocumentType) {
    return (JAXBElement<?>)
        epcisDocumentType
            .getEPCISBody()
            .getEventList()
            .getObjectEventOrAggregationEventOrQuantityEvent()
            .get(0);
  }

  public static String getMessageId(EPCISDocumentType epcisDocumentType)
      throws EPCISException {
    return getMessageId(getFirstEvent(epcisDocumentType), epcisDocumentType);
  }
  public static String getMessageId(Object eventObject, EPCISDocumentType epcisDocumentType)
      throws EPCISException {
    Optional<String> footerMessageId = getFooterMessageId(epcisDocumentType);
    BusinessTransactionListType bizTransactions = getBizTransactions(eventObject);
    Optional<String> bizTransMessageId =
        getBizTransaction(Epcis.BizTransaction.MESSAGE_ID, bizTransactions);
    if (footerMessageId.isPresent() && bizTransMessageId.isPresent()) {
      if (!footerMessageId.get().equals(bizTransMessageId.get())) {
        throw new EPCISException(
            "Message id was specified in footer and in bizTransaction "
                + Epcis.BizTransaction.MESSAGE_ID.getXmlValue()
                + " with different values. Please only one entry or matching values");
      }
      return bizTransMessageId.get();
    } else if (!footerMessageId.isPresent() && bizTransMessageId.isPresent()) {
      return bizTransMessageId.get();
    } else if (footerMessageId.isPresent() && !bizTransMessageId.isPresent()) {
      return footerMessageId.get();
    } else {
      throw new EPCISException(
          "Mandatory field message_id is missing. Specify either in epcisFoot or in bizTransactions");
    }
  }

  public static Optional<String> getFooterMessageId(EPCISDocumentType epcisDocumentType) {
    Optional<String> footerMessageId = Optional.empty();
    EPCISFooter epcisFooter = epcisDocumentType.getEPCISFooter();
    if (epcisFooter != null) {
      String messageId = epcisFooter.getMessageId();
      if (Strings.isNotBlank(messageId)) {
        footerMessageId = Optional.of(messageId);
      }
    }
    return footerMessageId;
  }
}
