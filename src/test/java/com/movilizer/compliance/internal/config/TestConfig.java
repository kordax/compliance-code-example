package com.movilizer.compliance.internal.config;

import javax.validation.constraints.NotEmpty;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Configuration
@ConfigurationProperties(prefix = "movilizer.test")
@Validated
public class TestConfig {
    public static class Path {
        @NotEmpty
        private String testCases;
        @NotEmpty
        private String xml;

        public String getTestCases() {
            return testCases;
        }

        public void setTestCases(String testCases) {
            this.testCases = testCases;
        }

        public String getXml() {
            return xml;
        }

        public void setXml(String xml) {
            this.xml = xml;
        }
    }

    public static class JsonFile {
        @NotEmpty
        private String locations;
        @NotEmpty
        private String credentials;
        @NotEmpty
        private String hierarchies;

        public String getLocations() {
            return locations;
        }

        public void setLocations(String locations) {
            this.locations = locations;
        }

        public String getCredentials() {
            return credentials;
        }

        public void setCredentials(String credentials) {
            this.credentials = credentials;
        }

        public String getHierarchies() {
            return hierarchies;
        }

        public void setHierarchies(String hierarchies) {
            this.hierarchies = hierarchies;
        }
    }

    public static class XmlFile {
        @NotEmpty
        private String ftpConfig;
        @NotEmpty
        private String dispatchTemplate;

        public String getFtpConfig() {
            return ftpConfig;
        }

        public void setFtpConfig(String ftpConfig) {
            this.ftpConfig = ftpConfig;
        }

        public String getDispatchTemplate() {
            return dispatchTemplate;
        }

        public void setDispatchTemplate(String dispatchTemplate) {
            this.dispatchTemplate = dispatchTemplate;
        }
    }

    private Path path;
    private JsonFile jsonFile;
    private XmlFile xmlFile;

    public Path getPath() {
        return path;
    }

    public void setPath(Path path) {
        this.path = path;
    }

    public JsonFile getJsonFile() {
        return jsonFile;
    }

    public void setJsonFile(JsonFile jsonFile) {
        this.jsonFile = jsonFile;
    }

    public XmlFile getXmlFile() {
        return xmlFile;
    }

    public void setXmlFile(XmlFile xmlFile) {
        this.xmlFile = xmlFile;
    }
}
