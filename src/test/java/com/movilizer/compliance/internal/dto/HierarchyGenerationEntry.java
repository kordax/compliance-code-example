package com.movilizer.compliance.internal.dto;

import com.movilizer.commons.model.ItemExtraFields;
import com.movilizer.hierarchy.api.wrapper.v1.Item;
import com.movilizer.warehouse.api.wrapper.v1.impl.ItemImpl;
import com.movilizer.warehouse.api.wrapper.v1.interfaces.ItemStatus;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class HierarchyGenerationEntry {
  private String id;
  private String parentId;
  private Integer level;
  private String gtin;
  private Integer count;
  private Integer serialLength;
  private Integer prefixLength = 7;
  private String prefix;
  private String extensionDigit;
  private String issuer;
  private String productCode;
  private String generatedIdentifiersLabel;
  private List<HierarchyGenerationEntry> children;
  private Integer offset = 0;

  public HierarchyGenerationEntry() { }

  public HierarchyGenerationEntry(HierarchyGenerationEntry hierarchyGenerationEntry) {
    this(null, hierarchyGenerationEntry);
  }

  public HierarchyGenerationEntry(String id, HierarchyGenerationEntry hierarchyGenerationEntry) {
    this.id = id;
    this.offset = hierarchyGenerationEntry.getOffset();
    this.parentId = hierarchyGenerationEntry.getParentId();
    this.level = hierarchyGenerationEntry.getLevel();
    this.gtin = hierarchyGenerationEntry.getGtin();
    this.count = hierarchyGenerationEntry.getCount();
    this.serialLength = hierarchyGenerationEntry.getSerialLength();
    this.prefixLength = hierarchyGenerationEntry.getPrefixLength();
    this.prefix = hierarchyGenerationEntry.getPrefix();
    this.extensionDigit = hierarchyGenerationEntry.getExtensionDigit();
    this.issuer = hierarchyGenerationEntry.getIssuer();
    this.productCode = hierarchyGenerationEntry.getProductCode();
    this.generatedIdentifiersLabel = hierarchyGenerationEntry.getGeneratedIdentifiersLabel();

    if (hierarchyGenerationEntry.getChildren() != null) {
      this.children = new ArrayList<>(hierarchyGenerationEntry.getChildren());
    } else {
      this.children = new ArrayList<>();
    }
  }

  public com.movilizer.warehouse.api.wrapper.v1.interfaces.Item toWItem() {
    com.movilizer.warehouse.api.wrapper.v1.interfaces.Item wItem = new ItemImpl();
    wItem.setCreationDate(Instant.now().toEpochMilli());
    wItem.setId(this.getId());
    wItem.setStatus(ItemStatus.IN_STOCK);
    wItem.setLastModifiedDate(Instant.now().toEpochMilli());

    Map<String, Object> customFields = new HashMap<>();
    customFields.put(ItemExtraFields.PACKAGING_LEVEL_TYPE_FIELD.getFieldText(), String.valueOf(this.getLevel()));
    customFields.put(ItemExtraFields.RAW_FIELD.getFieldText(), this.getId());

    wItem.setCustomFields(customFields);

    return wItem;
  }

  public Item toHItem() {
    Item result = new Item(
        this.getId(),
        this.getParentId(),
        null,
        null,
        System.currentTimeMillis(),
        null
    );

    if (this.getChildren() != null) {
      result.setChildren(this.getChildren().stream().map(HierarchyGenerationEntry::toHItem).collect(Collectors.toList()));
    } else {
      result.setChildren(Collections.emptyList());
    }

    return result;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getParentId() {
    return parentId;
  }

  public void setParentId(String parentId) {
    this.parentId = parentId;
  }

  public Integer getLevel() {
    return level;
  }

  public void setLevel(Integer level) {
    this.level = level;
  }

  public String getGtin() {
    return gtin;
  }

  public void setGtin(String gtin) {
    this.gtin = gtin;
  }

  public Integer getCount() {
    return count;
  }

  public void setCount(Integer count) {
    this.count = count;
  }

  public Integer getSerialLength() {
    return serialLength;
  }

  public void setSerialLength(Integer serialLength) {
    this.serialLength = serialLength;
  }

  public Integer getPrefixLength() {
    return prefixLength;
  }

  public void setPrefixLength(Integer prefixLength) {
    this.prefixLength = prefixLength;
  }

  public String getPrefix() {
    return prefix;
  }

  public void setPrefix(String prefix) {
    this.prefix = prefix;
  }

  public String getExtensionDigit() {
    return extensionDigit;
  }

  public void setExtensionDigit(String extensionDigit) {
    this.extensionDigit = extensionDigit;
  }

  public String getIssuer() {
    return issuer;
  }

  public void setIssuer(String issuer) {
    this.issuer = issuer;
  }

  public String getProductCode() {
    return productCode;
  }

  public void setProductCode(String productCode) {
    this.productCode = productCode;
  }

  public String getGeneratedIdentifiersLabel() {
    return generatedIdentifiersLabel;
  }

  public void setGeneratedIdentifiersLabel(String generatedIdentifiersLabel) {
    this.generatedIdentifiersLabel = generatedIdentifiersLabel;
  }

  public List<HierarchyGenerationEntry> getChildren() {
    return children;
  }

  public void setChildren(List<HierarchyGenerationEntry> children) {
    this.children = children;
  }

  public int getOffset() {
    return offset;
  }

  public void setOffset(int offset) {
    this.offset = offset;
  }
}
