package com.movilizer.compliance.internal.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.movilizer.compliance.internal.config.TestConfig;
import com.movilizer.compliance.internal.dto.HierarchyGenerationEntry;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class HierarchyHelperService {
  private static final Logger logger = LoggerFactory.getLogger(HierarchyHelperService.class);

  private final HierarchyGenerator generator;
  private final TestCaseLoader loader;
  private final FileService fileService;
  private final TestConfig config;
  private final Gson gson = new GsonBuilder().create();

  public HierarchyHelperService(HierarchyGenerator generator,
      TestCaseLoader loader, FileService fileService, TestConfig config) {
    this.generator = generator;
    this.loader = loader;
    this.fileService = fileService;
    this.config = config;
  }

  public List<HierarchyGenerationEntry> generateHierarchies(String caseName) throws IOException {
    JsonObject file = fileService.loadJson(config.getJsonFile().getHierarchies());
    if (file.has(caseName)) {
      JsonArray entries = file.get(caseName).getAsJsonArray();
      List<HierarchyGenerationEntry> result = new ArrayList<>(entries.size());
      for (JsonElement entry : entries) {
        result.addAll(generator.processEntry(gson.fromJson(entry, HierarchyGenerationEntry.class)));
      }

      return result;
    } else {
      logger.warn("Hierarchies not found for test case: {}", caseName);
      if (caseName.equals("default")) throw new IOException("Inconsistent hierarchies file content!");

      return generateHierarchies("default");
    }
  }
}
