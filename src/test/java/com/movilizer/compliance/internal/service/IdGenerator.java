package com.movilizer.compliance.internal.service;
;
import groovy.lang.Reference;
import java.util.Calendar;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

/**
 * Excerpt from https://git.tools.movilizer.cloud/ps-itg/caesar-project-performance-tests
 *
 */
@Service
public class IdGenerator {
    public static final Integer SSCC_LENGTH = 18;
    public static final Integer GTIN_LENGTH = 14;
    public static final Integer SERIAL_MAX_LENGTH = 20;
    public static final Integer EXTENSION_DIGIT_LENGTH = 1;
    public static final Integer CHECK_DIGIT_LENGTH = 1;
    public static final Integer TPD_TIMESTAMP_LENGHT = 8;
    public static final String SERIAL_LENGTH_TOO_BIG_ERROR_MESSAGE = "Serial number length specified is larger that max allowed. Max: 20";
    public static final String COUNTER_OVERRUN_ERROR_MESSAGE = "Counter is bigger than the chars allocated for the serial number";
    public static final String WRONG_GTIN_LENGTH_ERROR_MESSAGE = "GTIN must contain 14 chars";
    public static final String COMPANY_PREFIX_TOO_SMALL_ERROR_MESSAGE = "Company prefix is too small. Min: 6";
    public static final String COMPANY_PREFIX_TOO_BIG_ERROR_MESSAGE = "Company prefix is too big. Max: 12";
    public static final String EXT_DIGIT_MUST_BE_1_CHAR_ERROR_MESSAGE = "Extension digit should be of length 1";
    public static final String TPD_TIMESTAMP_LENGTH_TOO_BIG_ERROR_MESSAGE = "TPD Timestamp should be of length 8";
    private final Boolean tpdFormat;

    public IdGenerator(Boolean tpdFormat) {
        this.tpdFormat = tpdFormat;
    }

    public IdGenerator() {
        this(false);
    }

    public String sscc(final String companyPrefix, long counter, final String extensionDigit) {
        assert companyPrefix.length() > 5 : COMPANY_PREFIX_TOO_SMALL_ERROR_MESSAGE;
        assert companyPrefix.length() < 13 : COMPANY_PREFIX_TOO_BIG_ERROR_MESSAGE;
        assert extensionDigit.length() == 1 : EXT_DIGIT_MUST_BE_1_CHAR_ERROR_MESSAGE;
        final String serialNumber = String.valueOf(counter);
        assert serialNumber.length() + companyPrefix.length() + EXTENSION_DIGIT_LENGTH + CHECK_DIGIT_LENGTH <= SSCC_LENGTH : COUNTER_OVERRUN_ERROR_MESSAGE;
        final int padding = SSCC_LENGTH - EXTENSION_DIGIT_LENGTH - CHECK_DIGIT_LENGTH - companyPrefix.length();
        String output;
        if (tpdFormat) {
            final String ssccWithoutChecksumDigit = extensionDigit + companyPrefix + StringUtils.leftPad(serialNumber, padding, "0");
            final int checksumDigit = gs1ChecksumDigit(ssccWithoutChecksumDigit);
            output = ssccWithoutChecksumDigit + checksumDigit;
        } else {
            output = "urn:epc:id:sscc:" + companyPrefix + "." + extensionDigit + StringUtils.leftPad(serialNumber, padding, "0");
        }

        return output;
    }

    public String sgtin14(Integer companyPrefixLength, final String gtin, long counter, Integer serialLength) {
        assert serialLength <= SERIAL_MAX_LENGTH : SERIAL_LENGTH_TOO_BIG_ERROR_MESSAGE;
        assert gtin.length() == GTIN_LENGTH : WRONG_GTIN_LENGTH_ERROR_MESSAGE;
        assert companyPrefixLength > 5 : COMPANY_PREFIX_TOO_SMALL_ERROR_MESSAGE;
        assert companyPrefixLength < 13 : COMPANY_PREFIX_TOO_BIG_ERROR_MESSAGE;
        final String serialNumber = String.valueOf(counter);
        assert serialNumber.length() <= serialLength : COUNTER_OVERRUN_ERROR_MESSAGE;
        final int padding = serialLength;
        final String companyPrefixBeforeDot = gtin.substring(1, companyPrefixLength + 1);
        final String gtinAfterDot = gtin.charAt(0) + gtin.substring(companyPrefixLength + 1, gtin.length() - CHECK_DIGIT_LENGTH);
        String output;
        if (tpdFormat) {
            output = gtin + StringUtils.leftPad(serialNumber, padding, "0");
        } else {
            output = "urn:epc:id:sgtin:" + companyPrefixBeforeDot + "." + gtinAfterDot + "." + StringUtils.leftPad(serialNumber, padding, "0");
        }

        return output;
    }

    public String upUIs(final String idIssuer, long counter, Integer serialLength, final String generatedIdentifiersLabel) {
        final String serialNumber = String.valueOf(counter);
        assert serialNumber.length() <= serialLength : COUNTER_OVERRUN_ERROR_MESSAGE;
        final int padding = serialLength;
        String output;
        if (tpdFormat) {
            output = idIssuer + generatedIdentifiersLabel + StringUtils.leftPad(serialNumber, padding, "0") + "a11234" + "04030700132127";
        } else {
            output = "urn:itg:id:upuis:" + idIssuer + generatedIdentifiersLabel + StringUtils.leftPad(serialNumber, padding, "0");
        }

        return output;
    }

    public String upUIl(String upUIs, String tpdTimestamp) {
        assert tpdTimestamp.length() == TPD_TIMESTAMP_LENGHT : TPD_TIMESTAMP_LENGTH_TOO_BIG_ERROR_MESSAGE;
        String upUIl = upUIs.replace("urn:itg:id:upuis:", "");
//        String output = upUIl.replace('QCBDR', '5RQCBDRU:') + "a11234" + "04030700132127" + tpdTimestamp
        String output = upUIl + "a11234" + "04030700132127" + tpdTimestamp;
        if (!tpdFormat) {
            output = output.replace("QCBDR", "5RQCBDRU:").replace("urn:itg:id:upuis:", "urn:itg:id:upuil:");
            if (!output.startsWith("urn:itg:id:upuil:")) {
                output = "urn:itg:id:upuil:" + output;
            }

        }

        return output;
    }

    public static String productCodeTs(Calendar calendar) {
        final String year = String.valueOf(calendar.get(Calendar.YEAR)).substring(2);
        final String month = StringUtils.leftPad(String.valueOf(calendar.get(Calendar.MONTH) + 1), 2, "0");
        final String date = StringUtils.leftPad(String.valueOf(calendar.get(Calendar.DATE)), 2, "0");
        final String hour = StringUtils.leftPad(String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)), 2, "0");
        final String minute = StringUtils.leftPad(String.valueOf(calendar.get(Calendar.MINUTE)), 2, "0");
        final String second = StringUtils.leftPad(String.valueOf(calendar.get(Calendar.SECOND)), 2, "0");
        return "a11234" + year + month + date + hour + minute + second + "00";
    }

    public static String tpdTimestamp(Calendar calendar) {
        final String year = String.valueOf(calendar.get(Calendar.YEAR)).substring(2);
        final String month = StringUtils.leftPad(String.valueOf(calendar.get(Calendar.MONTH) + 1), 2, "0");
        final String date = StringUtils.leftPad(String.valueOf(calendar.get(Calendar.DATE)), 2, "0");
        final String hour = StringUtils.leftPad(String.valueOf(calendar.get(Calendar.HOUR_OF_DAY)), 2, "0");
        return year + month + date + hour;
    }

    public static String testCompanyPrefix(final Integer testerId, final Calendar executionStartTime) {
        assert testerId < 10 && testerId > -1;
        return String.valueOf(testerId) + IdGenerator.tpdTimestamp(executionStartTime).substring(2);
    }

    public static Long testSsccOffset(Calendar executionStartTime) {
        final String hour = StringUtils.leftPad(String.valueOf(executionStartTime.get(Calendar.HOUR_OF_DAY)), 2, "0");
        final String minute = StringUtils.leftPad(String.valueOf(executionStartTime.get(Calendar.MINUTE)), 2, "0");
        String partialGtin = (String) hour + minute;
        int availableDigits = SSCC_LENGTH - 7 - EXTENSION_DIGIT_LENGTH - CHECK_DIGIT_LENGTH;
        return Long.parseLong(StringUtils.rightPad(partialGtin, availableDigits, "0"));
    }

    public static String testGtin(final String companyPrefix, Calendar executionStartTime, final int level) {
        final String hour = StringUtils.leftPad(String.valueOf(executionStartTime.get(Calendar.HOUR_OF_DAY)), 2, "0");
        final String minute = StringUtils.leftPad(String.valueOf(executionStartTime.get(Calendar.MINUTE)), 2, "0");
        final Reference<String> partialGtin = new Reference<>(hour + minute + level);

        int proposedGtinLength = companyPrefix.length() + partialGtin.get().length();
        if (proposedGtinLength > GTIN_LENGTH - CHECK_DIGIT_LENGTH) {
            partialGtin.set(partialGtin.get().substring(proposedGtinLength - GTIN_LENGTH + CHECK_DIGIT_LENGTH));
        } else {
            partialGtin.set(StringUtils.rightPad(partialGtin.get(), GTIN_LENGTH - CHECK_DIGIT_LENGTH - companyPrefix.length(), "0"));
        }


        final String gtinNoChecksum = companyPrefix + partialGtin.get();
        final int checksum = gs1ChecksumDigit(gtinNoChecksum);
        return gtinNoChecksum + checksum;
    }

    public static int gs1ChecksumDigit(final String numberWithoutChecksum) {
        int checksum = 0;
        boolean isMultThree = true;
        for (int i = numberWithoutChecksum.length(); i > 0 ; i--) {
            int n = Integer.parseInt(numberWithoutChecksum.substring(i - 1, i));
            if (isMultThree) {
                n *= 3;
            }

            checksum += n;
            isMultThree = !isMultThree;
        }

        checksum = (10 - (checksum % 10)) % 10;
        return checksum;
    }
}
