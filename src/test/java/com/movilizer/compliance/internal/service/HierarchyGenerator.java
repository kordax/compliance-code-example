package com.movilizer.compliance.internal.service;

import com.movilizer.compliance.internal.dto.HierarchyGenerationEntry;
import java.util.List;

public interface HierarchyGenerator {
  List<HierarchyGenerationEntry> processEntry(HierarchyGenerationEntry entry);

  List<HierarchyGenerationEntry> generateSSCCs(HierarchyGenerationEntry entry
  );

  List<HierarchyGenerationEntry> generateSGTINs(HierarchyGenerationEntry entry);

  List<HierarchyGenerationEntry> generateUpUIs(HierarchyGenerationEntry entry);
}
