package com.movilizer.compliance.internal.service;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.movilizer.compliance.util.XmlUtils;
import com.movilizer.ps.itg.epcis.EPCISDocumentType;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import org.springframework.stereotype.Service;

@Service
public class FileService {
  private final Gson gson;

  public FileService(Gson gson) {
    this.gson = gson;
  }

  public JsonObject loadJson(String file) throws FileNotFoundException {
    if (!file.contains(".json")) {
      file += ".json";
    }

    InputStream stream =
        Thread.currentThread().getContextClassLoader().getResourceAsStream(file);

    if (stream == null) {
      throw new FileNotFoundException(String.format("Failed to load file %s. File was not found", file));
    }

    return gson.fromJson(new InputStreamReader(stream), JsonObject.class);
  }

  public <T> T loadXml(String file, Class<T> clazz) throws FileNotFoundException, JAXBException {
    if (!file.contains(".xml")) {
      file += ".xml";
    }

    InputStream stream =
        Thread.currentThread().getContextClassLoader().getResourceAsStream(file);

    if (stream == null) {
      throw new FileNotFoundException(String.format("Failed to load file %s. File was not found", file));
    }

    Unmarshaller unmarshaller = JAXBContext.newInstance(clazz).createUnmarshaller();

    return ((JAXBElement<T>) unmarshaller.unmarshal(stream)).getValue();
  }

  public InputStream loadResource(String file) throws IOException {
    InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(file);
    if (stream == null) {
      throw new IOException(
          String.format("Failed to load %s file. File was not found", file)
      );
    }

    return stream;
  }

  public File loadResourceDir(String path) throws IOException {
    URL resourcesUrl = Thread.currentThread().getContextClassLoader().getResource(path);
    if (resourcesUrl == null) {
      throw new IOException(
          String.format("Failed to read resource directory %s. Directory was not found", path)
      );
    }

    return new File(resourcesUrl.getFile());
  }
}
