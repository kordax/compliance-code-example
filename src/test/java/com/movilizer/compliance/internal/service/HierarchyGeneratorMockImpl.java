package com.movilizer.compliance.internal.service;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.movilizer.compliance.internal.dto.HierarchyGenerationEntry;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class HierarchyGeneratorMockImpl implements HierarchyGenerator {
  private static final Logger logger = LoggerFactory.getLogger(HierarchyGeneratorMockImpl.class);
  private final Gson gson;

  private IdGenerator generator;

  public final String tpdTimestampFormat = "yyyyMMddh";
  public final DateTimeFormatter dtf = DateTimeFormatter.ofPattern(tpdTimestampFormat);

  public HierarchyGeneratorMockImpl(Gson gson, IdGenerator generator) {
    this.gson = gson;
    this.generator = generator;
  }

  @Override
  public List<HierarchyGenerationEntry> processEntry(HierarchyGenerationEntry entry) {
    List<HierarchyGenerationEntry> result = new ArrayList<>(1);

    if (entry.getId() != null) {
      result.add(entry);
    } else {
      result.addAll(processNode(entry, null));
    }

    for (HierarchyGenerationEntry resultEntry : result) {
      if (resultEntry.getChildren() != null) {
        resultEntry.getChildren().forEach(c -> processNode(c, entry.getId()));
      }
    }

    return result;
  }

  @Override
  public List<HierarchyGenerationEntry> generateSSCCs(HierarchyGenerationEntry entry) {
    List<String> ids = IntStream
        .range(1, entry.getCount() + 1)
        .mapToObj(c -> generator.sscc(entry.getPrefix(), entry.getOffset() + c, entry.getExtensionDigit()))
        .collect(Collectors.toList());

    return toEntries(ids, entry);
  }

  @Override
  public List<HierarchyGenerationEntry> generateSGTINs(HierarchyGenerationEntry entry) {
    List<String> ids = IntStream
        .range(1, entry.getCount() + 1)
        .mapToObj(c -> generator.sgtin14(entry.getPrefixLength(), String.valueOf(entry.getGtin()), entry.getOffset() + c, entry.getSerialLength()))
        .collect(Collectors.toList());

    return toEntries(ids, entry);
  }

  @Override
  public List<HierarchyGenerationEntry> generateUpUIs(HierarchyGenerationEntry entry) {
    if (entry.getSerialLength() == null) {
      throw new IllegalArgumentException("Hierarchy entry should have \"serialLength\" param specified");
    }
    if (entry.getCount() == null) {
      throw new IllegalArgumentException("Hierarchy entry should have \"count\" param specified");
    }

    List<String> uis = IntStream
        .range(1, entry.getCount() + 1)
        .mapToObj(c -> generator.upUIs(
            entry.getIssuer(),
            entry.getOffset() + c,
            entry.getSerialLength(),
            entry.getGeneratedIdentifiersLabel()
        )).collect(Collectors.toList());

    List<String> ids = uis
        .stream()
        .map(s ->
            generator.upUIl(s, String.valueOf(System.currentTimeMillis()).substring(0, 8))
        )
        .collect(Collectors.toList());

    return toEntries(ids, entry);
  }

  public boolean isHierarchyEntry(JsonElement jsonElement) {
    try {
      gson.fromJson(jsonElement, HierarchyGenerationEntry.class);

      return true;
    } catch (Exception e) {
      return false;
    }
  }

  public @Nullable HierarchyGenerationEntry toHierarchyEntry(JsonElement jsonElement) {
    return gson.fromJson(jsonElement, HierarchyGenerationEntry.class);
  }

  private List<HierarchyGenerationEntry> processNode(HierarchyGenerationEntry entry, @Nullable String parentId) {
    List<HierarchyGenerationEntry> result = new CopyOnWriteArrayList<>();

    if (entry.getId() != null) {
      result.add(entry);
    } else {
      if (entry.getLevel() == null) {
        result.addAll(
            generateUpUIs(entry)
        );
      } else {
        switch (entry.getLevel()) {
          case 40:
            result.addAll(
                generateSSCCs(entry)
            );
            break;
          case 30:
          case 20:
            result.addAll(
                generateSGTINs(entry)
            );
            break;
          case 10:
          default:
            result.addAll(
                generateUpUIs(entry)
            );
            break;
        }
      }
    }

    result.forEach(r -> r.setParentId(parentId));

    int n = 0;
    for (HierarchyGenerationEntry resultEntry : result) {
      if (resultEntry.getChildren() != null) {
        resultEntry.getChildren().forEach(e -> {
          e.setLevel(resultEntry.getLevel() - 10);
          e.setParentId(resultEntry.getId());
        });
        List<HierarchyGenerationEntry> childResult = new ArrayList<>();
        for (HierarchyGenerationEntry child : resultEntry.getChildren()) {
          child.setOffset(child.getCount() * n);

          childResult.addAll(processNode(child, resultEntry.getId()));
        }

        resultEntry.getChildren().clear();
        resultEntry.getChildren().addAll(childResult);
        n++;
      }
    }

    return result;
  }

  private List<HierarchyGenerationEntry> toEntries(List<String> ids, HierarchyGenerationEntry entry) {
    return ids.stream()
        .map(id -> new HierarchyGenerationEntry(id, entry))
        .collect(Collectors.toList());
  }
}
