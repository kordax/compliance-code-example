package com.movilizer.compliance.internal.service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.movilizer.commons.util.Credentials;
import com.movilizer.compliance.internal.config.TestConfig;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

@Service
public class TestCaseLoader {
  @Autowired
  private FileService fileService;

  private final TestConfig config;
  private final Environment environment;
  private final Gson gson;

  public TestCaseLoader(TestConfig config, Environment environment) {
    this.config = config;
    this.environment = environment;
    this.gson = new GsonBuilder().setPrettyPrinting().create();
  }

  public JsonObject loadCaseJson(String caseName) throws FileNotFoundException {
    JsonObject envEntries = fileService.loadJson(config.getPath().getTestCases() + caseName);

    return envEntries.get(getEnv()).getAsJsonObject();
  }

  public String getEnv() {
    List<String> profiles = Arrays.asList(environment.getActiveProfiles().clone());
    return profiles
        .stream()
        .filter(
            p -> p.contains("production") ||
                p.contains("preprod") ||
                p.contains("development") ||
                p.contains("quality") ||
                p.contains("local")
        ).findFirst().orElseThrow(() -> new RuntimeException("Failed to retrieve spring profile, no matches!"));
  }

  public Credentials loadTestCredentials() throws IOException {
    JsonObject jsonObject = fileService.loadJson(config.getJsonFile().getCredentials());

    return gson.fromJson(jsonObject.get(getEnv()), Credentials.class);
  }

  public JsonObject retrieveLocations() throws IOException {
    InputStream stream = fileService.loadResource(config.getJsonFile().getLocations());
    JsonObject locations = gson.fromJson(new InputStreamReader(stream, StandardCharsets.UTF_8), JsonObject.class);

    return locations.get(getEnv()).getAsJsonObject();
  }
}
