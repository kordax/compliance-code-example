#!/usr/bin/env groovy

envName = 'not-set'
cloudHostname = 'not-set'
credentialsId = 'not-set'
cloudName = 'not-set'
helm = 'not-set'

def deploy(
           String artifactName,
           String backendName,
           String baseContext,
           String gitCommit,
           String tagName,
           String dockerDomain,
           String publicPort,
           String managementPort,
           String serviceTypes
) {
    String version = 'not-set'
    String customEnv = 'not-set'
    String backendNameWithCustomEnv = 'not-set'
    String baseContextWithCustomEnv = 'not-set'
    String envNameWithCustomEnv = 'not-set'
    String namespace = 'not-set'
    String cpuLimit = 'not-set'
    String cpuRequest = 'not-set'
    String memLimit = 'not-set'
    String memRequest = 'not-set'
    String javaOpts = ''
    String replica
    namespace = 'default'

    cloudName = helpers.detectCloudNameForTag(tagName)
    echo "Target cloud name: $cloudName"
    if (cloudName == 'production') {
        envName = 'production'
        cloudHostname = 'prod.itg.plus.movilizer.cloud'
        credentialsId = 'itg-production-kubeconfig'
        replica = 8
        cpuLimit = '2'
        cpuRequest = '500m'
        memLimit = '4096Mi'
        memRequest = '512Mi'
        javaOpts = '-Xmx4G'
    } else if (cloudName == 'quality') {
        envName = 'quality'
        cloudHostname = 'qa.itg.plus.movilizer.cloud'
        credentialsId = 'itg-qa-kubeconfig'
        replica = 1
        cpuLimit = '2'
        cpuRequest = '250m'
        memLimit = '1024Mi'
        memRequest = '512Mi'
    } else if (cloudName == 'preprod') {
        envName = 'preprod'
        cloudHostname = 'preprod.itg.plus.movilizer.cloud'
        credentialsId = 'itg-preprod-kubeconfig'
        replica = 1
        cpuLimit = '2'
        cpuRequest = '500m'
        memLimit = '4096Mi'
        memRequest = '512Mi'
        javaOpts = '-Xmx4G'
    } else if (cloudName == 'ps') {
        envName = 'ps'
        cloudHostname = 'ps.test.dev.movilizer.cloud'
        credentialsId = 'test-dev-kubeconfig'
        namespace = "ps"
        replica = 1
        cpuLimit = '2'
        cpuRequest = '250m'
        memLimit = '1024Mi'
        memRequest = '512Mi'
    } else if (cloudName == 'rambo') {
        envName = 'rambo'
        cloudHostname = 'test.dev.movilizer.cloud'
        credentialsId = 'test-dev-kubeconfig'
        namespace = "johnrambo"
        replica = 1
        cpuLimit = '2'
        cpuRequest = '250m'
        memLimit = '1024Mi'
        memRequest = '512Mi'
    } else {
        envName = 'development'
        cloudHostname = 'dev.itg.plus.movilizer.cloud'
        credentialsId = 'itg-dev-kubeconfig'
        replica = 1
        cpuLimit = '2'
        cpuRequest = '250m'
        memLimit = '1024Mi'
        memRequest = '512Mi'
    }
    echo "Target cloud name: $credentialsId , replicas requested: $replica"

    echo "Before: $version $customEnv $backendNameWithCustomEnv $baseContextWithCustomEnv"
    withCredentials([usernamePassword(credentialsId: 'jenkins',
            usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
            passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
        version = sh(script: 'gradle -q --console=plain printVersion', returnStdout: true).trim()
        customEnv = sh(script: "gradle -q --console=plain printCustomEnv -PtagName=${tagName}",
                returnStdout: true).trim()
        namespace = sh(script: "gradle -q --console=plain getNamespace -PcloudName=${cloudName}  -PcustomEnv=${customEnv}", returnStdout: true).trim()

        // base context should always be unique. The Kubernetes load balancer would give 404 if 2
        // or more services have the same base context
        backendNameWithCustomEnv = customEnv != "" ? "${backendName}-${customEnv}" : "${backendName}"
        baseContextWithCustomEnv = customEnv != "" ? "/${customEnv}${baseContext}" : "${baseContext}"
        envNameWithCustomEnv = customEnv != "" ? "${envName}-${customEnv}" : "${envName}"

        echo "K8S deployment name: $backendNameWithCustomEnv"
        echo "Endpoint base: $baseContextWithCustomEnv"
        echo "After: $version $customEnv $backendNameWithCustomEnv $baseContextWithCustomEnv "
        withCredentials([file(credentialsId: "$credentialsId", variable: 'KC')]) {
            sh """
                ARTIFACT_NAME=\"${artifactName}\" \
                VERSION=\"${version}\" \
                GIT_COMMIT=\"${gitCommit}\" \
                ENV_NAME=\"${envName}\" \
                BACKEND_NAME=\"${backendNameWithCustomEnv}\" \
                PUBLIC_BASE_CONTEXT=\"${baseContextWithCustomEnv}\" \
                CLOUD_HOSTNAME=\"${cloudHostname}\" \
                DOCKER_DOMAIN=\"${dockerDomain}\" \
                PUBLIC_PORT=\"${publicPort}\" \
                MANAGEMENT_PORT=\"${managementPort}\" \
                SPRING_PROFILES_ACTIVE=\"${envNameWithCustomEnv}\" \
                NAMESPACE=\"${namespace}\" \
                REPLICA_SET=\"${replica}\" \
                SERVICE_TYPES=\"${serviceTypes}\" \
                CPU_LIMIT=\"${cpuLimit}\" \
                CPU_REQUEST=\"${cpuRequest}\" \
                MEM_LIMIT=\"${memLimit}\" \
                MEM_REQUEST=\"${memRequest}\" \
                JAVA_OPTS=\"${javaOpts}\" \
                envsubst < k8s-misl.yaml > k8s-${backendNameWithCustomEnv}.yaml
            """
            helpers.pull("${env.TAG_NAME}")
            sh "mkdir -p ${cloudName}/k8s/${namespace}/ac-tool"
            sh "cp k8s-${backendNameWithCustomEnv}.yaml ${cloudName}/k8s/${namespace}/ac-tool/k8s-${backendNameWithCustomEnv}.yaml"
            sh "cat k8s-${backendNameWithCustomEnv}.yaml"
        }
    }
}

pipeline {
    agent { node { label "ps" } }
    tools {
        gradle 'gradle-6'
        jdk 'JDK11'
    }
    // Limit concurrent builds and number of artifacts
    //options { buildDiscarder(logRotator(numToKeepStr: '1')), disableConcurrentBuilds() }
    options {
        buildDiscarder(logRotator(artifactNumToKeepStr: '40',))
    }
    environment {
        MAVEN_HOME = '/tmp/.m2'
        M2_HOME = '/tmp/.m2'
        GRADLE_USER_HOME = '/tmp/.gradle'
        JAVA_HOME = "${tool 'JDK11'}"
        PATH = "${env.JAVA_HOME}/bin:${env.PATH}"
        SPRING_PROFILES_ACTIVE = "jenkins"
    }
    stages {
        stage('Init') {
            when {
                anyOf {
                    expression { env.BRANCH_NAME ==~ /(master|develop|release\/[.]+)/ }
                    buildingTag()
                }
            }
            options {
                timeout(time: 5, unit: 'MINUTES')
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins',
                        usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
                        passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
                    script {
                        sh 'gradle clean'
                        def version = sh(script: "gradle -q --console=plain printVersion",
                                returnStdout: true).trim()
                        if (env.TAG_NAME != null && version != env.TAG_NAME) {
                            sh "sed -i -e\"s/version = '\\(.*\\)'/version = '${env.TAG_NAME}'/\" build.gradle"
                        }
                    }
                }
            }
            post {
                always {
                    archiveArtifacts artifacts: 'build.gradle'
                }
            }
        }
        stage('Build') {
            when {
                anyOf {
                    expression { env.BRANCH_NAME ==~ /(master|develop|release\/[.]+)/ }
                    buildingTag()
                }
            }
            options {
                timeout(time: 10, unit: 'MINUTES')
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins',
                        usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
                        passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
                    sh 'gradle --info --full-stacktrace clean --refresh-dependencies'
                    sh 'gradle --info --full-stacktrace build --refresh-dependencies -x check'
                }
            }
        }
        stage('Test') {
            when {
                anyOf {
                    expression { env.BRANCH_NAME ==~ /(master|develop|release\/[.]+)/ }
                    buildingTag()
                }
            }
            options {
                timeout(time: 10, unit: 'MINUTES')
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins',
                        usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
                        passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
                    sh 'gradle --info --full-stacktrace check -x build'
                }
            }
            post {
                always {
                    junit '**/build/test-results/**/*.xml'
                }
            }
        }
        stage('Publish') {
            when {
                anyOf {
                    expression {
                        (currentBuild.result == null || currentBuild.result == 'SUCCESS') &&
                                (env.BRANCH_NAME ==~ /(master|develop|release\/[.]+)/)
                    }
                    allOf {
                        expression {
                            currentBuild.result == null || currentBuild.result == 'SUCCESS'
                        }
                        buildingTag()
                    }
                }
            }
            options {
                timeout(time: 8, unit: 'MINUTES')
            }
            parallel {
                stage('Docker') {
                    steps {
                        script {
                            helpers.dockerBuildAndPush()
                        }
                    }
                }
            }
        }
        stage('Create manifest') {
            when {
                anyOf {
                    expression {
                        (currentBuild.result == null || currentBuild.result == 'SUCCESS') &&
                                (env.BRANCH_NAME ==~ /(develop|release\/[.]+)/)
                    }
                    allOf {
                        expression {
                            currentBuild.result == null || currentBuild.result == 'SUCCESS'
                        }
                        buildingTag()
                    }
                }
            }
            options {
                timeout(time: 30, unit: 'MINUTES')
            }
            parallel {
                stage('Create manifest') {
                    steps {
                        script {
                            String artifactName = 'compliance-interface'
                            String backendName = 'ac-tool'
                            String baseContext = '/misl/ac-tool'
                            deploy(artifactName,
                                    backendName,
                                    baseContext,
                                    "${env.GIT_COMMIT}",
                                    "${env.TAG_NAME}",
                                    'nexus.tools.movilizer.cloud/ps-itg',
                                    '8080',
                                    '9001',
                                    'NodePort'
                            )
                        }
                    }
                }
            }
        }
        stage('Git push to GitOps') {
            when {
                allOf {
                    expression { ! helpers.envRequiresPR(envName) }
                    buildingTag()
                }
            }
            steps {
                script {
                    helpers.push("${env.TAG_NAME}")
                }
            }
        }
        stage('Create new branch and PR for GitOps') {
            when {
                allOf {
                    // Change this to 'prod'! 'development' env doesnt require this.
                    expression { helpers.envRequiresPR(envName) }
                    buildingTag()
                }
            }
            steps {
                script {
                    helpers.newBranchAndPR("${env.TAG_NAME}")
                }
            }
        }
        stage('Integration Test') {
            when {
                expression {
                    env.BRANCH_NAME == 'develop' && (currentBuild.result == 'SUCCESS' || currentBuild.result == null)
                }
            }
            steps {
                withCredentials([usernamePassword(credentialsId: 'jenkins',
                        usernameVariable: 'ORG_GRADLE_PROJECT_movilizerUsername',
                        passwordVariable: 'ORG_GRADLE_PROJECT_movilizerPassword')]) {
                    script {
                        env.BRANCH_NAME = 'development'
                        sh 'gradle --info --stacktrace integrationTestDevelopment'
                    }
                }
            }
            post {
                always {
                    junit '**/build/test-results/**/*.xml'
                }
            }
        }
    }
}