FROM registry.hub.docker.com/library/openjdk:11-jre-slim


ENV JAVA_HOME=/usr/local/openjdk-11
ENV PATH=/usr/local/openjdk-11/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# Place executable in /app
RUN mkdir /app
ARG JAR_FILE
ADD ${JAR_FILE} /app/app.jar
RUN chmod +x /app/app.jar

EXPOSE 8080
EXPOSE 9001
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app/app.jar"]
