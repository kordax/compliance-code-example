#!/bin/bash

BASEDIR=$(dirname "$0")
cd $BASEDIR

CFG=""
for line in $(cat test-config.config); do
  CFG="$CFG --$line"
done;

export JAVA_HOME="/d/projects/jdk-11.0.7"

#cd ../../compliance-interface && \
./gradlew clean build
$JAVA_HOME/bin/java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -jar $(ls build/libs/*.jar) -D$CFG
#./gradlew bootRun --args="$CFG" -Dorg.gradle.jvmargs='-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005'
